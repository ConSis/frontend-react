import * as types from './action-types';

export function onLoginRequest() {
  return {
    type: types.ON_LOGIN_REQUEST,
  };
}

export function onLoginSuccess(session) {
  return {
    type: types.ON_LOGIN_SUCCESS,
    session,
  };
}

export function onLoginFailure() {
  return {
    type: types.ON_LOGIN_FAILURE,
  };
}
