import * as types from './action-types';

export function onGetNotificationsSuccess(notifications) {
  return {
    type: types.ON_GET_NOTIFICATIONS_SUCCESS,
    notifications: notifications,
  };
};