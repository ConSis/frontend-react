import axios from 'axios';
import is from 'is_js';

import { getUserSession } from '../utils/authHelper';
import URL from '../utils/urlHelper';


export function getCondos(filters) {
  const session = getUserSession();
  let params;

  if (is.existy(filters)) {
    params = {
      without_admin: is.existy(filters.withoutAdmin)
      && is.truthy(filters.withoutAdmin) ? true : undefined,
    };
  }

  return axios({
    method: 'get',
    url: `${URL}/condominios`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
    params, // Es lo mism oque params: params
  })
    .then((response) => {
      console.log(response);

      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function addCondo(condo) {
  const session = getUserSession();


  const params = {
    condominio: condo,
  };

  return axios({
    method: 'post',
    url: `${URL}/condominios`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
    data: params,
  })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log(error, error.response);
      throw new Error(error);
    });
}

export function deleteCondo(id) {
  const session = getUserSession();
  return axios({
    method: 'delete',
    url: `${URL}/condominios/${id}`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log("condominio eliminaoooo")
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    })
}

export function getCondoAdmins() {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/admins`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function addCondoAdmin(adminCondo) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/admins/create`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
    data: adminCondo,
  })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log(error, error.response);
      throw new Error(error);
    });
}