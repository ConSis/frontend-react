import axios from 'axios';
import { getUserSession } from '../utils/authHelper';
import URL from '../utils/urlHelper';

export function getAdminCondoInfo() {
  //Con esto puedo obtener el id del administrador.
  const session = getUserSession();
  const id = session.user.id;
  return axios({
    method: 'get',
    url: `${URL}/admins/${id}`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function createPayment(condoId, newPayment) {
  const session = getUserSession();
  const object = {
    payment: newPayment,
  };
  return axios({
    method: 'post',
    url: `${URL}/condominios/${condoId}/create_payment`,
    data: object,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
    //se deja sin catch porque ya creè el catch en el admincondcontainer.js
}

export function getNotPublishedPayments(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/payments_not_published`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function getPublishedPayments(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/payments_published`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function getPaymentDetails(paymentId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/payment/${paymentId}/details`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function sendNewDetail(paymentId, newDetail) {
  const object = {
    payment_detail: newDetail,
  };
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/payment/${paymentId}/create_detail`,
    data: object,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function getPaymentDetail(paymentDetailId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/payment_detail/${paymentDetailId}`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function editPaymentDetail(paymentDetailId, paymentDetail) {
  const session = getUserSession();
  const paymentDetailObject = {
    payment_detail: paymentDetail,
  };
  return axios({
    method: 'post',
    url: `${URL}/payment_detail/${paymentDetailId}/edit`,
    data: paymentDetailObject,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function destroyPaymentDetail(paymentDetailId) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/payment_detail/${paymentDetailId}/remove`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function publishPayment(paymentId, condoId) {
  const session = getUserSession();
  const object = {
    publish_payment: {
      condominio_id: condoId,
    },
  };
  return axios({
    method: 'post',
    url: `${URL}/payment/${paymentId}/publish_payment`,
    data: object,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function createCopropietary(condoId, newCopropietary) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/condominios/${condoId}/create_copropietary`,
    data: newCopropietary,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return true;
    })
    .catch((error) => {
      console.log(error);
      return false;
    });
}

export function getCopropietaries(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/copropietaries_for_admin`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function getVisits(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/visits`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response.data);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function destroyVisit(visitId) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/visit/${visitId}/destroy_visit`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return true;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
      return false;
    });
}

export function getPackages(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/packages`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function registerPackage(userId, newPackage) {
  const packageObject = {
    package: newPackage,
  };
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/copropietary/${userId}/register_package`,
    data: packageObject,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
    })
}

export function changeStatePackage(packageId, packageChanged) {
  const packageObject = {
    package: packageChanged,
  };
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/package/${packageId}/change_state`,
    data: packageObject,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function getNews(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/news`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function registerNew(condoId, newNewObject) {
  const session = getUserSession();
  const newObject = {
    new: newNewObject,
  };
  return axios({
    method: 'post',
    url: `${URL}/condominios/${condoId}/register_news`,
    data: newObject,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function destroyNew(newId) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/news/${newId}/destroy_new`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return true;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
      return false;
    });
}

export function destroyEmployee(employeeId) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/employee/${employeeId}/destroy_employee`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return true;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
      return false;
    });
}

export function getEmployees(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/employees`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function registerEmployee(condoId, newEmployee) {
  const session = getUserSession();
  const employeeObject = {
    employee: newEmployee,
  };
  return axios({
    method: 'post',
    url: `${URL}/condominios/${condoId}/register_employee`,
    data: employeeObject,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function getStorageTypes(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/storage_types`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function registerStorageType(condoId, newStorageType) {
  const session = getUserSession();
  const storageTypeObject = {
    storage_type: newStorageType,
  };
  return axios({
    method: 'post',
    url: `${URL}/condominios/${condoId}/register_storage_type`,
    data: storageTypeObject,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function destroyStorageType(storageTypeId) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/storage_type/${storageTypeId}/destroy_storage_type`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return true;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
      return false;
    });
}

export function getParkingTypes(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/parking_types`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function registerParkingType(condoId, newParkingType) {
  const session = getUserSession();
  const parkingTypeObject = {
    parking_type: newParkingType,
  };
  return axios({
    method: 'post',
    url: `${URL}/condominios/${condoId}/register_parking_type`,
    data: parkingTypeObject,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return true;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
      return false;
    });
}

export function destroyParkingType(parkingTypeId) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/parking_type/${parkingTypeId}/destroy_parking_type`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return true;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
      return false;
    });
}

export function getAppartmentTypes(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/appartment_types`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function registerAppartmentType(condoId, newAppartmentType) {
  const session = getUserSession();
  const appartmentTypeObject = {
    appartment_type: newAppartmentType,
  };
  return axios({
    method: 'post',
    url: `${URL}/condominios/${condoId}/register_appartment_type`,
    data: appartmentTypeObject,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return true;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
      return false;
    });
}

export function getTransferInfo(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/transfer_info`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function editTransferInfo(transferInfoId, newTransferInfo) {
  const session = getUserSession();
  const transferInfoObject = {
    transfer_info: newTransferInfo,
  };
  return axios({
    method: 'post',
    url: `${URL}/transfer_info/${transferInfoId}/edit`,
    data: transferInfoObject,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
      return false;
    });
}

export function getPayedUserPayments(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/user_payments`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function confirmUserPayment(userPaymentId) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/user_payment/${userPaymentId}/admin_confirm_payment`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function rejectUserPayment(userPaymentId) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/user_payment/${userPaymentId}/admin_reject_payment`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function getGraphData(condoId, year, month) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/graph_data`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
    params: {
      year: year,
      month: month,
    },
  })
    .then((response) => {
      return response.data;
    });
}

