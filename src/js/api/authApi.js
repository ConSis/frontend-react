
import axios from 'axios';
import { toast } from 'react-toastify';
import store from '../store';
import URL from '../utils/urlHelper';
import { saveSession, loadSession } from '../utils/localStorageHelper';
import { getAuthToken } from '../utils/authHelper';

import {
  onLoginRequest,
  onLoginSuccess,
  onLoginFailure,
} from '../actions/authActions';

export function logIn(credentials) {
  store.dispatch(onLoginRequest());

  return axios.post(`${URL}/auth/sign_in`, credentials)
    .then((response) => {
      console.log(response);
      const session = {
        token: response.headers['access-token'],
        client: response.headers.client,
        uid: response.headers.uid,
        user: response.data.data,
      };
      // const { user, business } = response.data;

      /*
      const session = {
        user,
        business,
      };
      */

      store.dispatch(onLoginSuccess(session));
      saveSession(session);
      // history.push('/');
    })
    .catch((error) => {
      console.log('ERROR0', error, error.response);
      store.dispatch(onLoginFailure());
      throw error;
    });
}
