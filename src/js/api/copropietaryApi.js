import axios from 'axios';
import { getUserSession } from '../utils/authHelper';
import URL from '../utils/urlHelper';

export function getCopropietaryInfo() {
  const session = getUserSession();
  const id = session.user.id;
  return axios({
    method: 'get',
    url: `${URL}/copropietary/${id}`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('ERROR', error, error.response);
    });
}

export function sendVisitsInfo(lista) {
  const visits = {
    visits: lista,
  };
  const session = getUserSession();
  //guarda la id que corresponde a user_id
  //en ignacio león user_id = 5
  const id = session.user.id;
  return axios({
    method: 'post',
    url: `${URL}/copropietary/${id}/register_visits`,
    data: visits,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log('error', error, error.response);
    });
}

export function getAdCategories(condo_id) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condo_id}/ad_categories`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('error', error, error.response);
    });
}

export function sendNewAd(newAd) {
  const ad = {
    ad: newAd,
  };
  const session = getUserSession();
  const id = session.user.id;
  return axios({
    method: 'post',
    url: `${URL}/copropietary/${id}/register_ad`,
    data: ad,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log('error', error, error.response);
    });
}

export function getAds(condo_id) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condo_id}/ads`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log('error', error, error.response);
    });
}

export function sendNewComplaint(newComplaint) {
  const complaint = {
    complaint: newComplaint,
  };
  const session = getUserSession();
  const id = session.user.id;
  return axios({
    method: 'post',
    url: `${URL}/copropietary/${id}/register_complaint`,
    data: complaint,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log('error', error, error.response);
    });
}

export function getComplaints(condo_id) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condo_id}/complaints`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log('error', error, error.response);
    });
}

export function getPackages() {
  const session = getUserSession();
  const id = session.user.id;
  return axios({
    method: 'get',
    url: `${URL}/copropietary/${id}/packages`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log('error', error, error.response);
    });
}

export function complaintVoteDown(complaintId) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/complains/${complaintId}/vote_down`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log('error', error, error.response);
      });
}

export function complaintVoteUp(complaintId) {
  const session = getUserSession();
  return axios({
    method: 'post',
    url: `${URL}/complains/${complaintId}/vote_up`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log('error', error, error.response);
    });
}

export function getNews(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/news`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      console.log(response);
      return response.data;
    })
    .catch((error) => {
      console.log('error', error, error.response);
    });
}

export function checkPayment(coproId, year, month) {
  const session = getUserSession();
  const object = {
    payment: {
      year: year,
      month: month,
    },
  };
  return axios({
    method: 'post',
    url: `${URL}/copropietary/${coproId}/check_payment`,
    data: object,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function getEmployees(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/employees`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function changeStateUserPayment(userPaymentId, transferCode) {
  const session = getUserSession();
  const object = {
    user_payment: {
      transfer_code: transferCode,
    },
  };
  return axios({
    method: 'post',
    url: `${URL}/user_payment/${userPaymentId}/change_state`,
    data: object,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function getUserPayments(coproId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/copropietary/${coproId}/get_user_payments`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function getHiredServices(coproId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/copropietary/${coproId}/hired_services`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    });
}

export function getTransferInfo(condoId) {
  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/condominios/${condoId}/transfer_info`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      return response.data;
    })
}
