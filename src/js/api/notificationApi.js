import axios from 'axios';
import { toast } from 'react-toastify';
import store from '../store';
import URL from '../utils/urlHelper';
import { saveSession, loadSession } from '../utils/localStorageHelper';
import { getAuthToken, getUserSession } from '../utils/authHelper';

import { onGetNotificationsSuccess } from '../actions/notificationsActions';

export function getNotifications(coproId) {

  const session = getUserSession();
  return axios({
    method: 'get',
    url: `${URL}/copropietary/${coproId}/notifications`,
    headers: {
      'access-token': session.token,
      client: session.client,
      uid: session.uid,
    },
  })
    .then((response) => {
      const notifications = response.data;

      store.dispatch(onGetNotificationsSuccess(notifications));
    });
}

export function peo() {

}