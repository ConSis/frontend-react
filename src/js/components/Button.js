import React, { Component } from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';
import classnames from 'classnames';

class Button extends Component {
  constructor(props) {

    super(props);
    this.state = {};

    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.onClick();
  }

  render() {
    return (
      <div onClick={this.onClick} className={this.props.className}>
        Iniciar Sesión
      </div>
    );
  }
}
export default Button;
