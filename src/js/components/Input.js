import React, { Component } from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';
import classnames from 'classnames';

class Input extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    // Puede que olviden pasarnos onChange al
    // invocar el component
    //console.log(this.props.onChange, event.target.value)
    if (is.existy(this.props.onChange)) {
      this.props.onChange(event.target.value);
    }
  }

  render() {
    // Analogo a const icon = this.props.icon;
    //console.log('props', this.props);
    const { icon } = this.props;

    const style = {
      backgroundImage: `url(${icon})`,
    };

    const renderedIcon = (icon !== null) ? (
      <div
        style={style}
        className="input-icon"
      />
    ) : null;


    return (
      <div className="input-container">
        {renderedIcon}
        <input
          type={this.props.type}
          className="input"
          onChange={this.onChange}
          value={this.props.value}
          placeholder={this.props.placeholder}
        />
      </div>
    );
  }
}

Input.propTypes = {
  onChange: PropTypes.func.isRequired,
  icon: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
};

Input.defaultProps = {
  icon: null,
  // sir vepar amap en arreglo por ej.
  value: '',
};


export default Input;
