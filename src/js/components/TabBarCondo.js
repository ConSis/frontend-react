import React, { Component } from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';
import classnames from 'classnames';

class TabBarCondo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
    };

    this.renderView = this.renderView.bind(this);
    this.renderTabs = this.renderTabs.bind(this);
  }

  renderView() {
    const vistas = this.props.vistas.map((vista, index) => {
      let myClass;
      if (index === this.state.currentIndex) {
        myClass = 'condo-view-active';
      } else {
        myClass = 'condo-view-inactive';
      }

      return (
        <div key={vista.text} className={myClass}>{vista.content}</div>
      );
    });

    return (
      <div className="content-condo-view">
        {vistas}
      </div>
    );
  }

  renderTabs() {
    const tab = this.props.vistas.map((vista, index) => {
      const tabClass = classnames({
        'tab-icon': true,
        'condo-tab-active': index === this.state.currentIndex,
      });

      return (
        <button
          key={vista.text}
          className={tabClass}
          onClick={() => {
            this.setState({
              currentIndex: index,
            });
          }}
        >
          <div className={vista.textClass}>
            {vista.text}
          </div>
        </button>
      );
    });

    return (
      <div className="tab-condo-content">
        {tab}
      </div>
    );
  }

  render() {
    return (
      <div className="views-condo-container">
        {this.renderTabs()}
        {this.renderView()}
      </div>
    );
  }
}

export default TabBarCondo;
