import React, { Component } from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';
import classnames from 'classnames';

const isCordovaApp = document.URL.indexOf('http://') === -1
  && document.URL.indexOf('https://') === -1;

class TabBarNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
    };

    this.renderViews = this.renderViews.bind(this);
    this.renderTabs = this.renderTabs.bind(this);

    this.goBack = this.goBack.bind(this);
    this.logOut = this.logOut.bind(this);
  }

  goBack() {
    if (!this.shouldShowBackButton()) {
      return;
    }

    this.props.history.goBack();
  }

  logOut() {
    if (window.cordova) {
      navigator.notification.confirm(
        '¿Está seguro que desea cerrar la sesión?',
        (buttonIndex) => {
          if (buttonIndex === 2) {
            localStorage.clear();
            this.props.history.replace('/auth');
          }
        },
        'Confirmación',
        ['Cancelar', 'Aceptar'],
      );
    } else {
      localStorage.clear();
      this.props.history.replace('/auth');
    }
  }

  shouldShowBackButton() {
    const { history } = this.props;

    const { location } = history;
    if (location.pathname === '/copropietary' || location.pathname === '/copropietary/') {
      return false;
    }

    if (location.pathname === '/admin' || location.pathname === '/admin/') {
      return false;
    }

    if (location.pathname === '/admin-cond' || location.pathname === '/admin-cond/') {
      return false;
    }
    return true;
  }

  renderTopBar() {
    const shouldShowBackButton = this.shouldShowBackButton();
    return (
      <div className="app-top-bar">
        <div
          className="action"
          onClick={this.goBack}
        >
          {shouldShowBackButton && <div className="back-button" />}
        </div>
        <div
          className="action"
          onClick={this.logOut}
        >
          <div className="log-out" />
        </div>
      </div>
    );
  }

  renderViews() {
    const views = this.props.views.map((view, index) => {
      let myclass;
      if (index === this.state.currentIndex) {
        myclass = 'active';
      } else {
        myclass = 'inactive';
      }

      return (
        <div key={view.icon} className={myclass}>{view.content}</div>
      );
    });

    return (
      <div className="content-view">
        {views}
      </div>
    );
  }

  renderTabs() {
    const tab = this.props.views.map((view, index) => {
      const tabClass = classnames({
        'tab-icon': true,
        active: index === this.state.currentIndex,
      });

      return (
        <button
          key={view.icon}
          className={tabClass}
          onClick={() => {
            this.setState({
              currentIndex: index,
            });
            if (is.existy(this.props.onTabSelected)) {
              this.props.onTabSelected(index);
            }
          }}
        >
          <div className={view.icon} />
          <div className={view.textClass}>
            {view.text}
          </div>
        </button>
      );
    });
    return (
      <div className="tab-content">
        {tab}
      </div>
    );
  }

  render() {
    return (
      <div className="views-container">
        {this.renderTopBar()}
        {this.renderViews()}
        {this.renderTabs()}
      </div>
    );
  }
}

export default TabBarNavigation;
