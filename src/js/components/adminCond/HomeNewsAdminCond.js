import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withRouter from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import moment from 'moment';
import is from 'is_js';

import history from '../../utils/historyHelper';
import {
  getAdminCondoInfo,
  getNews,
  registerNew,
  destroyNew } from '../../api/adminCondoApi';

moment.locale('es');

class HomeNewsAdminCond extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openAddNewsModal: false,
      openRemoveNewsModal: false,

      stepAddNewsModal: 1,
      stepRemoveNewModal: 1,
      titleNews: '',
      messageNews: '',

      succesfulNewNew: 0,
      successfulDestroyNew: 0,

      adminCondoInfo: '',

      newsList: [],
    };
    this.onOpenAddNewsModal = this.onOpenAddNewsModal.bind(this);
    this.onOpenRemoveNewsModal = this.onOpenRemoveNewsModal.bind(this);

    this.onCloseModal = this.onCloseModal.bind(this);
    this.onNextStepAddNewsModal = this.onNextStepAddNewsModal.bind(this);
    this.onNextStepRemoveNewsModal = this.onNextStepRemoveNewsModal.bind(this);
    this.onNextStepModal = this.onNextStepModal.bind(this);

    this.onTitleNewsChange = this.onTitleNewsChange.bind(this);
    this.onMessageNewsChange = this.onMessageNewsChange.bind(this);

    this.registerNew = this.registerNew.bind(this);
    this.goBack = this.goBack.bind(this);
    this.logOut = this.logOut.bind(this);
  }

  componentDidMount() {
    getAdminCondoInfo()
      .then((adminInfoFromApi) => {
        this.setState({
          adminCondoInfo: adminInfoFromApi,
        });
        getNews(this.state.adminCondoInfo.condo_id)
          .then((newsListFromApi) => {
            this.setState({
              newsList: newsListFromApi,
            });
          });
      });
  }

  goBack() {
    this.props.history.goBack();
  }

  logOut() {
    if (window.cordova) {
      navigator.notification.confirm(
        '¿Está seguro que desea cerrar la sesión?',
        (buttonIndex) => {
          if (buttonIndex === 2) {
            localStorage.clear();
            this.props.history.replace('/auth');
          }
        },
        'Confirmación',
        ['Cancelar', 'Aceptar'],
      );
    } else {
      localStorage.clear();
      this.props.history.replace('/auth');
    }
  }

  renderTopBar() {
    const shouldShowBackButton = true;
    return (
      <div className="app-top-bar" style={{ position: 'fixed' }}>
        <div
          className="action"
          onClick={this.goBack}
        >
          {shouldShowBackButton && <div className="back-button" />}
        </div>
        <div
          className="action"
          onClick={this.logOut}
        >
          <div className="log-out" />
        </div>
      </div>
    );
  }

  /*MODALS*/
  onOpenAddNewsModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openAddNewsModal: true });
  }

  onOpenRemoveNewsModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openRemoveNewsModal: true });
  }

  onCloseModal() {
    this.setState({
      openAddNewsModal: false,
      openRemoveNewsModal: false,

      stepAddNewsModal: 1,
      stepRemoveNewModal: 1,

      titleNews: '',
      messageNews: '',

      succesfulNewNew: 0,
      successfulDestroyNew: 0,
  
      selectedNewCard: '',
    });
    this.onCancelEditClick();
  }

  registerNew(condoId, newNew) {
    registerNew(condoId, newNew)
      .then((response) => {
        this.setState({
          succesfulNewNew: 1,
        });
      })
      .catch((error) => {
        this.setState({
          succesfulNewNew: 2,
        });
      });
  }

  destroyNew(newId) {
    destroyNew(newId)
      .then((response) => {
        this.setState({
          successfulDestroyNew: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulDestroyNew: 2,
        });
      });
  }

  onTitleNewsChange(event) {
    const data = event.target.value;
    this.setState({
      titleNews: data,
    });
  }

  onMessageNewsChange(event) {
    const data = event.target.value;
    this.setState({
      messageNews: data,
    });
  }

  onNextStepAddNewsModal(nextStep) {
    this.setState({
      stepAddNewsModal: nextStep,
    });
  }

  onNextStepRemoveNewsModal(nextStep) {
    this.setState({
      stepRemoveNewModal: nextStep,
    });
  }

  onNextStepModal(nextStep) {
    this.setState({
      stepAddStorageTypeModal: nextStep,
      stepRemoveStorageType: nextStep,
      stepAddParkingTypeModal: nextStep,
      stepRemoveParkingType: nextStep,
      stepAddAppartmentTypeModal: nextStep,
      stepEditTransferInfo: nextStep,
    });
  }
  /*ENDMODALS*/

  render() {

    let newsListCards;
    if (is.empty(this.state.newsList)) {
      newsListCards = (
        <div className="no-news-container">
          No existen noticias aún, crea una para los copropietarios.
        </div>
      );
    } else {
      newsListCards = this.state.newsList.map((item, index) => {
        const createdDate = moment(item.created_at);
        return (
          <div
            key={index}
            className="news-card"
          >
            <div className="title-container">
              <div className="title">
                {item.title}
              </div>
            </div>
            <div className="image-container">
            </div>
            <div className="message-container">
              <div className="message">
                {item.message}
              </div>
            </div>
            <div className="date-container">
              <div className="date">
                {createdDate.format('DD/MM/YYYY')} a las {createdDate.format('HH:MM')}
              </div>
            </div>
            <div
              className="trash-container"
              onClick={(e) => {
                this.onOpenRemoveNewsModal(e);
                this.setState({
                  selectedNewCard: item.id,
                });
              }}
            >
              <img
                alt="Borrar Noticia"
                className="trash"
                src={require("../../../assets/imgs/trash-color.svg")}
              />
            </div>
          </div>
        );
      });
    }

    let messageNewNewsResponse;
    if (this.state.succesfulNewNew === 1) {
      messageNewNewsResponse = '¡Se ha creado la noticia con éxito!';
    } else if (this.state.succesfulNewNew === 2) {
      messageNewNewsResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentAddNewsModal;
    switch (this.state.stepAddNewsModal) {
      case 1: {
        contentAddNewsModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                ¡Mantén a tu comunidad informada con noticias!
              </div>
            </div>
            <div className="icon-package-modal">
              <img
                alt="Ícono"
                className="icon"
                src={require("../../../assets/imgs/reading.svg")}
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => { this.onNextStepAddNewsModal(2); }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentAddNewsModal = (
          <div className="package-modal-container">
            <div className="sub-title-container">
              <div className="sub-title">
                Completa los siguientes datos:
              </div>
            </div>
            <div className="form-line">
              <div className="line-title">
                Título de la noticia:
              </div>
              <input
                type="text"
                onChange={this.onTitleNewsChange}
                value={this.state.titleNews}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                ¿Qué le quieres informar a la comunidad?:
              </div>
              <textarea
                rows={5}
                onChange={this.onMessageNewsChange}
                value={this.state.messageNews}
                className="text-area-form"
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  const object = {
                    title: this.state.titleNews,
                    message: this.state.messageNews,
                  };
                  this.registerNew(this.state.adminCondoInfo.condo_id, object);
                  this.onNextStepAddNewsModal(3); 
                }}
              >
                Guardar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 3: {
        contentAddNewsModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageNewNewsResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getNews(this.state.adminCondoInfo.condo_id)
                    .then((newsListFromApi) => {
                      this.setState({
                        newsList: newsListFromApi,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    let messageDestroyNewsResponse;
    if (this.state.successfulDestroyNew === 1) {
      messageDestroyNewsResponse = '¡Se ha borrado noticia con éxito!';
    } else if (this.state.successfulDestroyNew === 2) {
      messageDestroyNewsResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentRemoveNewsModal;
    switch (this.state.stepRemoveNewModal) {
      case 1: {
        contentRemoveNewsModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                ¿Seguro que quieres borrar esta noticia?
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  this.destroyNew(this.state.selectedNewCard);
                  this.onNextStepRemoveNewsModal(2);
                }}
              >
                Confirmar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentRemoveNewsModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageDestroyNewsResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getNews(this.state.adminCondoInfo.condo_id)
                    .then((newsListFromApi) => {
                      this.setState({
                        newsList: newsListFromApi,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    console.log(this.state);
    return (
      <div className="condo-view">
        <Modal
          classNames={{
            modal: 'custom-package-modal',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openAddNewsModal}
          onClose={this.onCloseModal}
          little
        >
          {contentAddNewsModal}
        </Modal>
        <Modal
          classNames={{
            modal: 'custom-package-modal',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openRemoveNewsModal}
          onClose={this.onCloseModal}
          little
        >
          {contentRemoveNewsModal}
        </Modal>
        <div
          className="add-news-container"
          onClick={this.onOpenAddNewsModal}
        >
          <img
            alt="Agregar Noticia"
            className="add-news"
            src={require("../../../assets/imgs/add-news.svg")}
          />
        </div>
        {this.renderTopBar()}
        <div className="news-cards-container">
          {newsListCards}
        </div>
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}


export default HomeNewsAdminCond;
