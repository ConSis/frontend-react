import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Modal from 'react-responsive-modal';
import moment from 'moment';
import is from 'is_js';
import { Async } from 'react-select';
import history from '../../utils/historyHelper';
import {
  getAdminCondoInfo,
  getCopropietaries,
  getPackages,
  registerPackage,
  changeStatePackage } from '../../api/adminCondoApi';

moment.locale('es');

class HomePackagesAdminCondComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      packagesList: [],
      openPackageModal: false,
      selectedPackageCard: '',
      copropietarySelected: '',
      descriptionPackageInput: '',
      openChangeStateModal: false,
      stepPackageModal: 1,
      stepChangeStatePackageModal: 1,
      receivedNameInput: '',
      retrivedNameInput: '',

      succesfulNewPackage: 0,
      queryPackages: '',
    };

    this.goBack = this.goBack.bind(this);
    this.logOut = this.logOut.bind(this);
    this.onOpenPackageModal = this.onOpenPackageModal.bind(this);
    this.onNextStepPackageModal = this.onNextStepPackageModal.bind(this);
    this.loadCopropietariesOption = this.loadCopropietariesOption.bind(this);
    this.onCopropietarySelectedChange = this.onCopropietarySelectedChange.bind(this);
    this.onDescriptionPackageChange = this.onDescriptionPackageChange.bind(this);
    this.onReceivedNamePackageChange = this.onReceivedNamePackageChange.bind(this);
    this.onRetrivedNamePackageChange = this.onRetrivedNamePackageChange.bind(this);
    this.registerPackage = this.registerPackage.bind(this);
    this.changeStatePackage = this.changeStatePackage.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
  }

  componentDidMount() {
    getAdminCondoInfo()
      .then((adminInfoFromApi) => {
        this.setState({
          adminCondoInfo: adminInfoFromApi,
        });
        getPackages(this.state.adminCondoInfo.condo_id)
          .then((packagesList) => {
            this.setState({
              packagesList: packagesList,
            });
          });
      });
  }

  /*MODALS*/

  onCloseModal() {
    this.setState({
      openPackageModal: false,
      openChangeStateModal: false,
      stepPackageModal: 1,
      stepChangeStatePackageModal: 1,
      copropietarySelected: '',
      descriptionPackageInput: '',
      receivedNameInput: '',
      retrivedNameInput: '',
      succesfulNewPackage: 0,
      succesfulChangedState: 0,
      selectedPackageCard: '',
    });
  }
  /*ENDMODALS*/

  goBack() {
    this.props.history.goBack();
  }

  logOut() {
    if (window.cordova) {
      navigator.notification.confirm(
        '¿Está seguro que desea cerrar la sesión?',
        (buttonIndex) => {
          if (buttonIndex === 2) {
            localStorage.clear();
            this.props.history.replace('/auth');
          }
        },
        'Confirmación',
        ['Cancelar', 'Aceptar'],
      );
    } else {
      localStorage.clear();
      this.props.history.replace('/auth');
    }
  }

  renderTopBar() {
    const shouldShowBackButton = true;
    return (
      <div className="app-top-bar" style={{ position: 'fixed' }}>
        <div
          className="action"
          onClick={this.goBack}
        >
          {shouldShowBackButton && <div className="back-button" />}
        </div>
        <div
          className="action"
          onClick={this.logOut}
        >
          <div className="log-out" />
        </div>
      </div>
    );
  }

  onOpenPackageModal(e) {
    console.log("im here");
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openPackageModal: true });
  }

  onOpenChangeStateModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openChangeStateModal: true });
  }

  loadCopropietariesOption() {
    return getCopropietaries(this.state.adminCondoInfo.condo_id)
      .then((data) => {
        const transformedData = data.map((copropietary) => {
          console.log(copropietary);
          // Esto es para cada condo. Recordar que es una transformación
          return { label: copropietary.copropietary.name, value: copropietary.copropietary.user_id };
        });
        return {
          options: transformedData,
        };
      });
  }

  onCopropietarySelectedChange(option) {
    const data = option;
    this.setState({
      copropietarySelected: data,
    });
  }

  onDescriptionPackageChange(event) {
    const data = event.target.value;
    this.setState({
      descriptionPackageInput: data,
    });
  }

  onReceivedNamePackageChange(event) {
    const data = event.target.value;
    this.setState({
      receivedNameInput: data,
    });
  }

  onRetrivedNamePackageChange(event) {
    const data = event.target.value;
    this.setState({
      retrivedNameInput: data,
    });
  }

  registerPackage(userId, newPackage) {
    console.log(newPackage);
    if (newPackage.description === "" || newPackage.received_by_name === "") {
      this.setState({
        succesfulNewPackage: 2,
      });
    } else {
      registerPackage(userId, newPackage)
        .then((response) => {
          this.setState({
            succesfulNewPackage: 1,
          });
        })
        .catch((error) => {
          this.setState({
            succesfulNewPackage: 2,
          });
        });
    }
  }

  changeStatePackage(packageId, changedPackage) {
    changeStatePackage(packageId, changedPackage)
      .then((response) => {
        this.setState({
          succesfulChangedState: 1,
        });
      })
      .catch((error) => {
        this.setState({
          succesfulChangedState: 2,
        });
      });
  }

  onNextStepPackageModal(nextStep) {
    this.setState({
      stepPackageModal: nextStep,
    });
  }

  onNextStepChangeStateModal(nextStep) {
    this.setState({
      stepChangeStatePackageModal: nextStep,
    });
  }

  render() {

    let packagesListCards;
    if (is.empty(this.state.packagesList)) {
      packagesListCards = (
        <div className="no-packages-container">
          No existen encomiendas registradas aún.
        </div>
      );
    } else {
      packagesListCards = this.state.packagesList.filter((item) => {
        return this.state.queryPackages.length > 0 ? item.copro_name.toLowerCase().includes(this.state.queryPackages.toLowerCase()) : true
      }).map((item, index) => {
        let divPackageCard;

        const dateRetrivedMoment = moment(item.retrived_date);
        if (item.state === 0) {
          divPackageCard = (
            <div className="containers">
              <div className="package-left">
                <div className="copropietary-container">
                  <div className="copropietary">
                    {item.copro_name}
                  </div>
                </div>
                <div className="state-container">
                  <div className="state">
                    En Recepción
                  </div>
                </div>
                <div className="description-container">
                  <div className="description">
                    {item.description}
                  </div>
                </div>
                <div className="info-container">
                  <div className="line-container">
                    <div className="icon-container">
                      <img
                        alt="Ícono"
                        className="icon"
                        src={require("../../../assets/imgs/user-login.svg")}
                      />
                    </div>
                    <div className="name">
                      {item.received_by_name} el {item.arrived_date}
                    </div>
                  </div>
                </div>
              </div>
              <div className="package-right">
                <div className="icon-package">
                  <img
                    alt="Ícono"
                    className="icon"
                    src={require("../../../assets/imgs/package.svg")}
                  />
                </div>
                <div className="button-container">
                  <button
                    className="button-package"
                    onClick={(e) =>{
                      this.onOpenChangeStateModal(e);
                      this.setState({
                        selectedPackageCard: item.id,
                      })
                    }}
                  >Retirar</button>
                </div>
              </div>
            </div>
          );
        } else {
          divPackageCard = (
            <div className="containers">
              <div className="package-left">
                <div className="copropietary-container">
                  <div className="copropietary">
                    {item.copro_name}
                  </div>
                </div>
                <div className="state-container">
                  <div className="state">
                    Retirado
                  </div>
                </div>
                <div className="info-container">
                  <div className="line-container">
                    <div className="icon-container">
                      <img
                        alt="Ícono"
                        className="icon"
                        src={require("../../../assets/imgs/user-login.svg")}
                      />
                    </div>
                    <div className="name">
                      Retirado por {item.retrived_by_name} el {dateRetrivedMoment.format('DD-MM-YYYY')}
                    </div>
                  </div>
                </div>
              </div>
              <div className="package-right">
                <div className="icon-package">
                  <img
                    alt="Ícono"
                    className="icon"
                    src={require("../../../assets/imgs/package.svg")}
                  />
                </div>
              </div>
            </div>
          );
        }

        return (
          <div
            key={index}
            className="package-card"
          >
            {divPackageCard}
          </div>
        );
      });
    }


    let messageChangeStatePackageResponse;
    if (this.state.succesfulChangedState === 1) {
      messageChangeStatePackageResponse = '¡Se ha cambiado el estado del paquete con éxito!';
    } else if (this.state.succesfulChangedState === 2) {
      messageChangeStatePackageResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentChangeStatePackageModal;
    switch (this.state.stepChangeStatePackageModal) {
      case 1: {
        contentChangeStatePackageModal = (
          <div className="package-modal-container">
            <div className="sub-title-container">
              <div className="sub-title">
                ¿Quién retira este paquete?
              </div>
            </div>
            <div className="form-line">
              <div className="line-title">
                Porfavor, escribe el nombre:
              </div>
              <input
                type="text"
                onChange={this.onRetrivedNamePackageChange}
                value={this.state.retrivedNameInput}
                className="input-form"
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  var date = new Date();
                  var momentObject = moment(date);
                  const object = {
                    retrived_date: momentObject.format('YYYY-MM-DD'),
                    retrived_by_name: this.state.retrivedNameInput,
                  };
                  this.changeStatePackage(this.state.selectedPackageCard, object);
                  this.onNextStepChangeStateModal(2);
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentChangeStatePackageModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageChangeStatePackageResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getPackages(this.state.adminCondoInfo.condo_id)
                    .then((packagesList) => {
                      this.setState({
                        packagesList: packagesList,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    let messageNewPackageResponse;
    if (this.state.succesfulNewPackage === 1) {
      messageNewPackageResponse = '¡Se ha registrado el paquete con éxito!';
    } else if (this.state.succesfulNewPackage === 2) {
      messageNewPackageResponse = 'Ha ocurrido un error, por favor verifica que todos los datos están correctos.';
    }

    let contentAddPackageModal;
    switch (this.state.stepPackageModal) {
      case 1: {
        contentAddPackageModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                ¡Registra los paquetes que lleguen para los copropietarios!
              </div>
              <div className="sub-title">
                Ellos se enterarán a través de la aplicación
              </div>
            </div>
            <div className="icon-package-modal">
              <img
                alt="Ícono"
                className="icon"
                src={require("../../../assets/imgs/delivery-man.svg")}
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => { this.onNextStepPackageModal(2); }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentAddPackageModal = (
          <div className="package-modal-container">
            <div className="sub-title-container">
              <div className="sub-title">
                Completa los siguientes datos:
              </div>
            </div>
            <div className="form-line">
              <div className="line-title">
                Selecciona Copropietario:
              </div>
              <Async
                name="form-field-name"
                loadOptions={this.loadCopropietariesOption}
                searchable={true}
                searchPromptText="No se ha encontrado copropietario"
                onChange={this.onCopropietarySelectedChange}
                value={this.state.copropietarySelected}
                placeholder="Copropietario..."
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                ¿Quién registró el paquete?:
              </div>
              <input
                type="text"
                onChange={this.onReceivedNamePackageChange}
                value={this.state.receivedNameInput}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Escriba una pequeña descripción:
              </div>
              <input
                type="text"
                onChange={this.onDescriptionPackageChange}
                value={this.state.descriptionPackageInput}
                className="input-form"
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  var date = new Date();
                  var momentObject = moment(date);
                  const object = {
                    state: 0,
                    arrived_date: momentObject.format('YYYY-MM-DD'),
                    description: this.state.descriptionPackageInput,
                    received_by_name: this.state.receivedNameInput,
                  };
                  this.registerPackage(this.state.copropietarySelected.value, object);
                  this.onNextStepPackageModal(3);
                }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 3: {
        contentAddPackageModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageNewPackageResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getPackages(this.state.adminCondoInfo.condo_id)
                    .then((packagesList) => {
                      this.setState({
                        packagesList: packagesList,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    console.log(this.state);
    return (
      <div className="condo-view">
        <Modal
          classNames={{
            modal: 'custom-package-modal',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openPackageModal}
          onClose={this.onCloseModal}
          little
        >
          {contentAddPackageModal}
        </Modal>
        <Modal
          classNames={{
            modal: 'custom-package-modal',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openChangeStateModal}
          onClose={this.onCloseModal}
          little
        >
          {contentChangeStatePackageModal}
        </Modal>
        <div
          className="add-package-container"
          onClick={this.onOpenPackageModal}
        >
          <img
            alt="Agregar Paquete"
            className="add-package"
            src={require("../../../assets/imgs/plus-black-symbol.svg")}
          />
        </div>
        {this.renderTopBar()}
        <div className="packages-cards-container">
          {packagesListCards}
        </div>
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}


export default HomePackagesAdminCondComponent;
