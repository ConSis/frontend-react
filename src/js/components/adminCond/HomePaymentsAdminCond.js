import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withRouter from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import is from 'is_js';
import moment from 'moment';

import history from '../../utils/historyHelper';
import {
  getAdminCondoInfo,
  getPaymentDetails,
  sendNewDetail,
  getPaymentDetail,
  editPaymentDetail,
  destroyPaymentDetail,
  publishPayment } from '../../api/adminCondoApi';

moment.locale('es');

class HomePaymentsAdminCond extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detailsList: [],
      selectedPaymentDetail: {},
      paymentId: '',
      openDetailsModal: false,
      openAddPaymentModal: false,
      openConfirmationPublishPaymentModal: false,
      selectedDetailCard: '',
      newDescriptionPayment: '',
      newTotalAmountPayment: '',
      successfulNewDetail: 0,
      successfulPublishPayment: 0,
      stepAddNewDetailModal: 1,
      stepPublishPaymentModal: 1,
      selectedDetailIndexCard: 0,
      editPaymentItemSelected: false,
    };

    this.onCloseModal = this.onCloseModal.bind(this);
    this.onOpenDetailsModal = this.onOpenDetailsModal.bind(this);
    this.onOpenAddPaymentModal = this.onOpenAddPaymentModal.bind(this);
    this.onConfirmationPublishPaymentModal = this.onConfirmationPublishPaymentModal.bind(this);
    this.onNextStepModal = this.onNextStepModal.bind(this);
    this.sendNewDetail = this.sendNewDetail.bind(this);

    this.onDescriptionPaymentChange = this.onDescriptionPaymentChange.bind(this);
    this.onTotalAmountPaymentChange = this.onTotalAmountPaymentChange.bind(this);
    this.onSelectedPaymentDetailDescrpitionChange = this.onSelectedPaymentDetailDescrpitionChange.bind(this);
    this.onSelectedPaymentDetailAmountChange = this.onSelectedPaymentDetailAmountChange.bind(this);

    this.goBack = this.goBack.bind(this);
    this.logOut = this.logOut.bind(this);
  }

  componentDidMount() {
    const { paymentId } = this.props.match.params;
    getAdminCondoInfo()
      .then((adminInfoFromApi) => {
        this.setState({
          adminCondoInfo: adminInfoFromApi,
          paymentId: paymentId,
        });
        getPaymentDetails(this.state.paymentId)
          .then((detailsFromApi) => {
            this.setState({
              detailsList: detailsFromApi,
            });
          });
      });
  }

  goBack() {
    this.props.history.goBack();
  }

  logOut() {
    if (window.cordova) {
      navigator.notification.confirm(
        '¿Está seguro que desea cerrar la sesión?',
        (buttonIndex) => {
          if (buttonIndex === 2) {
            localStorage.clear();
            this.props.history.replace('/auth');
          }
        },
        'Confirmación',
        ['Cancelar', 'Aceptar'],
      );
    } else {
      localStorage.clear();
      this.props.history.replace('/auth');
    }
  }

  renderTopBar() {
    const shouldShowBackButton = true;
    return (
      <div className="app-top-bar" style={{ position: 'fixed' }}>
        <div
          className="action"
          onClick={this.goBack}
        >
          {shouldShowBackButton && <div className="back-button" />}
        </div>
        <div
          className="action"
          onClick={this.logOut}
        >
          <div className="log-out" />
        </div>
      </div>
    );
  }

  /*MODALS*/
  onCloseModal() {
    this.setState({
      openDetailsModal: false,
      openAddPaymentModal: false,
      selectedDetailCard: '',
      newDescriptionPayment: '',
      newTotalAmountPayment: '',
      successfulNewDetail: 0,
      successfulPublishPayment: 0,
      stepAddNewDetailModal: 1,
      selectedDetailIndexCard: 1,
      stepPublishPaymentModal: 1,
      editPaymentItemSelected: false,
      selectedPaymentDetail: {},
      openConfirmationPublishPaymentModal: false,
    });
  }

  onOpenDetailsModal(e, index, cardId) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({
      openDetailsModal: true,
      selectedDetailIndexCard: index,
      selectedDetailCard: cardId,
    });
    getPaymentDetail(cardId)
      .then((response) => {
        this.setState({
          selectedPaymentDetail: response,
        });
      });
  }

  onOpenAddPaymentModal() {
    this.setState({
      openAddPaymentModal: true,
    });
  }

  onConfirmationPublishPaymentModal() {
    this.setState({
      openConfirmationPublishPaymentModal: true,
    });
  }

  onNextStepModal(nextStep) {
    this.setState({
      stepAddNewDetailModal: nextStep,
    });
  }
  /*ENDMODALS*/

  onDescriptionPaymentChange(event) {
    const data = event.target.value;
    this.setState({
      newDescriptionPayment: data,
    });
  }

  onTotalAmountPaymentChange(event) {
    const data = event.target.value;
    this.setState({
      newTotalAmountPayment: data,
    });
  }

  onSelectedPaymentDetailDescrpitionChange(event) {
    const data = event.target.value;
    const paymentDetailObject = Object.assign({}, this.state.selectedPaymentDetail);
    paymentDetailObject.description = data;
    this.setState({ selectedPaymentDetail: paymentDetailObject });
  }

  onSelectedPaymentDetailAmountChange(event) {
    const data = event.target.value;
    const paymentDetailObject = Object.assign({}, this.state.selectedPaymentDetail);
    paymentDetailObject.amount = data;
    this.setState({ selectedPaymentDetail: paymentDetailObject });
  }

  sendNewDetail(paymentId, newDetail) {
    sendNewDetail(paymentId, newDetail)
      .then((response) => {
        this.setState({ successfulNewDetail: 1 });
      })
      .catch((error) => {
        this.setState({ successfulNewDetail: 2 });
      });
  }

  render() {

    let messageAddNewDetailResponse;
    if (this.state.successfulNewDetail === 1) {
      messageAddNewDetailResponse = '¡Se ha creado el cobro exitosamente!';
    } else if (this.state.successfulNewDetail === 2) {
      messageAddNewDetailResponse = 'Ha ocurrido un problema, por favor intentálo de nuevo mas tarde';
    }

    let contentAddPaymentModal;
    switch (this.state.stepAddNewDetailModal) {
      case 1: {
        contentAddPaymentModal = (
          <div className="add-payment-modal">
            <div className="title-container">
              <div className="title">
                Registra el gasto hecho en el condominio.
              </div>
            </div>
            <div className="form-line">
              <div className="static">
                Detalle:
              </div>
              <input
                type="text"
                onChange={this.onDescriptionPaymentChange}
                value={this.state.newDescriptionPayment}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="static">
                Monto Total:
              </div>
              <input
                type="number"
                onChange={this.onTotalAmountPaymentChange}
                value={this.state.newTotalAmountPayment}
                className="input-form"
              />
            </div>
            <div className="button-container">
              <div
                className="save-button"
                onClick={() => {
                  const object = {
                    description: this.state.newDescriptionPayment,
                    amount: this.state.newTotalAmountPayment,
                  };
                  this.sendNewDetail(this.state.paymentId, object);
                  this.onNextStepModal(2);
                }}
              >
                Guardar
              </div>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentAddPaymentModal = (
          <div className="add-payment-modal">
            <div className="title-response-container">
              <div className="title">
                {messageAddNewDetailResponse}
              </div>
            </div>
            <div className="button-container">
              <div
                className="save-button"
                onClick={() => {
                  getPaymentDetails(this.state.paymentId)
                    .then((response) => {
                      this.setState({
                        detailsList: response,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </div>
            </div>
          </div>
        )
        break;
      }
    }

    let contentDetailSelectedModal;
    if (this.state.editPaymentItemSelected === false) {
      contentDetailSelectedModal = (
        <div className="detail-payment-modal">
          <div className="detail-line">
            <div className="static">
              Detalle:
            </div>
            {!is.empty(this.state.selectedPaymentDetail) && (
              <div className="dynamic">
                {this.state.selectedPaymentDetail.description}
              </div>
            )}
          </div>
          <div className="detail-line">
            <div className="static">
              Monto Total:
            </div>
            {!is.empty(this.state.selectedPaymentDetail) && (
              <div className="dynamic">
                {this.state.selectedPaymentDetail.amount}
              </div>
            )}
          </div>
          <div className="buttons-container">
            <div className="edit-button-container">
              <div
                className="edit-button"
                onClick={() => {
                  this.setState({
                    editPaymentItemSelected: true,
                  });
                }}
              >
                Editar
              </div>
            </div>
            <div className="delete-button-container">
              <div
                className="delete-button"
                onClick={() => {
                  destroyPaymentDetail(this.state.selectedDetailCard)
                    .then((response) => {
                      getPaymentDetails(this.state.paymentId)
                        .then((response) => {
                          this.setState({
                            detailsList: response,
                          });
                          this.onCloseModal();
                        });
                    });
                }}
              >
                Eliminar
              </div>
            </div>
          </div>
        </div>
      );
    } else if (this.state.editPaymentItemSelected === true) {
      contentDetailSelectedModal = (
        <div className="detail-payment-modal">
          <div className="title-container">
            <div className="title">
              Editar cobro:
            </div>
          </div>
          <div className="detail-line">
            <div className="static">
              Detalle:
            </div>
            {!is.empty(this.state.selectedPaymentDetail) && (
              <div className="dynamic">
                <input
                  type="text"
                  onChange={this.onSelectedPaymentDetailDescrpitionChange}
                  value={this.state.selectedPaymentDetail.description}
                  className="input-form"
                />
              </div>
            )}
          </div>
          <div className="detail-line">
            <div className="static">
              Monto:
            </div>
            {!is.empty(this.state.selectedPaymentDetail) && (
              <div className="dynamic">
                <input
                  type="text"
                  onChange={this.onSelectedPaymentDetailAmountChange}
                  value={this.state.selectedPaymentDetail.amount}
                  className="input-form"
                />
              </div>
            )}
          </div>
          <div className="buttons-container">
            <div className="edit-button-container">
              <div
                className="edit-button"
                onClick={() => {
                  this.setState({
                    editPaymentItemSelected: false,
                  });
                  editPaymentDetail(this.state.selectedPaymentDetail.id, this.state.selectedPaymentDetail)
                    .then((response) => {
                      getPaymentDetails(this.state.paymentId)
                        .then((response) => {
                          this.setState({
                            detailsList: response,
                          });
                        });
                    });
                }}
              >
                Guardar
              </div>
            </div>
          </div>
        </div>
      );
    }

    const detailsListCard = this.state.detailsList.map((item, index) => {
      return (
        <div
          key={index}
          className="details-card"
        >
          <div className="description-container">
            <div className="description">
              {item.payment_detail.description}
            </div>
          </div>
          <div className="amount-container">
            <div className="amount">
              $ {item.total_amount_per_copro.total}
            </div>
          </div>
          <div className="total-amount-container">
            <div className="total-amount">
              $ {item.payment_detail.amount}
            </div>
          </div>
          <div
            className="options-container"
            onClick={(e) => {
              //get payment details
              this.onOpenDetailsModal(e, index, item.payment_detail.id);
            }}
          >
            <img
              alt="more"
              className="more-options"
              src={require("../../../assets/imgs/more.svg")}
            />
          </div>
        </div>
      );
    });

    let messagePublishPaymentResponse;
    if (this.state.successfulPublishPayment === 1) {
      messagePublishPaymentResponse = '¡Se ha publicado el cobro mensual para los copropietarios con éxito!';
    } else if (this.state.successfulPublishPayment === 2) {
      messagePublishPaymentResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde';
    }

    let contentPublishPaymentModal;
    switch (this.state.stepPublishPaymentModal) {
      case 1: {
        contentPublishPaymentModal = (
          <div className="add-payment-modal">
            <div className="title-container-confirmation">
              <div className="title">
                ¿Seguro que desea publicar estos gastos comunes?
              </div>
            </div>
            <div className="sub-title-container">
              <div className="sub-title">
                Le llegara un aviso a todos los copropietarios con sus respectivos cobros.
              </div>
            </div>
            <div className="buttons-container">
              <div 
                className="button"
                onClick={this.onCloseModal}
              >
                Cancelar
              </div>
              <div 
                className="button-confirmation"
                onClick={() => {
                  publishPayment(this.state.paymentId, this.state.adminCondoInfo.condo_id)
                    .then((response) => {
                      this.setState({
                        successfulPublishPayment: 1,
                        stepPublishPaymentModal: 2,
                      });
                    })
                    .catch((error) => {
                      this.setState({
                        successfulPublishPayment: 2,
                      });
                    });
                }}
              >
                Aceptar
              </div>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentPublishPaymentModal = (
          <div className="add-payment-modal">
            <div className="title-container">
              <div className="title">
                {messagePublishPaymentResponse}
              </div>
            </div>
            <div className="button-container">
              <div
                className="save-button"
                onClick={() => {
                  this.onCloseModal();
                  this.goBack();
                }}
              >
                Aceptar
              </div>
            </div>
          </div>
        );
        break;
      }
    }

    console.log(this.state);
    return (
      <div className="views-admin-container">
        <Modal
          classNames={{
            modal: 'custom-package-modal',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openDetailsModal}
          onClose={this.onCloseModal}
          little
        >
          {contentDetailSelectedModal}
        </Modal>
        <Modal
          classNames={{
            modal: 'custom-package-modal',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openConfirmationPublishPaymentModal}
          onClose={this.onCloseModal}
          little
        >
          {contentPublishPaymentModal}
        </Modal>
        <Modal
          classNames={{
            modal: 'custom-package-modal',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openAddPaymentModal}
          onClose={this.onCloseModal}
          little
        >
          {contentAddPaymentModal}
        </Modal>
        <div className="payments-container">
          <div className="titles-container">
            <div className="description">
              Detalle
            </div>
            <div className="amount">
              Total/Cop.
            </div>
            <div className="total">
              Total
            </div>
          </div>
          <div className="payments-cards-container">
            {detailsListCard}
          </div>
          <div className="add-more-payments-container">
            <div
              className="add-more-payments-button"
              onClick={() => {
                this.onOpenAddPaymentModal();
              }}
            >
              +
            </div>
          </div>
          <div className="publish-button-container">
            <div
              className="publish-button"
              onClick={() => {
                this.onConfirmationPublishPaymentModal();
              }}
            >
              Publicar
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}


export default HomePaymentsAdminCond;
