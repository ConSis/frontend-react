import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Modal from 'react-responsive-modal';
import moment from 'moment';
import is from 'is_js';

import history from '../../utils/historyHelper';
import {
  getAdminCondoInfo,
  getVisits,
  destroyVisit } from '../../api/adminCondoApi';

moment.locale('es');

class HomeVisitsAdminCondComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDestroyVisitModal: false,
      selectedVisitCard: '',
      stepRemoveVisitModal: 1,

      successfulDestroyVisit: 0,

      adminCondoInfo: '',
      transferInfo: '',
      visitsList: [],
      query: '',
    };

    this.onOpenDestroyVisitModal = this.onOpenDestroyVisitModal.bind(this);

    this.onCloseModal = this.onCloseModal.bind(this);
    this.onNextStepRemoveVisitModal = this.onNextStepRemoveVisitModal.bind(this);
    this.onNextStepModal = this.onNextStepModal.bind(this);

    this.destroyVisit = this.destroyVisit.bind(this);
    this.goBack = this.goBack.bind(this);
    this.logOut = this.logOut.bind(this);
  }

  componentDidMount() {
    getAdminCondoInfo()
      .then((adminInfoFromApi) => {
        this.setState({
          adminCondoInfo: adminInfoFromApi,
        });
        getVisits(this.state.adminCondoInfo.condo_id)
          .then((visitsList) => {
            this.setState({
              visitsList: visitsList,
            });
          });
      });
  }

  /*MODALS*/
  onOpenDestroyVisitModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openDestroyVisitModal: true });
  }

  onCloseModal() {
    this.setState({
      openDestroyVisitModal: false,
      stepRemoveVisitModal: 1,
      successfulDestroyVisit: 0,
    });
  }

  destroyVisit(visitId) {
    destroyVisit(visitId)
      .then((response) => {
        this.setState({
          successfulDestroyVisit: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulDestroyVisit: 2,
        });
      });
  }

  onNextStepRemoveVisitModal(nextStep) {
    this.setState({
      stepRemoveVisitModal: nextStep,
    });
  }

  onNextStepModal(nextStep) {
    this.setState({
      stepAddStorageTypeModal: nextStep,
      stepRemoveStorageType: nextStep,
      stepAddParkingTypeModal: nextStep,
      stepRemoveParkingType: nextStep,
      stepAddAppartmentTypeModal: nextStep,
      stepEditTransferInfo: nextStep,
    });
  }
  /*ENDMODALS*/

  goBack() {
    this.props.history.goBack();
  }

  logOut() {
    if (window.cordova) {
      navigator.notification.confirm(
        '¿Está seguro que desea cerrar la sesión?',
        (buttonIndex) => {
          if (buttonIndex === 2) {
            localStorage.clear();
            this.props.history.replace('/auth');
          }
        },
        'Confirmación',
        ['Cancelar', 'Aceptar'],
      );
    } else {
      localStorage.clear();
      this.props.history.replace('/auth');
    }
  }

  renderTopBar() {
    const shouldShowBackButton = true;
    return (
      <div className="app-top-bar" style={{ position: 'fixed' }}>
        <div
          className="action"
          onClick={this.goBack}
        >
          {shouldShowBackButton && <div className="back-button" />}
        </div>
        <div
          className="action"
          onClick={this.logOut}
        >
          <div className="log-out" />
        </div>
      </div>
    );
  }

  render() {

    let visitsListCards;
    if (is.empty(this.state.visitsList)) {
      visitsListCards = (
        <div className="no-visits-container">
          Los copropietarios no han registrado visitas aún.
        </div>
      );
    } else {
      visitsListCards = this.state.visitsList.filter((item) => {
        return this.state.query.length > 0 ? item.name.toLowerCase().includes(this.state.query.toLowerCase()) : true
      }).map((item, index) => {
        let divs;
        if (item.plate !== null) {
          divs = (
            <div className="containers">
              <div className="left-container">
                <div className="line-container">
                  <div className="name">
                    {item.name}
                  </div>
                </div>
                <div className="line-container">
                  <div className="rut">
                    {item.rut}
                  </div>
                </div>
                <div className="line-container">
                  <div className="plate">
                    {item.plate}
                  </div>
                </div>
                <div className="line-container">
                  <div className="icon-copro">
                    <img
                      alt="Ícono"
                      className="icon"
                      src={require("../../../assets/imgs/user-login.svg")}
                    />
                  </div>
                  <div className="copropietary">
                    {item.copropietary_name}
                  </div>
                </div>
              </div>
              <div className="right-container">
                <div className="icon-car-container">
                  <img
                    alt="Ícono"
                    className="icon"
                    src={require("../../../assets/imgs/car-white.svg")}
                  />
                </div>
                <div className="button-container">
                  <button
                    className="button-visit"
                    onClick={(e) => {
                      this.setState({
                        selectedVisitCard: item.id,
                      });
                      this.onOpenDestroyVisitModal(e);
                    }}
                  >Ingresar</button>
                </div>
              </div>
            </div>
          );
        } else {
          divs = (
            <div className="containers">
              <div className="left-container">
                <div className="line-container">
                  <div className="name">
                    {item.name}
                  </div>
                </div>
                <div className="line-container">
                  <div className="rut">
                    {item.rut}
                  </div>
                </div>
                <div className="line-container">
                  <div className="icon-copro">
                    <img
                      alt="Ícono"
                      className="icon"
                      src={require("../../../assets/imgs/user-login.svg")}
                    />
                  </div>
                  <div className="copropietary">
                    {item.copropietary_name}
                  </div>
                </div>
              </div>
              <div className="right-container">
                <div className="icon-car-container">
                  <img
                    alt="Ícono"
                    className="icon"
                    src={require("../../../assets/imgs/walkin-white.svg")}
                  />
                </div>
                <div className="button-container">
                  <button
                    className="button-visit"
                    onClick={(e) => {
                      this.setState({
                        selectedVisitCard: item.id,
                      });
                      this.onOpenDestroyVisitModal(e);
                    }}
                  >Ingresar</button>
                </div>
              </div>
            </div>
          );
        }

        return (
          <div
            key={index}
            className="visit-card"
          >
            {divs}
          </div>
        );
      });
    }

    let messageDestroyVisitResponse;
    if (this.state.successfulDestroyVisit === 1) {
      messageDestroyVisitResponse = '¡Se ha borrado visita con éxito!';
    } else if (this.state.successfulDestroyVisit === 2) {
      messageDestroyVisitResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentDestroyVisitModal;
    switch (this.state.stepRemoveVisitModal) {
      case 1: {
        contentDestroyVisitModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                ¿Seguro que quieres registrar esta visita?
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  this.destroyVisit(this.state.selectedVisitCard);
                  this.onNextStepRemoveVisitModal(2);
                }}
              >
                Confirmar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentDestroyVisitModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageDestroyVisitResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getVisits(this.state.adminCondoInfo.condo_id)
                    .then((visitsList) => {
                      this.setState({
                        visitsList: visitsList,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    console.log(this.state);
    return (
      <div className="condo-view">
        <Modal
          classNames={{
            modal: 'custom-package-modal',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openDestroyVisitModal}
          onClose={this.onCloseModal}
          little
        >
          {contentDestroyVisitModal}
        </Modal>
        {this.renderTopBar()}
        <div className="visits-cards-container">
          {visitsListCards}
        </div>
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}


export default HomeVisitsAdminCondComponent;
