import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

class BlackboardCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="blackboard-card">
        <div className="image-service-container">
        </div>
        <div className="body-container">
          <div className="category-container">
            <div className="category">
              {this.props.serviceCategory}
            </div>
          </div>
          <div className="title-container">
            <div className="title">
              {this.props.title}
            </div>
          </div>
          <div className="message-container">
            <div className="message">
              {this.props.message}
            </div>
          </div>
          <div className="published-by-container">
            <div className="profile-pic-container">
            </div>
            <div className="published-by-name">
              {this.props.publishedByName}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BlackboardCard.defaultProps = {
  image: null,
  serviceCategory: null,
  title: null,
  message: null,
  profilePic: null,
  publishedByName: null,
};

export default BlackboardCard;