import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import arrowUpIcon from '../../../assets/imgs/arrow-up-white.svg';
import arrowDownIcon from '../../../assets/imgs/arrow-down-white.svg';

class ComplaintCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.onVoteUp = this.onVoteUp.bind(this);
    this.onVoteDown = this.onVoteDown.bind(this);
  }

  onVoteUp(props) {
    this.props.onVoteUp();
  }

  onVoteDown() {
    this.props.onVoteDown();
  }

  render() {
    return (
      <div className="complaint-card">
        <div className="category-container">
          <div className="category">
            {this.props.category}
          </div>
        </div>
        <div className="info-member-container">
          <div className="info-member">
            <div className="profile-pic-container">
            </div>
            <div className="name-container">
              <div className="name">
                {this.props.name}
              </div>
            </div>
          </div>
        </div>
        <div className="message-container">
          <div className="message">
            {this.props.complaintMessage}
          </div>
        </div>
        <div className="votes-container">
          <div
            className="vote-down"
            onClick={this.onVoteDown}
          >
            <img
              alt="Voto Down"
              className="votes-class"
              src={arrowDownIcon}
            />
          </div>
          <div className="count">
            {this.props.countVotes}
          </div>
          <div
            className="vote-up"
            onClick={this.onVoteUp}
          >
            <img
              alt="Voto Up"
              className="votes-class"
              src={arrowUpIcon}
            />
          </div>
        </div>
      </div>
    );
  }
}

ComplaintCard.defaultProps = {
  category: null,
  profilePic: null,
  name: null,
  complaintMessage: null,
  countVotes: null,
};

export default ComplaintCard;