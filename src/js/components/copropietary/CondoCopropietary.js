import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import is from 'is_js';

import history from '../../utils/historyHelper';
import { Carousel } from 'react-responsive-carousel';
import TabBarCondo from '../../components/TabBarCondo';
import StaffCard from '../copropietary/StaffCard';
import { getCopropietaryInfo, getEmployees, getHiredServices } from '../../api/copropietaryApi';

class CondoCopropietary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copropietaryInfo: '',
      employeesList: [],
      hiredServices: { appartment_type: {}, parkings: [], storages: [] },
    };
  }

  componentDidMount() {
    getCopropietaryInfo()
      .then((copropietaryInfoFromApi) => {
        this.setState({
          copropietaryInfo: copropietaryInfoFromApi,
        });
        getEmployees(this.state.copropietaryInfo.condo_id)
          .then((response) => {
            this.setState({
              employeesList: response,
            });
          });
        getHiredServices(this.state.copropietaryInfo.id)
          .then((response) => {
            this.setState({
              hiredServices: response,
            });
          });
      });
  }

  render() {
    const staffCards = this.state.employeesList.map((staffCard) =>{
      return (
        <StaffCard
          key={staffCard.id}
          nameStaff={staffCard.name}
          chargeStaff={staffCard.charge}
        />
      );
    });

    const houseCard = (
      <div className="house-card">
        <div className="left-container">
          <img
            alt="Placeholder"
            className="house"
            src={require("../../../assets/imgs/house-color.svg")}
          />
        </div>
        <div className="right-container">
          <div className="line-container">
            <div className="static-container">
              Dirección:
            </div>
            <div className="dynamic-container">
              {this.state.hiredServices.appartment_type.address}
            </div>
          </div>
          <div className="line-container">
            <div className="static-container">
              Numeración:
            </div>
            <div className="dynamic-container">
              {this.state.hiredServices.appartment_type.number}
            </div>
          </div>
          <div className="line-container">
            <div className="static-container">
              Tipo Apartamento:
            </div>
            <div className="dynamic-container">
              {this.state.hiredServices.appartment_type.appartment_type_name}
            </div>
          </div>
          <div className="line-container">
            <div className="static-container">
              Costo Apartamento:
            </div>
            <div className="dynamic-container">
              $ {
                is.existy(this.state.hiredServices.appartment_type) && is.existy(this.state.hiredServices.appartment_type.appartment_type_cost) &&
                 this.state.hiredServices.appartment_type.appartment_type_cost.toLocaleString('es-IN')
              }
            </div>
          </div>
        </div>
      </div>
    );

    let parkingsCards;
    if (is.empty(this.state.hiredServices.parkings)) {
      parkingsCards = (
        <div className="no-parkings">
          No tienes contratado ningún estacionamiento.
        </div>
      );
    } else if (!is.empty(this.state.hiredServices.parkings)) {
      parkingsCards = this.state.hiredServices.parkings.map((item, index) => {
        return (
          <div className="parking-card">
            <div className="left-container">
              <img
                alt="Placeholder"
                className="parking"
                src={require("../../../assets/imgs/parking-color.svg")}
              />
            </div>
            <div className="right-container">
              <div className="line-container">
                <div className="static-container">
                  Nombre:
                </div>
                <div className="dynamic-container">
                  {item.name}
                </div>
              </div>
              <div className="line-container">
                <div className="static-container">
                  Tipo Estacionamiento:
                </div>
                <div className="dynamic-container">
                  {item.parking_type_name}
                </div>
              </div>
              <div className="line-container">
                <div className="static-container">
                  Costo:
                </div>
                <div className="dynamic-container">
                  $ {item.parking_type_cost.toLocaleString('es-IN')}
                </div>
              </div>
            </div>
          </div>
        );
      });
    }

    let storagesCard;
    if (is.empty(this.state.hiredServices.storages)) {
      storagesCard = (
        <div className="no-storages">
          No tienes contratado ninguna bodega.
        </div>
      );
    } else if (!is.empty(this.state.hiredServices.storages)) {
      storagesCard = this.state.hiredServices.storages.map((item, index) => {
        return (
          <div className="storage-card">
            <div className="left-container">
              <img
                alt="Placeholder"
                className="storage"
                src={require("../../../assets/imgs/warehouse-color.svg")}
              />
            </div>
            <div className="right-container">
              <div className="line-container">
                <div className="static-container">
                  Nombre:
                </div>
                <div className="dynamic-container">
                  {item.name}
                </div>
              </div>
              <div className="line-container">
                <div className="static-container">
                  Tipo Estacionamiento:
                </div>
                <div className="dynamic-container">
                  {item.storage_type_name}
                </div>
              </div>
              <div className="line-container">
                <div className="static-container">
                  Costo:
                </div>
                <div className="dynamic-container">
                  $ {item.storage_type_cost.toLocaleString('es-IN')}
                </div>
              </div>
            </div>
          </div>
        );
      });
    }

    const viewss = [
      {
        text: 'General',
        textClass: 'text-condo-tab',
        content: (
          <div className="info-tab-condo">
            <div className="condo-info-container">
              <div className="cards-container">
                {houseCard}
                {parkingsCards}
                {storagesCard}
              </div>
            </div>
          </div>
        ),
      },
      {
        text: 'Reglamento',
        textClass: 'text-condo-tab',
        content: (
          <div className="info-tab-condo">
            <div className="condo-info-container">
              <div className="rules-container">
                <div className="rules-title-container">
                  <div className="title">
                    REGLAMENTO
                  </div>
                </div>
                <div className="rules-info">
                  <div className="message">
                    Es de suma importancia seguir este reglamento, para poder así vivir todos en armonía.
                    Agradecemos bajar y leer este reglamento para que conozcas tus derechos y responsabilidades.
                  </div>
                  <div className="download-container">
                    Presione aquí para descargar PDF
                  </div>
                </div>
              </div>
            </div>
          </div>
        ),
      },
      {
        text: 'Staff',
        textClass: 'text-condo-tab',
        content: (
          <div className="info-tab-condo">
            <div className="condo-info-container">
              <div className="staff-cards-list">
                {staffCards}
              </div>
            </div>
          </div>
        ),
      },
    ];
    //const { copropietaryInfo } = this.state;
    return (
      <div className="copropietary-view">
        <div className="photo-condominio-page-container">
          <div className="photo-condominio-page">
          </div>
        </div>
        <div className="gradient-container">
        </div>
        <div className="info-condominio-page">
          <TabBarCondo
            vistas={viewss}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}

export default CondoCopropietary;