import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import { Async } from 'react-select';
import is from 'is_js';

import history from '../../utils/historyHelper';
import { getAdCategories, getCopropietaryInfo, sendNewAd, getAds } from '../../api/CopropietaryApi';
import BlackboardCard from '../copropietary/BlackboardCard';

// Assets
import WalkingWithDogIcon from '../../../assets/imgs/walking-with-dog.svg';
import AddFileIcon from '../../../assets/imgs/addfile.svg';

class HomeBlackboardCopropietaryComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copropietaryInfo: '',
      openAddBlackBoard: false,
      step: 1,
      ad_category: '',
      title: '',
      message: '',
      succesfulAd: 0,
      ads: [],
    };

    this.onOpenAddBlackboardModal = this.onOpenAddBlackboardModal.bind(this);
    this.loadAdCategoriesOption = this.loadAdCategoriesOption.bind(this);
    this.onNextStep = this.onNextStep.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.onAdCategoryChange = this.onAdCategoryChange.bind(this);
    this.onTitleChange = this.onTitleChange.bind(this);
    this.onMessageChange = this.onMessageChange.bind(this);
    this.sendNewAd = this.sendNewAd.bind(this);
  }

  componentDidMount() {
    getCopropietaryInfo()
      .then((copropietaryInfoFromApi) => {
        this.setState({
          copropietaryInfo: copropietaryInfoFromApi,
        });
        getAds(this.state.copropietaryInfo.condo_id)
          .then((adsFromApi) => {
            this.setState({
              ads: adsFromApi,
            });
          });
      });
  }

  onOpenAddBlackboardModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openAddBlackBoard: true });
  }

  onAdCategoryChange(option) {
    const data = option;
    this.setState({
      ad_category: data,
    });
  }

  onTitleChange(event) {
    const data = event.target.value;
    this.setState({
      title: data,
    });
  }

  onMessageChange(event) {
    const data = event.target.value;
    this.setState({
      message: data,
    });
  }

  onCloseModal() {
    this.setState({
      openAddBlackBoard: false,
      step: 1,
      title: '',
      message: '',
      succesfulAd: 0,
      ad_category: '',
    });
  }

  onNextStep(nextStep) {
    this.setState({
      step: nextStep,
    });
  }

  loadAdCategoriesOption() {
    return getAdCategories(this.state.copropietaryInfo.condo_id)
      .then((data) => {
        const transformedData = data.map((category) => {
          // Esto es para cada condo. Recordar que es una transformación
          return { label: category.name, value: category.id };
        });

        return {
          options: transformedData,
        };
      });
  }

  sendNewAd(ad) {
    sendNewAd(ad)
      .then((response) => {
        this.setState({
          succesfulAd: 1,
        });
      })
      .catch((error) => {
        this.setState({
          succesfulAd: 2,
        });
      });
  }

  render() {

    let cards;
    if (is.empty(this.state.ads)) {
      cards = (
        <div className="no-blackboards-container">
          Aún no existen avisos en tu comunidad. ¡Publicita tu servicio!
        </div>
      );
    } else {
      cards = this.state.ads.map((card) => {
        return (
          <BlackboardCard
            key={card.id}
            serviceCategory={card.ad_category_id}
            title={card.title}
            message={card.message}
            publishedByName={card.copropietary_name}
          />
        );
      });
    }

    let messageAdBlackboard;
    if (this.state.succesfulAd === 1) {
      messageAdBlackboard = '¡Se ha registrado tu aviso! Espera a que el administrador lo apruebe';
    } else if (this.state.succesfulAd === 2) {
      messageAdBlackboard = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    const { openAddBlackBoard } = this.state;

    let contentAddBlackboardModal;
    switch (this.state.step) {
      case 1: {
        contentAddBlackboardModal = (
          <div className="add-blackboard-container">
            <div className="title">
              ¡Agrega tus avisos para que el resto de la comunidad pueda verlos!
            </div>
            <div className="icon-blackboard">
              <img
                alt="Ícono"
                className="icon-services"
                src={WalkingWithDogIcon}
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => { this.onNextStep(2); }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentAddBlackboardModal = (
          <div className="add-blackboard-container">
            <div className="sub-title">
              Completa con los siguientes datos:
            </div>
            <div className="form-line">
              <div className="line-title">
              Categoría:
              </div>
              <Async
                name="form-field-name"
                loadOptions={this.loadAdCategoriesOption}
                searchable={false}
                searchPromptText="Seleccionar..."
                onChange={this.onAdCategoryChange}
                value={this.state.ad_category}
                placeholder="Categoría..."
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Título:
              </div>
              <input
                type="text"
                onChange={this.onTitleChange}
                value={this.state.title}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Mensaje:
              </div>
              <textarea
                rows={5}
                onChange={this.onMessageChange}
                value={this.state.message}
                className="text-area-form"
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  const object = {
                    title: this.state.title,
                    message: this.state.message,
                    is_approved: false,
                    ad_category_id: this.state.ad_category.value,
                  };
                  this.sendNewAd(object);
                  this.onNextStep(3);
                }}
              >
                Enviar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 3: {
        contentAddBlackboardModal = (
          <div className="add-blackboard-container">
            <div className="title">
              {messageAdBlackboard}
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={this.onCloseModal}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }
    return (
      <div className="condo-view">
        <div className="blackboard-card-list">
          <Modal
            classNames={{
              modal: 'custom-blackboard-modal',
              closeIcon: 'custom-modal-close',
            }}
            open={openAddBlackBoard}
            onClose={this.onCloseModal}
            little
          >
            {contentAddBlackboardModal}
          </Modal>
          <div
            className="add-card-blackboard-container"
            onClick={this.onOpenAddBlackboardModal}
          >
            <img
              alt="Ícono"
              className="icon"
              src={AddFileIcon}
            />
          </div>
          {cards}
        </div>
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}

export default HomeBlackboardCopropietaryComponent;
