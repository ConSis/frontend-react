import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import is from 'is_js';

import history from '../../utils/historyHelper';
import { getCopropietaryInfo, sendNewComplaint, getComplaints, complaintVoteDown, complaintVoteUp } from '../../api/CopropietaryApi';
import ComplaintCard from '../copropietary/ComplaintCard';

// Assets
import CustomerIcon from '../../../assets/imgs/customer.svg';
import AddFileIcon from '../../../assets/imgs/addfile.svg';

class HomeComplaintsCopropietaryComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openAddComplaintModal: false,
      step: 1,
      complaintTitle: '',
      complaintMessage: '',
      succesfulComplaint: 0,
      copropietaryInfo: '',
      complaints: [],
      voteQueue: [],
    };

    this.onOpenAddComplaintModal = this.onOpenAddComplaintModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.onNextStep = this.onNextStep.bind(this);
    this.onComplaintTitleChange = this.onComplaintTitleChange.bind(this);
    this.onComplaintMessageChange = this.onComplaintMessageChange.bind(this);
    this.onVoteUp = this.onVoteUp.bind(this);
    this.onVoteDown = this.onVoteDown.bind(this);
  }

  componentDidMount() {
    getCopropietaryInfo()
      .then((copropietaryInfoFromApi) => {
        this.setState({
          copropietaryInfo: copropietaryInfoFromApi,
        });
        getComplaints(this.state.copropietaryInfo.condo_id)
          .then((complaintsFromApi) => {
            this.setState({
              complaints: JSON.parse(JSON.stringify(complaintsFromApi)),
            });
          });
      });
  }

  onOpenAddComplaintModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openAddComplaintModal: true });
  }

  onCloseModal() {
    this.setState({
      openAddComplaintModal: false,
      step: 1,
      complaintTitle: '',
      complaintMessage: '',
      succesfulComplaint: 0,
    });
  }

  onNextStep(nextStep) {
    this.setState({
      step: nextStep,
    });
  }

  onComplaintTitleChange(event) {
    const data = event.target.value;
    this.setState({
      complaintTitle: data,
    });
  }

  onComplaintMessageChange(event) {
    const data = event.target.value;
    this.setState({
      complaintMessage: data,
    });
  }

  sendNewComplaint(complaint) {
    sendNewComplaint(complaint)
      .then((response) => {
        this.setState({
          succesfulComplaint: 1,
        });
      })
      .catch((error) => {
        this.setState({
          succesfulComplaint: 2,
        });
      });
  }

  onVoteUp(actionIndex) {
    let queueList = this.state.voteQueue;
    let queueIndex = queueList.findIndex((item) => {
      return item === actionIndex;
    });
    if (queueIndex >= 0) {
      console.log('Blocked');
      return;
    }
    queueList.push(actionIndex);
    const cardList = this.state.complaints;
    cardList[actionIndex].votes += 1;
    this.setState({
      voteQueue: queueList,
      complaints: cardList,
    });
    complaintVoteUp(cardList[actionIndex].id)
      .then(() => {
        queueList = this.state.voteQueue;
        queueIndex = queueList.findIndex((item) => {
          return item === actionIndex;
        });

        if (queueIndex >= 0) {
          queueList.splice(queueIndex, 1);
        }

        this.setState({
          voteQueue: queueList,
        });
      })
      .catch((error) => {
        // Rectificación del voto en caso de error
        // 1) Se suma el voto restado anteriormente
        // 2) Se saca el item de la cola de espera
        const cardList = this.state.complaints;
        cardList[actionIndex].votes -= 1;

        queueList = this.state.voteQueue;
        queueIndex = queueList.findIndex((item) => {
          return item === actionIndex;
        });

        if (queueIndex >= 0) {
          queueList.splice(queueIndex, 1);
        }

        this.setState({
          voteQueue: queueList,
          complaints: cardList,
        })
        alert('Oops! Something went wrong while doing that :(')
      })
  }

  onVoteDown(actionIndex) {
    // Get the vote queue
    let queueList = this.state.voteQueue;

    // Is this item (actionIndex) in the list?
    // Return -1 if its not
    let queueIndex = queueList.findIndex((item) => {
      return item === actionIndex;
    });

    // Item in the queue. Block callbacks
    console.log('queueIndex', queueIndex);
    if (queueIndex >= 0) {
      console.log('Blocked');
      return;
    }

    queueList.push(actionIndex);

    const cardList = this.state.complaints;
    cardList[actionIndex].votes -= 1;


    this.setState({
      voteQueue: queueList,
      complaints: cardList,
    })

    /*new Promise((res, err) => {
      setTimeout(() => {
        res();
        //err();
      }, 6000)
    })*/

    complaintVoteDown(cardList[actionIndex].id)
      .then(() => {
        queueList = this.state.voteQueue;
        queueIndex = queueList.findIndex((item) => {
          return item === actionIndex;
        });

        if (queueIndex >= 0) {
          queueList.splice(queueIndex, 1);
        }

        this.setState({
          voteQueue: queueList,
        })
      })
      .catch((error) => {
        // Rectificación del voto en caso de error
        // 1) Se suma el voto restado anteriormente
        // 2) Se saca el item de la cola de espera
        const cardList = this.state.complaints;
        cardList[actionIndex].votes += 1;

        queueList = this.state.voteQueue;
        queueIndex = queueList.findIndex((item) => {
          return item === actionIndex;
        });

        if (queueIndex >= 0) {
          queueList.splice(queueIndex, 1);
        }

        this.setState({
          voteQueue: queueList,
          complaints: cardList,
        })
        alert('Oops! Something went wrong while doing that :(')
      })
    
  }

  render() {

    let messageAddComplaint;
    if (this.state.succesfulComplaint === 1) {
      messageAddComplaint = '¡Se ha registrado tu inconveniente, ahora espera los votos de tus vecinos!';
    } else if (this.state.succesfulAd === 2) {
      messageAddComplaint = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentAddComplaintModal;
    switch (this.state.step) {
      case 1: {
        contentAddComplaintModal = (
          <div className="complaint-modal-container">
            <div className="title-container">
              <div className="title">
                Registra si tienes algún inconveniente, y así podrás ver si a tu comunidad le afecta también.
              </div>
            </div>
            <div className="icon-complaint">
              <img
                alt="Ícono"
                className="icon"
                src={CustomerIcon}
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => { this.onNextStep(2); }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentAddComplaintModal = (
          <div className="complaint-modal-container">
            <div className="sub-title-container">
              <div className="sub-title">
                Completa con los siguientes datos:
              </div>
            </div>
            <div className="form-line">
              <div className="line-title">
                Título:
              </div>
              <input
                type="text"
                onChange={this.onComplaintTitleChange}
                value={this.state.complaintTitle}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Mensaje:
              </div>
              <textarea
                rows={5}
                onChange={this.onComplaintMessageChange}
                value={this.state.complaintMessage}
                className="text-area-form"
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  const object = {
                    title: this.state.complaintTitle,
                    message: this.state.complaintMessage,
                    votes: 0,
                  };
                  this.sendNewComplaint(object);
                  this.onNextStep(3);
                }}
              >
                Guardar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 3: {
        contentAddComplaintModal = (
          <div className="complaint-modal-container">
            <div className="title-container">
              <div className="title">
                {messageAddComplaint}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={this.onCloseModal}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    let complaintCards;
    if (is.empty(this.state.complaints)) {
      complaintCards = (
        <div className="no-complaint-cards">
          Aún no existen reclamos en tu comunidad.
        </div>
      );
    } else {
      complaintCards = this.state.complaints.map((card, index) => {
        return (
          <ComplaintCard
            key={card.id}
            category={card.title}
            name={card.copropietary_name}
            complaintMessage={card.message}
            countVotes={card.votes}
            onVoteDown={() => { this.onVoteDown(index); }}
            onVoteUp={() => { this.onVoteUp(index); }}
          />
        );
      });
    }

    const { openAddComplaintModal } = this.state;
    return (
      <div className="condo-view">
        <div className="complaint-card-list">
          <Modal
            classNames={{
              modal: 'custom-complain-modal',
              closeIcon: 'custom-modal-close',
            }}
            open={openAddComplaintModal}
            onClose={this.onCloseModal}
            little
          >
            {contentAddComplaintModal}
          </Modal>
          <div
            className="add-complaint-container"
            onClick={this.onOpenAddComplaintModal}
          >
            <img
              alt="Ícono"
              className="icon"
              src={AddFileIcon}
            />
          </div>
          {complaintCards}
        </div>
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}

export default HomeComplaintsCopropietaryComponent;