import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import is from 'is_js';

import history from '../../utils/historyHelper';
import TabBarNavigation from '../../components/TabBarNavigation';
import { Carousel } from 'react-responsive-carousel';
import { getCopropietaryInfo, sendVisitsInfo, checkPayment, changeStateUserPayment } from '../../api/copropietaryApi';

// Assets
import CommunityIcon from '../../../assets/imgs/community.svg';

class HomeCopropietaryComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copropietaryInfo: '',
      transferCode: '',
      openVisits: false,
      openNotifyPaymentModal: false,
      step: 1,
      peopleInCar: 0,
      peopleInCarList: [],
      people: 0,
      peopleList: [],
      registeredVisits: 0,
      checkPayment: { payment: {} },

      stepNotifyPaymentModal: 1,
      successfulChangeUserPaymentState: 0,
    };

    this.onOpenVisitsModal = this.onOpenVisitsModal.bind(this);
    this.onOpenNotifyPaymentModal = this.onOpenNotifyPaymentModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.onChangeStep = this.onChangeStep.bind(this);
    this.onPeopleInCarChanged = this.onPeopleInCarChanged.bind(this);
    this.onPeopleChanged = this.onPeopleChanged.bind(this);
    this.onMorePeopleInCar = this.onMorePeopleInCar.bind(this);
    this.onLessPeopleInCar = this.onLessPeopleInCar.bind(this);
    this.onMorePeople = this.onMorePeople.bind(this);
    this.onLessPeople = this.onLessPeople.bind(this);

    this.onInputNameCarChanged = this.onInputNameCarChanged.bind(this);
    this.onInputNameChanged = this.onInputNameChanged.bind(this);
    this.onInputRutCarChanged = this.onInputRutCarChanged.bind(this);
    this.onInputRutChanged = this.onInputRutChanged.bind(this);
    this.onInputPlateChange = this.onInputPlateChange.bind(this);
    this.onTransferCodeChange = this.onTransferCodeChange.bind(this);

    this.onNextStep = this.onNextStep.bind(this);

    this.sendVisitsInfo = this.sendVisitsInfo.bind(this);
  }

  componentDidMount() {
    const year = new Date().getFullYear();
    const month = new Date().getMonth() + 1;
    getCopropietaryInfo()
      .then((copropietaryInfoFromApi) => {
        this.setState({
          copropietaryInfo: copropietaryInfoFromApi,
        });
        checkPayment(this.state.copropietaryInfo.id, year, month)
          .then((response) => {
            this.setState({
              checkPayment: response,
            });
          });
      });
  }

  changeStateUserPayment(userPaymentId, transferCode) {
    changeStateUserPayment(userPaymentId, transferCode)
      .then((response) => {
        this.setState({
          successfulChangeUserPaymentState: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulChangeUserPaymentState: 2,
        });
      });
  }

  onNextStep(nextStep) {
    this.setState({
      stepNotifyPaymentModal: nextStep,
    });
  }

  sendVisitsInfo(list) {
    sendVisitsInfo(list)
      .then((response) => {
        this.setState({
          registeredVisits: 1,
        });
      })
      .catch((error) => {
        this.setState({
          registeredVisits: 0,
        });
      });
  }

  onPeopleInCarChanged(event) {
    this.setState({
      peopleInCar: event.target.value,
    });
  }

  onPeopleChanged(event) {
    this.setState({
      people: event.target.value,
    });
  }

  onChangeStep(nextStep) {
    if (nextStep === 3) {
      const list = new Array(this.state.peopleInCar).fill().map(() => {
        return {
          name: null,
          rut: null,
          plate: null,
        };
      });
      this.setState({
        peopleInCarList: list,
        step: nextStep,
      });
      return;
    }
    if (nextStep === 5) {
      const list = new Array(this.state.people).fill().map(() => {
        return {
          name: null,
          rut: null,
        };
      });
      this.setState({
        peopleList: list,
        step: nextStep,
      });
    }
    this.setState({
      step: nextStep,
    });
  }

  onOpenVisitsModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openVisits: true });
  }

  onOpenNotifyPaymentModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openNotifyPaymentModal: true });
  }

  onCloseModal() {
    this.setState({
      openVisits: false,
      openNotifyPaymentModal: false,
      step: 1,
      stepNotifyPaymentModal: 1,
      peopleInCar: 0,
      people: 0,
      registeredVisits: 0,
      peopleInCarList: [],
      peopleList: [],
      successfulChangeUserPaymentState: 0,
    });
  }

  onMorePeopleInCar() {
    this.setState({
      peopleInCar: this.state.peopleInCar + 1,
    });
  }

  onLessPeopleInCar() {
    if (this.state.peopleInCar === 0) {
      this.setState({
        peopleInCar: 0,
      });
    } else {
      this.setState({
        peopleInCar: this.state.peopleInCar - 1,
      });
    }
  }

  onMorePeople() {
    this.setState({
      people: this.state.people + 1,
    });
  }

  onLessPeople() {
    if (this.state.people === 0) {
      this.setState({
        people: 0,
      });
    } else {
      this.setState({
        people: this.state.people - 1,
      });
    }
  }

  //test

  onInputNameCarChanged(name, index) {
    const list = this.state.peopleInCarList;
    list[index].name = name;
    this.setState({
      peopleInCarList: list,
    });
  }

  onInputNameChanged(name, index) {
    const list = this.state.peopleList;
    list[index].name = name;
    this.setState({
      peopleList: list,
    });
  }

  onInputRutCarChanged(rut, index) {
    const list = this.state.peopleInCarList;
    list[index].rut = rut;
    this.setState({
      peopleInCarList: list,
    });
  }

  onInputRutChanged(rut, index) {
    const list = this.state.peopleList;
    list[index].rut = rut;
    this.setState({
      peopleList: list,
    });
  }

  onInputPlateChange(plate, index) {
    const list = this.state.peopleInCarList;
    list[index].plate = plate;
    this.setState({
      peopleInCarList: list,
    });
  }

  onTransferCodeChange(event) {
    const data = event.target.value;
    this.setState({
      transferCode: data,
    });
  }

  //test
  //no se debe setear estado en render.
  render() {
    let questionCarText;
    let questionText;
    if (this.state.peopleInCar === 1) {
      questionCarText = '¿Quién es?';
    } else {
      questionCarText = '¿Quiénes son?';
    }
    if (this.state.people === 1) {
      questionText = 'Quién es?';
    } else {
      questionText = '¿Quiénes son?';
    }

    let messageVisits;
    if (this.state.registeredVisits === 1) {
      messageVisits = 'Se ha registrado con éxito tus visitas';
    } else {
      messageVisits = 'Ha ocurrido un error, por favor inténtalo nuevamente más tarde.';
    }

    console.log(this.state);
    const visitsForm = this.state.peopleList.map((item, index) => {
      return (
        <div
          key={index}
          className="car-form-card"
        >
          <div className="car-form-line">
            <div className="title-input-car-form">
              Nombre:
            </div>
            <input
              type="text"
              onChange={(event) => {
                const name = event.target.value;
                this.onInputNameChanged(name, index);
              }}
              className="car-input-class"
            />
          </div>
          <div className="car-form-line">
            <div className="title-input-car-form">
              Rut:
            </div>
            <input
              type="text"
              onChange={(event) => {
                const rut = event.target.value;
                this.onInputRutChanged(rut, index);
              }}
              className="car-input-class"
            />
          </div>
        </div>
      );
    });

    const visitsCarForm = this.state.peopleInCarList.map((item, index) => {
      return (
        <div
          key={index}
          className="car-form-card"
        >
          <div className="car-form-line">
            <div className="title-input-car-form">
              Nombre:
            </div>
            <input
              type="text"
              onChange={(event) => {
                const name = event.target.value;
                this.onInputNameCarChanged(name, index);
              }}
              className="car-input-class"
              placeholder="María"
            />
          </div>
          <div className="car-form-line">
            <div className="title-input-car-form">
              Rut:
            </div>
            <input
              type="text"
              onChange={(event) => {
                const rut = event.target.value;
                this.onInputRutCarChanged(rut, index);
              }}
              className="car-input-class"
              placeholder="19.289.131-0"
            />
          </div>
          <div className="car-form-line">
            <div className="title-input-car-form">
              Patente:
            </div>
            <input
              type="text"
              onChange={(event) => {
                const plate = event.target.value;
                this.onInputPlateChange(plate, index);
              }}
              className="car-input-class"
              placeholder="dd-dd-dd"
            />
          </div>
        </div>
      );
    });

    const checkCarForm = this.state.peopleInCarList.map((item, index) => {
      return (
        <div className="check-car-card">
          <div className="left-container">
            <div className="icon-container">
              <div className="icon-car">
              </div>
            </div>
          </div>
          <div className="right-container">
            <div className="line">
              {item.name}
            </div>
            <div className="line">
              {item.rut}
            </div>
            <div className="line">
              {item.plate}
            </div>
          </div>
        </div>
      );
    });

    const checkPeopleForm = this.state.peopleList.map((item, index) => {
      return (
        <div className="check-car-card">
          <div className="left-container">
            <div className="icon-container">
              <div className="icon-person">
              </div>
            </div>
          </div>
          <div className="right-container">
            <div className="line">
              {item.name}
            </div>
            <div className="line">
              {item.rut}
            </div>
          </div>
        </div>
      );
    });

    let content;
    switch (this.state.step) {
      case 1: {
        content = (
          <div className="wizard-container">
            <div className="progress-container">
            </div>
            <div className="message-wizard">
              <div className="message">
                Avisa al conserje cuando esperes visitas para hacer más segura tu comunidad.
              </div>
              <div className="icon-community-container">
                <img
                  alt="Ícono"
                  className="icon-community"
                  src={CommunityIcon}
                />
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => { this.onChangeStep(2); }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        content = (
          <div className="wizard-container">
            <div className="progress-container">
            </div>
            <div className="message-wizard">
              <div className="message">
                ¿Cuántas personas vienen en automóvil?
              </div>
              <div className="input-car-container">
                <div
                  className="less-button-container"
                  onClick={this.onLessPeopleInCar}
                >
                  <div className="less-button">
                  </div>
                </div>
                <input
                  type="number"
                  className="input-car-visits"
                  placeholder="1"
                  onChange={this.onPeopleInCarChanged}
                  value={this.state.peopleInCar}
                />
                <div
                  className="more-button-container"
                  onClick={this.onMorePeopleInCar}
                >
                  <div className="more-button">
                  </div>
                </div>
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                //Se puede pasar parámetros
                onClick={() => {
                  if (this.state.peopleInCar === 0) {
                    this.onChangeStep(4);
                  } else {
                    this.onChangeStep(3);
                  }
                }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 3: {
        content = (
          <div className="wizard-container">
            <div className="progress-container">
            </div>
            <div className="message-wizard">
              <div className="message">
                {questionCarText}
              </div>
              <div className="car-cards-list">
                {visitsCarForm}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                //Se puede pasar parámetros
                onClick={() => {
                  this.onChangeStep(4);
                }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 4: {
        content = (
          <div className="wizard-container">
            <div className="progress-container">
            </div>
            <div className="message-wizard">
              <div className="message">
                ¿Cuántas personas vienen a pie?
              </div>
              <div className="input-car-container">
                <div
                  className="less-button-container"
                  onClick={this.onLessPeople}
                >
                  <div className="less-button">
                  </div>
                </div>
                <input
                  type="number"
                  className="input-car-visits"
                  placeholder="1"
                  onChange={this.onPeopleChanged}
                  value={this.state.people}
                />
                <div
                  className="more-button-container"
                  onClick={this.onMorePeople}
                >
                  <div className="more-button">
                  </div>
                </div>
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                //Se puede pasar parámetros
                onClick={() => {
                  if (this.state.people === 0) {
                    this.onChangeStep(6);
                  } else {
                    this.onChangeStep(5);
                  }
                }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 5: {
        content = (
          <div className="wizard-container">
            <div className="progress-container">
            </div>
            <div className="message-wizard">
              <div className="message">
                {questionText}
              </div>
              <div className="car-cards-list">
                {visitsForm}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                //Se puede pasar parámetros
                onClick={() => {
                  this.onChangeStep(6);
                }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 6: {
        content = (
          <div className="wizard-container">
            <div className="message-wizard">
              <div className="message">
                ¡Asegúrate que todo este bien!
              </div>
              <div className="check-cards-container">
                {checkCarForm}
                {checkPeopleForm}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                //Se puede pasar parámetros
                onClick={() => {
                  this.sendVisitsInfo([...this.state.peopleInCarList, ...this.state.peopleList]);
                  this.onChangeStep(7);
                }}
              >
                Confirmar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 7: {
        content = (
          <div className="wizard-container">
            <div className="message-wizard">
              <div className="message">
                {messageVisits}
              </div>
            </div>
          </div>
        );
        break;
      }
      default: {
        content = null;
      }
    }

    let monthAsString;
    if (is.existy(this.state.checkPayment)) {
      if (this.state.checkPayment.payment.month === 1) {
        monthAsString = 'Enero';
      } else if (this.state.checkPayment.payment.month === 2) {
        monthAsString = 'Febrero';
      } else if (this.state.checkPayment.payment.month === 3) {
        monthAsString = 'Marzo';
      } else if (this.state.checkPayment.payment.month === 4) {
        monthAsString = 'Abril';
      } else if (this.state.checkPayment.payment.month === 5) {
        monthAsString = 'Mayo';
      } else if (this.state.checkPayment.payment.month === 6) {
        monthAsString = 'Junio';
      } else if (this.state.checkPayment.payment.month === 7) {
        monthAsString = 'Julio';
      } else if (this.state.checkPayment.payment.month === 8) {
        monthAsString = 'Agosto';
      } else if (this.state.checkPayment.payment.month === 9) {
        monthAsString = 'Septiembre';
      } else if (this.state.checkPayment.payment.month === 10) {
        monthAsString = 'Octubre';
      } else if (this.state.checkPayment.payment.month === 11) {
        monthAsString = 'Noviembre';
      } else {
        monthAsString = 'Diciembre';
      }
    }

    let messagePaymentCarousel;
    if (this.state.checkPayment.payment === null) {
      messagePaymentCarousel = (
        <div className="card-carousel">
          <div className="payment-card">
            No tienes ningún pago pendiente :)
          </div>
        </div>
      );
    } else if (this.state.checkPayment.payment != null) {
      if (this.state.checkPayment.payment.user_did_pay === false) {
        messagePaymentCarousel = (
          <div className="card-carousel">
            <div className="payment-card">
              <div className="left-container">
                <div className="left-top-container">
                  <div className="message">
                    ¡Tienes un pago pendiente correspondiente al mes de {monthAsString}!
                  </div>
                </div>
                <div className="amount-container">
                  <div className="amount">
                    $ {this.state.checkPayment.payment.total_amount}
                  </div>
                </div>
              </div>
              <div className="right-container">
                <div className="image-top-container">
                  <img
                    alt="Placeholder"
                    className="exclamation"
                    src={require("../../../assets/imgs/chat.svg")}
                  />
                </div>
                <div className="button-container">
                  <div
                    className="button"
                    onClick={(e) => {
                      this.onOpenNotifyPaymentModal(e);
                    }}
                  >
                    Avisar Pago
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      } else if (this.state.checkPayment.payment.user_did_pay === true) {
        messagePaymentCarousel = (
          <div className="card-carousel">
            <div className="payment-card">
              <div className="left-container-pay">
                <div className="message">
                  Tu pago está siendo procesado, espera que el administrador lo apruebe.
                </div>
              </div>
              <div className="right-container-pay">
                <img
                  alt="Placeholder"
                  className="exclamation"
                  src={require("../../../assets/imgs/refresh-green.svg")}
                />
              </div>
            </div>
          </div>
        );
      }
    }

    let messageChangeUserPaymentStateResponse;
    if (this.state.successfulChangeUserPaymentState === 1) {
      messageChangeUserPaymentStateResponse = '¡Se ha enviado el aviso con éxito!, ahora debes esperar que aprueben el pago';
    } else if (this.state.successfulChangeUserPaymentState === 2) {
      messageChangeUserPaymentStateResponse = 'Ha ocurrido un problema, por favor intentálo denuevo mas tarde';
    }

    let contentNotifyPaymentModal;
    switch (this.state.stepNotifyPaymentModal) {
      case 1: {
        contentNotifyPaymentModal = (
          <div className="first-content-modal">
            <div className="title-container">
              <div className="title">
                Avisa al administrador que ya realizaste el pago
              </div>
            </div>
            <div className="image-container">
              <img
                alt="Placeholder"
                className="image"
                src={require("../../../assets/imgs/pay.svg")}
              />
            </div>
            <div className="buttons-container">
              <div
                className="button"
                onClick={() => {
                  this.onNextStep(2);
                }}
              >
                Pago en Efectivo
              </div>
              <div
                className="button"
                onClick={() => {
                  this.onNextStep(3);
                }}
              >
                Transferencia Bancaria
              </div>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        break;
      }
      case 3: {
        contentNotifyPaymentModal = (
          <div className="second-content-modal">
            <div className="title-container">
              <div className="title">
                Escribe tu código de transacción
              </div>
            </div>
            <div className="sub-title-container">
              <div className="sub-title">
                Así el administrador puede confirmar tu pago
              </div>
            </div>
            <div className="content-container">
              <div className="one-line-form">
                <div className="static">
                  Código:
                </div>
                <input
                  type="text"
                  onChange={this.onTransferCodeChange}
                  value={this.state.transferCode}
                  className="input-form"
                />
              </div>
            </div>
            <div className="button-container">
              <div
                className="button"
                onClick={() => {
                  this.changeStateUserPayment(this.state.checkPayment.payment.id, this.state.transferCode);
                  this.onNextStep(4);
                }}
              >
                Aceptar
              </div>
            </div>
          </div>
        );
        break;
      }
      case 4: {
        contentNotifyPaymentModal = (
          <div className="second-content-modal">
            {messageChangeUserPaymentStateResponse}
          </div>
        );
        break;
      }
    }

    const { copropietaryInfo, openVisits } = this.state;
    return (
      <div className="copropietary-view">
        <Modal
          classNames={{
            modal: 'custom-modal-visits',
            closeIcon: 'custom-modal-close',
          }}
          open={openVisits}
          onClose={this.onCloseModal}
          little
        >
          {content}
        </Modal>
        <Modal
          classNames={{
            modal: 'custom-modal-copropietary',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openNotifyPaymentModal}
          onClose={this.onCloseModal}
          little
        >
          {contentNotifyPaymentModal}
        </Modal>
        <div className="summary-container">
          <div className="name-copropietary-container">
            Bienvenido devuelta, {copropietaryInfo.user_name}.
          </div>
          <div className="summary-card">
            <Carousel showStatus={false} showArrows={false} showThumbs={false}>
              {messagePaymentCarousel}
              <div className="card-carousel">
                <div className="image-condo-container">
                  <div className="image-condo-carousel">
                    <div className="name-condo-carousel">
                      {copropietaryInfo.condo_name}
                    </div>
                  </div>
                  <div className="text-carousel-container">
                    <div className="info-container">
                      <div className="address-info-container">
                        <div className="icon-address">
                        </div>
                        <div className="address-text">
                          {copropietaryInfo.condo_address}
                        </div>
                      </div>
                      <div className="apartment-info-container">
                        <div className="icon-apartment">
                        </div>
                        <div className="apartment-text">
                          Torre A, 1205
                        </div>
                      </div>
                    </div>
                    <div className="button-explore-container">
                      <div className="button-explore">
                        Explorar
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Carousel>
          </div>
        </div>
        <div className="options-container">
          <div className="options-left-container">
            <div className="visits-button-container">
              <div
                className="button-container"
                onClick={this.onOpenVisitsModal}
              >
                <div className="image-button-container visits">
                </div>
                <div className="text-button-container">
                  Visitas
                </div>
              </div>
            </div>
            <div className="complaints-button-container">
              <Link to="/copropietary/complaints">
                <div className="button-container">
                  <div className="image-button-container complaints">
                  </div>
                  <div className="text-button-container">
                    Reclamos
                  </div>
                </div>
              </Link>
            </div>
          </div>
          <div className="options-right-container">
            <div className="packages-button-container">
              <Link to="/copropietary/packages">
                <div className="button-container">
                  <div className="image-button-container package">
                  </div>
                  <div className="text-button-container">
                    Encomiendas
                  </div>
                </div>
              </Link>
            </div>
            <div className="blackboard-button-container">
              <Link to="/copropietary/blackboard">
                <div className="button-container">
                  <div className="image-button-container blackboard">
                  </div>
                  <div className="text-button-container">
                    Pizarra
                  </div>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}

export default HomeCopropietaryComponent;
