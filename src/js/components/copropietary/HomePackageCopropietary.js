import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import is from 'is_js';


import history from '../../utils/historyHelper';
import { getCopropietaryInfo, getPackages } from '../../api/CopropietaryApi';
import PackageCard from '../copropietary/PackageCard';

class HomePackageCopropietaryComponent extends Component {
	constructor(props) {
    super(props);
    this.state = {
      packages: [],
    };
  }

  componentDidMount() {
    getPackages()
      .then((packagesFromApi) => {
        this.setState({
          packages: packagesFromApi,
        });
      });
  }

  render() {
    let cards;
    if (!is.empty(this.state.packages)) {
      cards = this.state.packages.map((card) => {
        return (
          <PackageCard
            key={card.id}
            state={card.state}
            arrivedDate={card.arrived_date}
            receivedByName={card.received_by_name}
            retrivedDate={card.retrived_date}
            retrivedByName={card.retrived_by_name}
            description={card.description}
          />
        );
      });
    } else {
      cards = (
        <div className="empty-message-container">
          <div className="empty-message">
            No existen encomiendas para ti aún :(
          </div>
        </div>
      );
    }

    return (
      <div className="condo-view">
        <div className="package-card-list">
          {cards}
        </div>
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}

export default HomePackageCopropietaryComponent;