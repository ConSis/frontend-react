import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import moment from 'moment';
moment.locale('es');

import history from '../../utils/historyHelper';
import { Carousel } from 'react-responsive-carousel';
import { getCopropietaryInfo, getNews } from '../../api/CopropietaryApi';
import StaffCard from '../copropietary/StaffCard';
import TabBarCoproNotificaciones from '../copropietary/TabBarCoproNotificaciones';

class NotificationsCopropietary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copropietaryInfo: '',
      newsList: [],
    };
  }

  componentDidMount() {
    getCopropietaryInfo()
      .then((copropietaryInfoFromApi) => {
        this.setState({
          copropietaryInfo: copropietaryInfoFromApi,
        });
        getNews(this.state.copropietaryInfo.condo_id)
          .then((newsFromApi) => {
            this.setState({
              newsList: newsFromApi,
            });
          });
      });
  }

  render() {
    const newsListCards = this.state.newsList.map((item, index) => {
      const createdDate = moment(item.created_at);
      return (
        <div className="news-card">
          <div className="title-container">
            <div className="title">
              {item.title}
            </div>
          </div>
          <div className="image-container">
          </div>
          <div className="message-container">
            <div className="message">
              {item.message}
            </div>
          </div>
          <div className="date-container">
            <div className="date">
              {createdDate.format('DD/MM/YYYY')} a las {createdDate.format('HH:MM')}
            </div>
          </div>
        </div>
      );
    });

    const viewsCoproNotificationsInfo = [{
      text: 'Notificaciones',
      textClass: 'text-admin-tab',
      content: (
        <div className="info-tab-condo">
          <div className="notifications-container">
            {this.props.notifications.map((notification) => {
              const createdDate = moment(notification.created_at);
              return (
                <div className="notification-container">
                  <div className="title">
                    {notification.title}
                  </div>
                  <div className="message">
                    {notification.message}
                  </div>
                  <div className="date">
                    {createdDate.format('DD/MM/YYYY')} a las {createdDate.format('HH:MM')}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      ),
    },
    {
      text: 'Noticias',
      textClass: 'text-admin-tab',
      content: (
        <div className="info-tab-condo">
          <div className="news-cards-container">
            {newsListCards}
          </div>
        </div>
      ),
    }];

    return (
      <div className="copropietary-view">
        <TabBarCoproNotificaciones
          views={viewsCoproNotificationsInfo}
        />
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}

export default NotificationsCopropietary;