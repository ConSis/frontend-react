import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';

// Assets
import UserLoginIcon from '../../../assets/imgs/user-login.svg';


moment.locale('es');

class PackageCard extends Component {
	constructor(props) {
    super(props);
    this.state = {};
  }


  render() {
    moment.locale('es', {
      monthsShort : 'Ene_Feb_Mar_Abr_May_Jun_Jul_Ago_Sep_Oct_Nov_Dic'.split('_'),
      months : 'enero_febrero_marzo_abril_mayoooo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre'.split('_'),
    });

    var retrivedDate = new Date(this.props.retrivedDate);
    var retrivedMoment = moment(retrivedDate);
    let stateString;
    if (this.props.state === 0) {
      stateString = 'En Recepción';
    } else {
      stateString = 'Retirado';
    }

    let divs;
    if (this.props.state === 0) {
      divs = (
        <div className="received-name-container">
          <div className="received-title">
            Recepcionado por: 
          </div>
          <div className="received-name">
            <div className="icon-container">
              <img
                alt="Ícono"
                className="icon"
                src={UserLoginIcon}
              />
            </div>
            <div className="name">
              {this.props.receivedByName}
            </div>
          </div>
        </div>
      );
    } else {
      divs = (
        <div className="retrived-info-container">
          <div className="retrived-name-container">
            <div className="retrived-name">
              Retirado por {this.props.retrivedByName}
            </div>
          </div>
          <div className="retrived-date-container">
            <div className="retrived-date">
              el {retrivedMoment.format('DD/MM')} a las {retrivedMoment.format('HH:MM')}
            </div>
          </div>
        </div>
      )
    }

    var date = new Date(this.props.arrivedDate);
    var momentObject = moment(date);

    return (
      <div className="card">
        <div className="date-container">
          <div className="month-container">
            {momentObject.format('MMM')}
          </div>
          <div className="day-container">
            {momentObject.format('DD')}
          </div>
          <div className="year-container">
            {momentObject.format('YYYY')}
          </div>
        </div>
        <div className="info-container">
          <div className="package-state-container">
            <div className="package-state">
              {stateString}
            </div>
          </div>
          <div className="description-container">
            <div className="description">
            {this.props.description}
            </div>
          </div>
          <div className="arrived-info-container">
            {divs}
          </div>
        </div>
      </div>
    );
  }
}

PackageCard.defaultProps = {
  state: null,
  arrivedDate: null,
  receivedByName: null,
  retrivedDate: null,
  retrivedByName: null,
  description: null,
};

export default PackageCard;