import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import moment from 'moment';
import is from 'is_js';
moment.locale('es');

import history from '../../utils/historyHelper';
import { Carousel } from 'react-responsive-carousel';
import StaffCard from '../copropietary/StaffCard';
import TabBarCoproNotificaciones from '../copropietary/TabBarCoproNotificaciones';
import {
  getCopropietaryInfo,
  checkPayment,
  getUserPayments,
  changeStateUserPayment,
  getTransferInfo } from '../../api/copropietaryApi';


class PaymentsCopropietary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copropietaryInfo: '',
      checkPayment: { payment: {} },
      //userPayments: { user_payment: {}, details: {} },
      userPayments: [],
      openUserPaymentDetailModal: false,
      openNotifyPaymentModal: false,
      openTransferInfoModal: false,
      selectedUserPaymentCardIndex: '',
      transferCode: '',

      stepNotifyPaymentModal: 1,
      successfulChangeUserPaymentState: 0,
      transferInfo: {},
    };

    this.onOpenUserPaymentDetailModal = this.onOpenUserPaymentDetailModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.onOpenNotifyPaymentModal = this.onOpenNotifyPaymentModal.bind(this);
    this.onOpenTransferInfoModal = this.onOpenTransferInfoModal.bind(this);
    this.onNextStep = this.onNextStep.bind(this);
    this.onTransferCodeChange = this.onTransferCodeChange.bind(this);
    this.getTransferInfo = this.getTransferInfo.bind(this);
  }

  componentDidMount() {
    const year = new Date().getFullYear();
    const month = new Date().getMonth() + 1;
    getCopropietaryInfo()
      .then((copropietaryInfoFromApi) => {
        this.setState({
          copropietaryInfo: copropietaryInfoFromApi,
        });
        checkPayment(this.state.copropietaryInfo.id, year, month)
          .then((response) => {
            if (response.payment != null) {
              this.setState({
                checkPayment: response,
              });
            }
          });
        getUserPayments(this.state.copropietaryInfo.id)
          .then((response) => {
            this.setState({
              userPayments: response,
            });
          });
      });
  }

  changeStateUserPayment(userPaymentId, transferCode) {
    changeStateUserPayment(userPaymentId, transferCode)
      .then((response) => {
        this.setState({
          successfulChangeUserPaymentState: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulChangeUserPaymentState: 2,
        });
      });
  }

  getTransferInfo(condoId) {
    getTransferInfo(condoId)
      .then((response) => {
        if (response != null) {
          this.setState({
            transferInfo: response,
          });
        }
      });
  }

  onOpenUserPaymentDetailModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({
      openUserPaymentDetailModal: true,
    });
  }

  onOpenNotifyPaymentModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openNotifyPaymentModal: true });
  }

  onOpenTransferInfoModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openTransferInfoModal: true });
  }

  onCloseModal() {
    this.setState({
      openUserPaymentDetailModal: false,
      selectedUserPaymentCardIndex: '',
      openNotifyPaymentModal: false,
      openTransferInfoModal: false,
      stepNotifyPaymentModal: 1,
      transferCode: '',
      successfulChangeUserPaymentState: 0,
    });
  }

  onNextStep(nextStep) {
    this.setState({
      stepNotifyPaymentModal: nextStep,
    });
  }

  onTransferCodeChange(event) {
    const data = event.target.value;
    this.setState({
      transferCode: data,
    });
  }

  render() {
    console.log(this.state);
    let monthAsString;
    if (is.existy(this.state.checkPayment)) {
      if (this.state.checkPayment.payment.month === 1) {
        monthAsString = 'Enero';
      } else if (this.state.checkPayment.payment.month === 2) {
        monthAsString = 'Febrero';
      } else if (this.state.checkPayment.payment.month === 3) {
        monthAsString = 'Marzo';
      } else if (this.state.checkPayment.payment.month === 4) {
        monthAsString = 'Abril';
      } else if (this.state.checkPayment.payment.month === 5) {
        monthAsString = 'Mayo';
      } else if (this.state.checkPayment.payment.month === 6) {
        monthAsString = 'Junio';
      } else if (this.state.checkPayment.payment.month === 7) {
        monthAsString = 'Julio';
      } else if (this.state.checkPayment.payment.month === 8) {
        monthAsString = 'Agosto';
      } else if (this.state.checkPayment.payment.month === 9) {
        monthAsString = 'Septiembre';
      } else if (this.state.checkPayment.payment.month === 10) {
        monthAsString = 'Octubre';
      } else if (this.state.checkPayment.payment.month === 11) {
        monthAsString = 'Noviembre';
      } else {
        monthAsString = 'Diciembre';
      }
    }

    let cardActiveUserPayment;
    if (this.state.checkPayment.payment != null) {
      if (this.state.checkPayment.payment.user_did_pay === true && this.state.checkPayment.payment.payment_confirmed === false) {
        cardActiveUserPayment = (
          <div className="active-payment-card">
            <div className="active-card-top-container">
              <div className="month">
                {monthAsString}
              </div>
            </div>
            <div className="message-container">
              <div className="left-container">
                <div className="message">
                  Tu pago está siendo procesado
                </div>
                <div className="sub-message">
                  Espera a que el administrador lo apruebe
                </div>
              </div>
              <div className="right-container">
                <img
                  alt="Placeholder"
                  className="exclamation"
                  src={require("../../../assets/imgs/refresh-green.svg")}
                />
              </div>
            </div>
          </div>
        );
      } else if (this.state.checkPayment.payment.user_did_pay === true && this.state.checkPayment.payment.payment_confirmed === true) {
        cardActiveUserPayment = (
          <div className="not-active-payment-card">
            <div className="message">
              No tienes pagos activos aún.
            </div>
          </div>
        );
      } else if (this.state.checkPayment.payment.user_did_pay === false && this.state.checkPayment.payment.payment_confirmed === false) {
        cardActiveUserPayment = (
          <div className="active-payment-card">
            <div className="active-card-top-container">
              <div className="month">
                {monthAsString}
              </div>
            </div>
            <div className="message-container">
              <div className="left-container">
                <div className="message">
                  Tienes un pago pendiente
                </div>
                <div className="amount">
                 $ {this.state.checkPayment.payment.total_amount.toLocaleString('es-IN')}
                </div>
              </div>
              <div className="right-container">
                <div
                  className="button"
                  onClick={(e) => {
                    this.onOpenNotifyPaymentModal(e);
                  }}
                >
                  Avisar Pago
                </div>
              </div>
            </div>
          </div>
        );
      }
    }

    let userPaymentsCards;
    if (is.empty(this.state.userPayments)) {
      userPaymentsCards = (
        <div className="no-cards">
          <div className="message">
            No existe historial de pagos, aún.
          </div>
        </div>
      );
    } else if (!is.empty(this.state.userPayments)) {
      userPaymentsCards = this.state.userPayments.map((item, index) => {
        let monthAsStringHistoric;
        if (item.user_payment.month === 1) {
          monthAsStringHistoric = 'Enero';
        } else if (item.user_payment.month === 2) {
          monthAsStringHistoric = 'Febrero';
        } else if (item.user_payment.month === 3) {
          monthAsStringHistoric = 'Marzo';
        } else if (item.user_payment.month === 4) {
          monthAsStringHistoric = 'Abril';
        } else if (item.user_payment.month === 5) {
          monthAsStringHistoric = 'Mayo';
        } else if (item.user_payment.month === 6) {
          monthAsStringHistoric = 'Junio';
        } else if (item.user_payment.month === 7) {
          monthAsStringHistoric = 'Julio';
        } else if (item.user_payment.month === 8) {
          monthAsStringHistoric = 'Agosto';
        } else if (item.user_payment.month === 9) {
          monthAsStringHistoric = 'Septiembre';
        } else if (item.user_payment.month === 10) {
          monthAsStringHistoric = 'Octubre';
        } else if (item.user_payment.month === 11) {
          monthAsStringHistoric = 'Noviembre';
        } else if (item.user_payment.month === 12) {
          monthAsStringHistoric = 'Diciembre';
        }

        return (
          <div className="historic-payment-card">
            <div className="top-container">
              <div className="month">
                {monthAsStringHistoric}
              </div>
            </div>
            <div className="message-container">
              <div className="left-container">
                <div className="message">
                  <div className="top-message">
                    Total pagado:
                  </div>
                  <div className="amount">
                    $ {item.user_payment.total_amount.toLocaleString('es-IN')}
                  </div>
                </div>
              </div>
              <div
                className="right-container"
                onClick={(e) => {
                  this.setState({
                    selectedUserPaymentCardIndex: index,
                  });
                  this.onOpenUserPaymentDetailModal(e);
                }}
              >
                <img
                  alt="Placeholder"
                  className="exclamation"
                  src={require("../../../assets/imgs/more.svg")}
                />
              </div>
            </div>
          </div>
        );
      });

    }

    let contentUserPaymentDetail;
    if (!is.empty(this.state.userPayments)) {
      if (is.existy(this.state.userPayments[this.state.selectedUserPaymentCardIndex])) {
        const details = this.state.userPayments[this.state.selectedUserPaymentCardIndex].details.map((item, index) => {
          return (
            <div className="detail-line">
              <div className="description">
                {item.description}
              </div>
              <div className="amount">
                $ {item.amount.toLocaleString('es-In')}
              </div>
            </div>
          );
        });

        contentUserPaymentDetail = (
          <div className="modal-container">
            <div className="titles-container">
              <div className="description">
                Detalle
              </div>
              <div className="amount">
                Monto
              </div>
            </div>
            <div className="details-container">
              {details}
            </div>
            <div className="message-bottom">
              *A estos valores hay que sumarles el cobro por tipo de apartamento, estacionamientos y bodegas*
            </div>
          </div>
        );
      }
    }

    let messageChangeUserPaymentStateResponse;
    if (this.state.successfulChangeUserPaymentState === 1) {
      messageChangeUserPaymentStateResponse = '¡Se ha enviado el aviso con éxito!, ahora debes esperar que aprueben el pago';
    } else if (this.state.successfulChangeUserPaymentState === 2) {
      messageChangeUserPaymentStateResponse = 'Ha ocurrido un problema, por favor intentálo denuevo mas tarde';
    }

    let contentNotifyPaymentModal;
    switch (this.state.stepNotifyPaymentModal) {
      case 1: {
        contentNotifyPaymentModal = (
          <div className="first-content-modal">
            <div className="title-container">
              <div className="title">
                Avisa al administrador que ya realizaste el pago
              </div>
            </div>
            <div className="image-container">
              <img
                alt="Placeholder"
                className="image"
                src={require("../../../assets/imgs/pay.svg")}
              />
            </div>
            <div className="buttons-container">
              <div
                className="button"
                onClick={() => {
                  this.onNextStep(2);
                }}
              >
                Pago en Efectivo
              </div>
              <div
                className="button"
                onClick={() => {
                  this.onNextStep(3);
                }}
              >
                Transferencia Bancaria
              </div>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        break;
      }
      case 3: {
        contentNotifyPaymentModal = (
          <div className="second-content-modal">
            <div className="title-container">
              <div className="title">
                Escribe tu código de transacción
              </div>
            </div>
            <div className="sub-title-container">
              <div className="sub-title">
                Así el administrador puede confirmar tu pago
              </div>
            </div>
            <div className="content-container">
              <div className="one-line-form">
                <div className="static">
                  Código:
                </div>
                <input
                  type="text"
                  onChange={this.onTransferCodeChange}
                  value={this.state.transferCode}
                  className="input-form"
                />
              </div>
            </div>
            <div className="button-container">
              <div
                className="button"
                onClick={() => {
                  this.changeStateUserPayment(this.state.checkPayment.payment.id, this.state.transferCode);
                  this.onNextStep(4);
                }}
              >
                Aceptar
              </div>
            </div>
          </div>
        );
        break;
      }
      case 4: {
        contentNotifyPaymentModal = (
          <div className="second-content-modal">
            <div className="title-container">
              {messageChangeUserPaymentStateResponse}
            </div>
            <div className="button-container">
              <div
                className="button"
                onClick={() => {
                  const year = new Date().getFullYear();
                  const month = new Date().getMonth() + 1;
                  checkPayment(this.state.copropietaryInfo.id, year, month)
                    .then((response) => {
                      if (response.payment != null) {
                        this.setState({
                          checkPayment: response,
                        });
                      }
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </div>
            </div>
          </div>
        );
        break;
      }
    }

    const contentTransferInfoModal = (
      <div className="first-content-modal">
        <div className="title-container">
          <div className="subtitle">
            Datos para transferencia electrónica
          </div>
        </div>
        <div className="line">
          <div className="static">
            Banco:
          </div>
          <div className="dynamic">
            {this.state.transferInfo.bank}
          </div>
        </div>
        <div className="line">
          <div className="static">
            Tipo de Cuenta:
          </div>
          <div className="dynamic">
            {this.state.transferInfo.account_type}
          </div>
        </div>
        <div className="line">
          <div className="static">
            Nombre:
          </div>
          <div className="dynamic">
            {this.state.transferInfo.name}
          </div>
        </div>
        <div className="line">
          <div className="static">
            Rut:
          </div>
          <div className="dynamic">
            {this.state.transferInfo.rut}
          </div>
        </div>
        <div className="line">
          <div className="static">
            Nº de cuenta:
          </div>
          <div className="dynamic">
            {this.state.transferInfo.account_number}
          </div>
        </div>
      </div>
    );


    return (
      <div className="copropietary-view">
        <Modal
          classNames={{
            modal: 'custom-modal-copropietary',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openNotifyPaymentModal}
          onClose={this.onCloseModal}
          little
        >
          {contentNotifyPaymentModal}
        </Modal>
        <Modal
          classNames={{
            modal: 'custom-modal-copropietary',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openTransferInfoModal}
          onClose={this.onCloseModal}
          little
        >
          {contentTransferInfoModal}
        </Modal>
        <Modal
          classNames={{
            modal: 'custom-package-modal',
            closeIcon: 'custom-modal-close',
          }}
          open={this.state.openUserPaymentDetailModal}
          onClose={this.onCloseModal}
          little
        >
          {contentUserPaymentDetail}
        </Modal>
        <div className="active-payment-container">
          <div
            className="refresh-button"
            onClick={() => {
              const year = new Date().getFullYear();
              const month = new Date().getMonth() + 1;
              checkPayment(this.state.copropietaryInfo.id, year, month)
                .then((response) => {
                  if (response.payment) {
                    this.setState({
                      checkPayment: response,
                    });
                  }
                });
            }}
          >
            Presiona para actualizar
          </div>
          {cardActiveUserPayment}
        </div>
        <div className="historic-payments-container">
          {userPaymentsCards}
        </div>
        <div
          className="transfer-info-button"
          onClick={(e) => {
            this.onOpenTransferInfoModal(e);
            this.getTransferInfo(this.state.copropietaryInfo.condo_id);
          }}
        >
          <img
            alt="Placeholder"
            className="icon"
            src={require("../../../assets/imgs/check-white.svg")}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}

export default PaymentsCopropietary;
