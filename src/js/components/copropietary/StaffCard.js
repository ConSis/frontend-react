import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

class StaffCard extends Component {
	constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="card">
        <div className="image-container">
          <div className="image">
          </div>
        </div>
        <div className="staff-info-container">
          <div className="staff-info-line">
            <div className="static-info">
              Nombre:
            </div>
            <div className="dinamic-info">
              {this.props.nameStaff}
            </div>
          </div>
          <div className="staff-info-line">
            <div className="static-info">
              Cargo:
            </div>
            <div className="dinamic-info">
              {this.props.chargeStaff}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

StaffCard.defaultProps = {
  nameStaff: null,
  chargeStaff: null,
};

export default StaffCard;