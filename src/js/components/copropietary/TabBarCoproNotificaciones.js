import React, { Component } from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';
import classnames from 'classnames';

class TabBarCoproNotificaciones extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
    };

    this.renderTabs = this.renderTabs.bind(this);
    this.renderView = this.renderView.bind(this);
  }

  renderTabs() {
    const tab = this.props.views.map((item, index) => {
      const tabClass = classnames({
        'tab-icon': true,
        'copro-notification-tab-active': index === this.state.currentIndex,
      });

      return (
        <button
          className={tabClass}
          onClick={() => {
            this.setState({
              currentIndex: index,
            });
          }}
        >
          <div className={item.textClass}>
            {item.text}
          </div>
        </button>
      );
    });

    return (
      <div className="tab-copro-notification-content">
        {tab}
      </div>
    );
  }

  renderView() {
    const view = this.props.views.map((item, index) => {
      let classTab;
      if (index === this.state.currentIndex) {
        classTab = 'copro-notification-view-active';
      } else {
        classTab = 'copro-notification-view-inactive';
      }

      return (
        <div
          key={item.text}
          className={classTab}
        >
          {item.content}
        </div>
      );
    });

    return (
      <div className="content-copro-notification-view">
        {view}
      </div>
    );
  }

  render() {
    return (
      <div className="views-admin-container">
        {this.renderTabs()}
        {this.renderView()}
      </div>
    );
  }
}

export default TabBarCoproNotificaciones;