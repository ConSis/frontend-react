import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter, Switch, Route, Link } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import { Async } from 'react-select';
import is from 'is_js';
import { Pie } from 'react-chartjs-2';
import moment from 'moment';

import history from '../utils/historyHelper';
import TabBarNavigation from '../components/TabBarNavigation';
import TabBarAdminCondo from '../components/TabBarAdminCondo';
import HomeVisitsAdminCond from '../components/adminCond/HomeVisitsAdminCond';
import HomeNewsAdminCond from '../components/adminCond/HomeNewsAdminCond';
import HomePackagesAdminCond from '../components/adminCond/HomePackagesAdminCond';
import HomePaymentsAdminCond from '../components/adminCond/HomePaymentsAdminCond';
import TabBarAdminCondoNotificaciones from '../components/TabBarAdminCondoNotificaciones';
import {
  getAdminCondoInfo,
  getCopropietaries,
  getEmployees,
  registerEmployee,
  destroyEmployee,
  getStorageTypes,
  registerStorageType,
  destroyStorageType,
  getParkingTypes,
  registerParkingType,
  destroyParkingType,
  getAppartmentTypes,
  registerAppartmentType,
  getTransferInfo,
  editTransferInfo,
  createCopropietary,
  createPayment,
  getNotPublishedPayments,
  getPublishedPayments,
  getPaymentDetails,
  getPayedUserPayments,
  getGraphData,
  confirmUserPayment,
  rejectUserPayment } from '../api/adminCondoApi';

moment.locale('es');

class AdminCondContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openAddCopropietaryModal: false,
      openAddEmployeeModal: false,
      openRemoveEmployeeModal: false,
      openAddStorageTypeModal: false,
      openRemoveStorageTypeModal: false,
      openAddParkingTypeModal: false,
      openRemoveParkingTypeModal: false,
      openAddAppartmentTypeModal: false,
      openTransferInfoModal: false,
      transferInfoModalEdit: false,
      openAddMonthPaymentModal: false,
      openPaymentPublishedInfoModal: false,
      openConfirmUserPayment: false,
      openRejectUserPayment: false,
      selectedEmployeeCard: '',
      selectedStorageTypeCard: '',
      selectedParkingTypeCard: '',
      selectedAppartmentTypeCard: '',
      userPaymentCardSelected: '',
      paymentPublishedSelected: '',
      stepAddCopropietaryModal: 1,
      stepAddEmployeeModal: 1,
      stepRemoveEmployeeModal: 1,
      stepAddStorageTypeModal: 1,
      stepRemoveStorageType: 1,
      stepAddParkingTypeModal: 1,
      stepRemoveParkingType: 1,
      stepAddAppartmentTypeModal: 1,
      stepEditTransferInfo: 1,
      stepAddPaymentModal: 1,
      stepConfirmUserPayment: 1,
      stepRejectUserPayment: 1,

      appartmentTypeSelected: '',
      newCopropietaryName: '',
      newCopropietaryRut: '',
      newCopropietaryEmail: '',
      newCopropietaryAddress: '',
      newCopropietaryNumber: '',
      errors: {},
      errorsParking: { parkingTypeSelected: [], nameNewParking: [] },
      errorsStorage: { storageTypeSelected: [], nameNewStorage: [] },

      newEmployeeName: '',
      newEmployeeRut: '',
      newEmployeeCharge: '',
      newEmployeeEmail: '',
      newStorageTypeName: '',
      newStorageTypeCost: '',
      newStorageTypeDescription: '',
      newParkingTypeName: '',
      newParkingTypeCost: '',
      newParkingTypeDescription: '',
      newAppartmentTypeName: '',
      newAppartmentTypeCost: '',
      newAppartmentTypeDescription: '',

      successfulNewCopropietary: 0,
      successfulNewEmployee: 0,
      succesfulDestroyEmployee: 0,
      successfulNewStorageType: 0,
      successfulDestroyStorageType: 0,
      successfulNewParkingType: 0,
      successfulNewAppartmentType: 0,
      successfulEditTransferInfo: 0,
      successfulNewPayment: 0,
      successfulConfirmUserPayment: 0,
      successfulRejectUserPayment: 0,

      adminCondoInfo: '',
      transferInfo: {},
      copropietariesList: [],

      addParkingList: [],
      addParkingNameList: [],
      addStorageList: [],
      addStorageNameList: [],

      employeesList: [],
      storageTypesList: [],
      parkingTypesList: [],
      appartmentTypesList: [],
      query: '',
      yearPayment: '',
      monthPayment: '',
      notPublishedPaymentsList: [],
      publishedPaymentsList: [],
      detailsPaymentPublishSelectedList: [],
      detailsPaymenPublishSelectedMonth: '',
      detailsPaymenPublishSelectedYear: '',
      payedUserPayments: [],
      graphData: [],
      graphLabels: [],
    };


    this.onTabSelected = this.onTabSelected.bind(this);

    this.onOpenAddCopropietaryModal = this.onOpenAddCopropietaryModal.bind(this);
    this.onOpenAddEmployeeModal = this.onOpenAddEmployeeModal.bind(this);
    this.onOpenRemoveEmployeeModal = this.onOpenRemoveEmployeeModal.bind(this);
    this.onOpenAddStorageTypeModal = this.onOpenAddStorageTypeModal.bind(this);
    this.onOpenRemoveStorageTypeModal = this.onOpenRemoveStorageTypeModal.bind(this);
    this.onOpenAddParkingTypeModal = this.onOpenAddParkingTypeModal.bind(this);
    this.onOpenRemoveParkingTypeModal = this.onOpenRemoveParkingTypeModal.bind(this);
    this.onOpenAddAppartmentTypeModal = this.onOpenAddAppartmentTypeModal.bind(this);
    this.onOpenTransferInfoModal = this.onOpenTransferInfoModal.bind(this);
    this.onOpenAddMonthPaymentModal = this.onOpenAddMonthPaymentModal.bind(this);
    this.onOpenPaymentPublishedInfoModal = this.onOpenPaymentPublishedInfoModal.bind(this);
    this.onOpenConfirmUserPayment = this.onOpenConfirmUserPayment.bind(this);
    this.onOpenRejectUserPayment = this.onOpenRejectUserPayment.bind(this);

    this.onCloseModal = this.onCloseModal.bind(this);
    this.onNextStepRemoveEmployeeModal = this.onNextStepRemoveEmployeeModal.bind(this);
    this.onNextStepAddEmployeeModal = this.onNextStepAddEmployeeModal.bind(this);
    this.onNextStepModal = this.onNextStepModal.bind(this);

    this.loadAppartmentTypesOption = this.loadAppartmentTypesOption.bind(this);
    this.onAppartmentTypeSelectedChange = this.onAppartmentTypeSelectedChange.bind(this);
    this.onNameCopropietaryChange = this.onNameCopropietaryChange.bind(this);
    this.onRutCopropietaryChange = this.onRutCopropietaryChange.bind(this);
    this.onEmailCopropietaryChange = this.onEmailCopropietaryChange.bind(this);
    this.onAddressCopropietaryChange = this.onAddressCopropietaryChange.bind(this);
    this.onNumberCopropietaryChange = this.onNumberCopropietaryChange.bind(this);
    this.validateCopropietariesInputs = this.validateCopropietariesInputs.bind(this);
    this.loadParkingTypesOption = this.loadParkingTypesOption.bind(this);
    this.onAddParkingInNewCopropietary = this.onAddParkingInNewCopropietary.bind(this);
    this.onParkingTypeSelectedChange = this.onParkingTypeSelectedChange.bind(this);
    this.onInputNameParkingChanged = this.onInputNameParkingChanged.bind(this);
    this.onParkingDelete = this.onParkingDelete.bind(this);
    this.onSubmitNewCopropietaryParkings = this.onSubmitNewCopropietaryParkings.bind(this);
    this.validateParkingCopropietariesInputs = this.validateParkingCopropietariesInputs.bind(this);
    this.onAddStorageInNewCopropietary = this.onAddStorageInNewCopropietary.bind(this);
    this.loadStorageTypesOption = this.loadStorageTypesOption.bind(this);
    this.onStorageTypeSelectedChange = this.onStorageTypeSelectedChange.bind(this);
    this.onInputNameStorageChanged = this.onInputNameStorageChanged.bind(this);
    this.onStorageDelete = this.onStorageDelete.bind(this);
    this.onSubmitNewCopropietaryStorages = this.onSubmitNewCopropietaryStorages.bind(this);
    this.validateStorageCopropietariesInput = this.validateStorageCopropietariesInput.bind(this);

    this.onNameEmployeeChange = this.onNameEmployeeChange.bind(this);
    this.onRutEmployeeChange = this.onRutEmployeeChange.bind(this);
    this.onChargeEmployeeChange = this.onChargeEmployeeChange.bind(this);
    this.onEmailEmployeeChange = this.onEmailEmployeeChange.bind(this);
    this.onNameStorageTypeChange = this.onNameStorageTypeChange.bind(this);
    this.onCostStorageTypeChange = this.onCostStorageTypeChange.bind(this);
    this.onDescriptionStorageTypeChange = this.onDescriptionStorageTypeChange.bind(this);
    this.onNameParkingTypeChange = this.onNameParkingTypeChange.bind(this);
    this.onCostParkingTypeChange = this.onCostParkingTypeChange.bind(this);
    this.onDescriptionParkingTypeChange = this.onDescriptionParkingTypeChange.bind(this);
    this.onNameAppartmentTypeChange = this.onNameAppartmentTypeChange.bind(this);
    this.onCostAppartmentTypeChange = this.onCostAppartmentTypeChange.bind(this);
    this.onDescriptionAppartmentTypeChange = this.onDescriptionAppartmentTypeChange.bind(this);
    this.onTransferInfoBankChange = this.onTransferInfoBankChange.bind(this);
    this.onTransferInfoAccountNumberChange = this.onTransferInfoAccountNumberChange.bind(this);
    this.onTransferInfoRutChange = this.onTransferInfoRutChange.bind(this);
    this.onTransferInfoAccountTypeChange = this.onTransferInfoAccountTypeChange.bind(this);
    this.onTransferInfoNameChange = this.onTransferInfoNameChange.bind(this);
    this.handleYearPaymentChange = this.handleYearPaymentChange.bind(this);
    this.handleMonthPaymentChange = this.handleMonthPaymentChange.bind(this);

    this.createCopropietary = this.createCopropietary.bind(this);
    this.destroyEmployee = this.destroyEmployee.bind(this);
    this.registerStorageType = this.registerStorageType.bind(this);
    this.destroyStorageType = this.destroyStorageType.bind(this);
    this.registerParkingType = this.registerParkingType.bind(this);
    this.destroyParkingType = this.destroyParkingType.bind(this);
    this.registerAppartmentType = this.registerAppartmentType.bind(this);
    this.createPayment = this.createPayment.bind(this);

    this.scrollDiv = null;
  }

  onTabSelected(index) {
    //const condoId = this.state.visitsList.
    const condoId = this.state.adminCondoInfo.condo_id;
    if (index === 2) {
      getNotPublishedPayments(condoId)
        .then((notPublishedPaymentsFromApi) => {
          this.setState({
            notPublishedPaymentsList: notPublishedPaymentsFromApi,
          });
        });
      getPublishedPayments(condoId)
        .then((publishedPaymentsFromApi) => {
          this.setState({
            publishedPaymentsList: publishedPaymentsFromApi,
          });
        });
    } else if (index === 1) {
      getCopropietaries(condoId)
        .then((copropietariesList) => {
          this.setState({
            copropietariesList: copropietariesList,
          });
        });
    } else if (index === 4) {
      getEmployees(condoId)
        .then((employeesFromApi) => {
          this.setState({
            employeesList: employeesFromApi,
          });
        });
      getStorageTypes(condoId)
        .then((storageTypesFromApi) => {
          this.setState({
            storageTypesList: storageTypesFromApi,
          });
        });
      getParkingTypes(condoId)
        .then((parkingTypesFromApi) => {
          this.setState({
            parkingTypesList: parkingTypesFromApi,
          });
        });
      getAppartmentTypes(condoId)
        .then((appartmentTypesFromApi) => {
          this.setState({
            appartmentTypesList: appartmentTypesFromApi,
          });
        });
    } else if (index === 3) {
      getPayedUserPayments(condoId)
        .then((response) => {
          this.setState({
            payedUserPayments: response,
          });
        });
    }
  }

  componentDidMount() {
    if (this.props.sessionState.token === null) {
      this.props.history.push('/auth');
      return;
    }

    const year = new Date().getFullYear();
    this.setState({
      yearPayment: year,
    });

    getAdminCondoInfo()
      .then((adminInfoFromApi) => {
        this.setState({
          adminCondoInfo: adminInfoFromApi,
        }, () => {
          getGraphData(this.state.adminCondoInfo.condo_id, moment().format('YYYY'), moment().format('MM'))
            .then((data) => {
              this.setState({
                graphData: [data.total - data.paid, data.paid],
                graphLabels: [`Pendiente (${data.total - data.paid})`, `Pagado (${data.paid})`],
              });
            });
        });
        getTransferInfo(this.state.adminCondoInfo.condo_id)
          .then((transferInfoFromApi) => {
            if (transferInfoFromApi != null) {
              this.setState({
                transferInfo: transferInfoFromApi,
              });
            }
          });
      });
  }

  /*MODALS*/

  onOpenConfirmUserPayment(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openConfirmUserPayment: true });
  }

  onOpenRejectUserPayment(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openRejectUserPayment: true });
  }

  onOpenAddCopropietaryModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openAddCopropietaryModal: true });
  }

  onOpenAddEmployeeModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openAddEmployeeModal: true });
  }

  onOpenRemoveEmployeeModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openRemoveEmployeeModal: true });
  }

  onOpenAddStorageTypeModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openAddStorageTypeModal: true });
  }

  onOpenRemoveStorageTypeModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openRemoveStorageTypeModal: true });
  }

  onOpenAddParkingTypeModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openAddParkingTypeModal: true });
  }

  onOpenRemoveParkingTypeModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openRemoveParkingTypeModal: true });
  }

  onOpenAddAppartmentTypeModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openAddAppartmentTypeModal: true });
  }

  onOpenTransferInfoModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openTransferInfoModal: true });
  }

  onOpenAddMonthPaymentModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({
      openAddMonthPaymentModal: true,
    });
  }

  onOpenPaymentPublishedInfoModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({
      openPaymentPublishedInfoModal: true,
    });
  }

  onCloseModal() {
    this.setState({
      addParkingList: [],
      addParkingNameList: [],
      addStorageList: [],
      addStorageNameList: [],
      detailsPaymentPublishSelectedList: [],
      openAddCopropietaryModal: false,
      openAddEmployeeModal: false,
      openRemoveEmployeeModal: false,
      openAddStorageTypeModal: false,
      openRemoveStorageTypeModal: false,
      openAddParkingTypeModal: false,
      openRemoveParkingTypeModal: false,
      openAddAppartmentTypeModal: false,
      openTransferInfoModal: false,
      transferInfoModalEdit: false,
      openAddMonthPaymentModal: false,
      openPaymentPublishedInfoModal: false,
      openConfirmUserPayment: false,
      openRejectUserPayment: false,
      stepAddCopropietaryModal: 1,
      stepAddEmployeeModal: 1,
      stepRemoveEmployeeModal: 1,
      stepAddStorageTypeModal: 1,
      stepRemoveStorageType: 1,
      stepAddParkingTypeModal: 1,
      stepRemoveParkingType: 1,
      stepAddAppartmentTypeModal: 1,
      stepEditTransferInfo: 1,
      stepAddPaymentModal: 1,
      stepConfirmUserPayment: 1,
      stepRejectUserPayment: 1,
      copropietarySelected: '',
      receivedNameInput: '',
      retrivedNameInput: '',

      appartmentTypeSelected: '',
      newCopropietaryName: '',
      newCopropietaryRut: '',
      newCopropietaryEmail: '',
      newCopropietaryAddress: '',
      newCopropietaryNumber: '',
      errors: {},
      errorsParking: { parkingTypeSelected: [], nameNewParking: [] },
      errorsStorage: { storageTypeSelected: [], nameNewStorage: [] },

      newEmployeeName: '',
      newEmployeeRut: '',
      newEmployeeCharge: '',
      newEmployeeEmail: '',
      newStorageTypeName: '',
      newStorageTypeCost: '',
      newStorageTypeDescription: '',
      newParkingTypeName: '',
      newParkingTypeCost: '',
      newParkingTypeDescription: '',
      newAppartmentTypeName: '',
      newAppartmentTypeCost: '',
      newAppartmentTypeDescription: '',
      successfulNewCopropietary: 0,
      successfulDestroyNew: 0,
      successfulNewEmployee: 0,
      succesfulDestroyEmployee: 0,
      successfulNewStorageType: 0,
      successfulDestroyStorageType: 0,
      successfulNewParkingType: 0,
      successfulNewAppartmentType: 0,
      successfulEditTransferInfo: 0,
      successfulNewPayment: 0,
      successfulConfirmUserPayment: 0,
      successfulRejectUserPayment: 0,
      openChangeStateModal: false,
      selectedEmployeeCard: '',
      selectedStorageTypeCard: '',
      selectedParkingTypeCard: '',
      selectedAppartmentTypeCard: '',
      paymentPublishedSelected: '',
      detailsPaymenPublishSelectedMonth: '',
      detailsPaymenPublishSelectedYear: '',
      userPaymentCardSelected: '',
    });
    this.onCancelEditClick();
  }

  registerEmployee(condoId, newEmployee) {
    registerEmployee(condoId, newEmployee)
      .then((response) => {
        this.setState({
          successfulNewEmployee: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulNewEmployee: 2,
        });
      });
  }

  destroyEmployee(employeeId) {
    destroyEmployee(employeeId)
      .then((response) => {
        this.setState({
          succesfulDestroyEmployee: 1,
        });
      })
      .catch((error) => {
        this.setState({
          succesfulDestroyEmployee: 2,
        });
      });
  }

  createCopropietary(condoId, newCopropietary) {
    createCopropietary(condoId, newCopropietary)
      .then((response) => {
        this.setState({
          successfulNewCopropietary: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulNewCopropietary: 2,
        })
      })
  }

  confirmUserPayment(newPaymentId) {
    confirmUserPayment(newPaymentId)
      .then((response) => {
        this.setState({
          successfulConfirmUserPayment: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulConfirmUserPayment: 2,
        });
      });
  }

  rejectUserPayment(paymentId) {
    rejectUserPayment(paymentId)
      .then((response) => {
        this.setState({
          successfulRejectUserPayment: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulRejectUserPayment: 2,
        });
      });
  }

  registerStorageType(condoId, newStorageType) {
    registerStorageType(condoId, newStorageType)
      .then((response) => {
        this.setState({
          successfulNewStorageType: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulNewStorageType: 2,
        });
      });
  }

  registerParkingType(condoId, newParkingType) {
    registerParkingType(condoId, newParkingType)
      .then((response) => {
        this.setState({
          successfulNewParkingType: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulNewParkingType: 2,
        });
      });
  }

  registerAppartmentType(condoId, newAppartmentType) {
    registerAppartmentType(condoId, newAppartmentType)
      .then((response) => {
        this.setState({
          successfulNewAppartmentType: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulNewAppartmentType: 2,
        });
      });
  }

  destroyStorageType(storageTypeId) {
    destroyStorageType(storageTypeId)
      .then((response) => {
        this.setState({
          successfulDestroyStorageType: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulDestroyStorageType: 2,
        });
      });
  }

  destroyParkingType(parkingTypeId) {
    destroyParkingType(parkingTypeId)
      .then((response) => {
        this.setState({
          successfulDestroyParkingType: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulDestroyParkingType: 2,
        });
      });
  }

  editTransferInfo(transferInfoId, newTransferInfo) {
    editTransferInfo(transferInfoId, newTransferInfo)
      .then((response) => {
        console.log(response);
        this.setState({
          successfulEditTransferInfo: 1,
        });
      })
      .catch((error) => {
        this.setState({
          successfulEditTransferInfo: 2,
        });
      });
  }

  loadAppartmentTypesOption() {
    return getAppartmentTypes(this.state.adminCondoInfo.condo_id)
      .then((data) => {
        const transformedData = data.map((appartmentType) => {
          // Esto es para cada condo. Recordar que es una transformación
          return { label: appartmentType.name, value: appartmentType.id };
        });

        return {
          options: transformedData,
        };
      });
  }

  onSubmitNewCopropietary() {
    const errors = this.validateCopropietariesInputs();

    if (!is.empty(errors)) {
      this.setState({
        errors: errors,
      });
      if(is.existy(this.scrollDiv)) {
        this.scrollDiv.scrollTop = 0;
      }
      return;
    }

    //api call
    this.setState({
      stepAddCopropietaryModal: 2,
    });
  }

  createPayment(condoId, newPayment) {
    if (newPayment.month === '') {
      console.log(newPayment);
      this.setState({ successfulNewPayment: 3 });
    } else {
      console.log(newPayment);
      createPayment(condoId, newPayment)
        .then((response) => {
          this.setState({ successfulNewPayment: 1 });
        })
        .catch((error) => {
          this.setState({ successfulNewPayment: 2 });
        });
    }
  }

  validateCopropietariesInputs() {
    const errors = {};

    if (this.state.newCopropietaryName.length <= 0) {
      errors.name = 'Debe escribir un nombre';
    } else if (this.state.newCopropietaryName.match(/\d+/g)) {
      errors.name = 'El nombre es incorrecto';
    }

    const { validate } = require('rut.js');

    if (this.state.newCopropietaryRut.length <= 0) {
      errors.rut = 'Debe escribir un Rut';
    } else if (validate(this.state.newCopropietaryRut) === false) {
      errors.rut = 'Ingrese un RUT válido';
    }

    if (this.state.newCopropietaryEmail.length <= 0) {
      errors.email = 'Debe escribir un correo';
    } else if (!is.email(this.state.newCopropietaryEmail)) {
      errors.email = 'El correo es inválido';
    }

    if (is.empty(this.state.appartmentTypeSelected)) {
      errors.selectedAppartmentType = 'Debe seleccionar un tipo de apartamento';
    }

    if (this.state.newCopropietaryAddress.length <= 0) {
      errors.address = 'Debe escribir una dirección';
    }

    if (this.state.newCopropietaryNumber.length <= 0) {
      errors.number = 'Debe escribir una numeración';
    }

    return errors;
  }

  onAppartmentTypeSelectedChange(option) {
    const data = option;
    this.setState({
      appartmentTypeSelected: data,
    });
  }

  onNameCopropietaryChange(event) {
    const data = event.target.value;
    this.setState({
      newCopropietaryName: data,
    });
  }

  onRutCopropietaryChange(event) {
    const data = event.target.value;
    this.setState({
      newCopropietaryRut: data,
    });
  }

  onEmailCopropietaryChange(event) {
    const data = event.target.value;
    this.setState({
      newCopropietaryEmail: data,
    });
  }

  onAddressCopropietaryChange(event) {
    const data = event.target.value;
    this.setState({
      newCopropietaryAddress: data,
    });
  }

  onNumberCopropietaryChange(event) {
    const data = event.target.value;
    this.setState({
      newCopropietaryNumber: data,
    });
  }

  onNameEmployeeChange(event) {
    const data = event.target.value;
    this.setState({
      newEmployeeName: data,
    });
  }

  onRutEmployeeChange(event) {
    const data = event.target.value;
    this.setState({
      newEmployeeRut: data,
    });
  }

  onEmailEmployeeChange(event) {
    const data = event.target.value;
    this.setState({
      newEmployeeEmail: data,
    });
  }

  onChargeEmployeeChange(event) {
    const data = event.target.value;
    this.setState({
      newEmployeeCharge: data,
    });
  }

  onNameStorageTypeChange(event) {
    const data = event.target.value;
    this.setState({
      newStorageTypeName: data,
    });
  }

  onCostStorageTypeChange(event) {
    const data = event.target.value;
    this.setState({
      newStorageTypeCost: data,
    });
  }

  onDescriptionStorageTypeChange(event) {
    const data = event.target.value;
    this.setState({
      newStorageTypeDescription: data,
    });
  }

  onNameParkingTypeChange(event) {
    const data = event.target.value;
    this.setState({
      newParkingTypeName: data,
    });
  }

  onCostParkingTypeChange(event) {
    const data = event.target.value;
    this.setState({
      newParkingTypeCost: data,
    });
  }

  onDescriptionParkingTypeChange(event) {
    const data = event.target.value;
    this.setState({
      newParkingTypeDescription: data,
    });
  }

  onNameAppartmentTypeChange(event) {
    const data = event.target.value;
    this.setState({
      newAppartmentTypeName: data,
    });
  }

  onCostAppartmentTypeChange(event) {
    const data = event.target.value;
    this.setState({
      newAppartmentTypeCost: data,
    });
  }

  onDescriptionAppartmentTypeChange(event) {
    const data = event.target.value;
    this.setState({
      newAppartmentTypeDescription: data,
    });
  }

  onTransferInfoBankChange(event) {
    const data = event.target.value;
    const transferInfoObject = Object.assign({}, this.state.transferInfo);
    transferInfoObject.bank = data;
    this.setState({ transferInfo: transferInfoObject });
  }

  onTransferInfoAccountNumberChange(event) {
    const data = event.target.value;
    const transferInfoObject = Object.assign({}, this.state.transferInfo);
    transferInfoObject.account_number = data;
    this.setState({ transferInfo: transferInfoObject });
  }

  onTransferInfoRutChange(event) {
    const data = event.target.value;
    const transferInfoObject = Object.assign({}, this.state.transferInfo);
    transferInfoObject.rut = data;
    this.setState({ transferInfo: transferInfoObject });
  }

  onTransferInfoAccountTypeChange(event) {
    const data = event.target.value;
    const transferInfoObject = Object.assign({}, this.state.transferInfo);
    transferInfoObject.account_type = data;
    this.setState({ transferInfo: transferInfoObject });
  }

  onTransferInfoNameChange(event) {
    const data = event.target.value;
    const transferInfoObject = Object.assign({}, this.state.transferInfo);
    transferInfoObject.name = data;
    this.setState({ transferInfo: transferInfoObject });
  }

  handleYearPaymentChange(event) {
    this.setState({ yearPayment: event.target.value });
  }

  handleMonthPaymentChange(event) {
    this.setState({ monthPayment: event.target.value });
  }

  onCancelEditClick() {
    getTransferInfo(this.state.adminCondoInfo.condo_id)
      .then((transferInfoFromApi) => {
        if (transferInfoFromApi != null) {
          this.setState({
            transferInfo: transferInfoFromApi,
          });
        }
        this.setState({
          transferInfoModalEdit: false,
        });
      });
  }

  onNextStepRemoveEmployeeModal(nextStep) {
    this.setState({
      stepRemoveEmployeeModal: nextStep,
    });
  }

  onNextStepAddEmployeeModal(nextStep) {
    this.setState({
      stepAddEmployeeModal: nextStep,
    });
  }

  onNextStepModal(nextStep) {
    this.setState({
      stepAddStorageTypeModal: nextStep,
      stepRemoveStorageType: nextStep,
      stepAddParkingTypeModal: nextStep,
      stepRemoveParkingType: nextStep,
      stepAddAppartmentTypeModal: nextStep,
      stepEditTransferInfo: nextStep,
      stepConfirmUserPayment: nextStep,
      stepRejectUserPayment: nextStep,
    });
  }

  /*ENDMODALS*/
  onAddStorageInNewCopropietary() {
    const item = null;
    const list = this.state.addStorageList;
    const listNames = this.state.addStorageNameList;
    this.setState({
      addStorageList: [...list, item],
      addStorageNameList: [...listNames, item],
    });
  }

  loadStorageTypesOption() {
    return getStorageTypes(this.state.adminCondoInfo.condo_id)
      .then((data) => {
        const transformedData = data.map((storageType) => {
          return { label: storageType.name, value: storageType.id };
        });

        return {
          options: transformedData,
        };
      });
  }

  onStorageTypeSelectedChange(option, index) {
    const list = this.state.addStorageList;
    list[index] = option;
    this.setState({
      addStorageList: list,
    });
  }

  onInputNameStorageChanged(name, index) {
    const list = this.state.addStorageNameList;
    list[index] = name;
    this.setState({
      addStorageNameList: list,
    });
  }

  onStorageDelete(index) {
    let storagesList = this.state.addStorageList.slice();
    storagesList.splice(index, 1);

    let storagesNameList = this.state.addStorageNameList.slice();
    storagesNameList.splice(index, 1);

    this.setState({
      addStorageList: storagesList,
      addStorageNameList: storagesNameList,
    });
  }

  onSubmitNewCopropietaryStorages() {
    const errors = this.validateStorageCopropietariesInput();

    if (!is.empty(errors.storageTypeSelected) || !is.empty(errors.nameNewStorage)) {
      this.setState({
        errorsStorage: errors,
      });
      return;
    }

    this.setState({
      stepAddCopropietaryModal: 4,
    });
  }

  validateStorageCopropietariesInput() {
    const errors = {};
    errors.storageTypeSelected = [];
    errors.nameNewStorage = [];

    this.state.addStorageList.map((item, index) => {
      if (this.state.addStorageList[index] === null) {
        errors.storageTypeSelected[index] = 'Seleccione tipo de bodega';
      }

      if (this.state.addStorageNameList[index] === null) {
        errors.nameNewStorage[index] = 'Ingrese nombre';
      }
    });

    return errors;
  }

  onAddParkingInNewCopropietary() {
    const item = null;
    const list = this.state.addParkingList;
    const listNames = this.state.addParkingNameList;
    this.setState({
      addParkingList: [...list, item],
      addParkingNameList: [...listNames, item],
    });
  }

  loadParkingTypesOption() {
    return getParkingTypes(this.state.adminCondoInfo.condo_id)
      .then((data) => {
        const transformedData = data.map((parkingType) => {
          return { label: parkingType.name, value: parkingType.id };
        });

        return {
          options: transformedData,
        };
      });
  }

  onParkingTypeSelectedChange(option, index) {
    const list = this.state.addParkingList;
    list[index] = option;
    this.setState({
      addParkingList: list,
    });
  }

  onInputNameParkingChanged(name, index) {
    const list = this.state.addParkingNameList;
    list[index] = name;
    this.setState({
      addParkingNameList: list,
    });
  }

  onParkingDelete(index) {
    let parkingsList = this.state.addParkingList.slice();
    parkingsList.splice(index, 1);

    let parkingNameList = this.state.addParkingNameList.slice();
    parkingNameList.splice(index, 1);

    this.setState({
      addParkingList: parkingsList,
      addParkingNameList: parkingNameList,
    });
  }

  onSubmitNewCopropietaryParkings() {
    const errors = this.validateParkingCopropietariesInputs();

    if (!is.empty(errors.parkingTypeSelected) || !is.empty(errors.nameNewParking)) {
      this.setState({
        errorsParking: errors,
      });
      return;
    }

    this.setState({
      stepAddCopropietaryModal: 3,
    });
  }

  validateParkingCopropietariesInputs() {
    const errors = {};
    errors.parkingTypeSelected = [];
    errors.nameNewParking = [];

    this.state.addParkingList.map((item, index) => {
      if (this.state.addParkingList[index] === null) {
        errors.parkingTypeSelected[index] = 'Seleccione tipo de estacionamiento';
      }

      if (this.state.addParkingNameList[index] === null) {
        errors.nameNewParking[index] = 'Ingrese nombre';
      }
    });

    return errors;
  }

  render() {
    const copropietariesListCards = this.state.copropietariesList.map((item, index) => {
      let counterParkings = 0;
      item.parkings.map(() => {
        counterParkings += 1;
      });

      let counterStorages = 0;
      item.storages.map(() => {
        counterStorages += 1;
      });

      return (
        <div
          key={index}
          className="copropietary-card"
        >
          <div className="top-container">
            <div className="icon-container">
              <div className="icon">
              </div>
            </div>
            <div className="name-container">
              <div className="name">
                {item.copropietary.name}
              </div>
            </div>
          </div>
          <div className="body-container">
            <div className="left-container">
              <div className="info-container">
                <div className="line-container">
                  <div className="left-line-container">
                    <div className="rut-icon">
                    </div>
                  </div>
                  <div className="right-line-container">
                    <div className="text-container">
                      {item.copropietary.rut}
                    </div>
                  </div>
                </div>
                <div className="line-container">
                  <div className="left-line-container">
                    <div className="email-icon">
                    </div>
                  </div>
                  <div className="right-line-container">
                    <div className="text-container">
                      {item.copropietary.email}
                    </div>
                  </div>
                </div>
                <div className="line-container">
                  <div className="left-line-container">
                    <div className="appartment-icon">
                    </div>
                  </div>
                  <div className="right-line-container">
                    {is.existy(item.house) && (
                      <div className="text-container">
                        {item.house.address} {item.house.number}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="right-container">
              <div className="right-top-container">
                <div className="line-container">
                  <div className="left-line-container">
                    <div className="parking-icon">
                    </div>
                  </div>
                  <div className="right-line-container">
                    <div className="text-container">
                      {counterParkings}
                    </div>
                  </div>
                </div>
                <div className="line-container">
                  <div className="left-line-container">
                    <div className="storage-icon">
                    </div>
                  </div>
                  <div className="right-line-container">
                    <div className="text-container">
                      {counterStorages}
                    </div>
                  </div>
                </div>
              </div>
              {/*<div className="right-bottom-container">
                <div className="more-info-button">
                  <div className="more-info-icon">
                  </div>
                </div>
              </div>*/}
            </div>
          </div>
        </div>
      );
    });
    
    const {
      openAddEmployeeModal,
      openRemoveEmployeeModal,
      openAddStorageTypeModal,
      openRemoveStorageTypeModal,
      openAddParkingTypeModal,
      openRemoveParkingTypeModal,
      openAddAppartmentTypeModal } = this.state;

    let contentConfirmUserPaymentResponse;
    if (this.state.successfulConfirmUserPayment === 1) {
      contentConfirmUserPaymentResponse = '¡Se ha aceptado el pago con éxito!';
    } else if (this.state.successfulConfirmUserPayment === 2) {
      contentConfirmUserPaymentResponse = '¡Ha ocurrido un problema!';
    }

    let contentConfirmUserPayment;
    switch (this.state.stepConfirmUserPayment) {
      case 1: {
        contentConfirmUserPayment = (
          <div className="modal-content">
            <div className="title-container">
              <div className="title">
                ¿Seguro que quieres confirmar este pago?
              </div>
            </div>
            <div className="sub-title-container">
              <div className="sub-title">
                Al copropietario le llegará una notificación avisando que su pago fue realizado con éxito
              </div>
            </div>
            <div className="buttons-container">
              <div
                className="button"
                onClick={() => {
                  this.confirmUserPayment(this.state.userPaymentCardSelected);
                  this.onNextStepModal(2);
                }}
              >
                Confirmar
              </div>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentConfirmUserPayment = (
          <div className="modal-content">
            <div className="title-container">
              <div className="title">
                {contentConfirmUserPaymentResponse}
              </div>
            </div>
            <div className="buttons-container">
              <div
                className="button"
                onClick={() => {
                  getPayedUserPayments(this.state.adminCondoInfo.condo_id)
                    .then((response) => {
                      this.setState({
                        payedUserPayments: response,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </div>
            </div>
          </div>
        );
      }
    }

    let contentRejectUserPaymentResponse;
    if (this.state.successfulRejectUserPayment === 1) {
      contentRejectUserPaymentResponse = '¡Se ha rechazado el pago con éxito!';
    } else if (this.state.successfulRejectUserPayment === 2) {
      contentRejectUserPaymentResponse = '¡Ha ocurrido un problema!';
    }

    let contentRejectUserPayment;
    switch (this.state.stepRejectUserPayment) {
      case 1: {
        contentRejectUserPayment = (
          <div className="modal-content">
            <div className="title-container">
              <div className="title">
                ¿Seguro que quieres rechazar este pago?
              </div>
            </div>
            <div className="sub-title-container">
              <div className="sub-title">
                Al copropietario le llegará una notificación avisando que hubo un problema con su pago
              </div>
            </div>
            <div className="buttons-container">
              <div
                className="button"
                onClick={() => {
                  this.rejectUserPayment(this.state.userPaymentCardSelected);
                  this.onNextStepModal(2);
                }}
              >
                Confirmar
              </div>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentRejectUserPayment = (
          <div className="modal-content">
            <div className="title-container">
              <div className="title">
                {contentRejectUserPaymentResponse}
              </div>
            </div>
            <div className="buttons-container">
              <div
                className="button"
                onClick={() => {
                  getPayedUserPayments(this.state.adminCondoInfo.condo_id)
                    .then((response) => {
                      this.setState({
                        payedUserPayments: response,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </div>
            </div>
          </div>
        );
      }
    }


    let payedUserPaymentsCards;
    if (is.empty(this.state.payedUserPayments)) {
      payedUserPaymentsCards = (
        <div className="no-payed-user-payments">
          <div
            className="text"
            onClick={() => {
              getPayedUserPayments(this.state.adminCondoInfo.condo_id)
                .then((response) => {
                  this.setState({
                    payedUserPayments: response,
                  });
                });
            }}
          >
            No existen pagos por revisar aún, haz click para actualizar.
          </div>
        </div>
      );
    } else if (!is.empty(this.state.payedUserPayments)) {
      payedUserPaymentsCards = this.state.payedUserPayments.map((item, index) => {
        return (
          <div className="payed-user-payment-card">
            <div className="top-container">
              <div className="name-container">
                {item.copro.name}
              </div>
              <div className="rut-container">
                {item.copro.rut}
              </div>
            </div>
            <div className="body-container">
              <div className="amount-container">
                <div className="static">
                  Total a pagar:
                </div>
                <div className="amount">
                  $ {item.total_amount.toLocaleString('es-IN')}
                </div>
              </div>
              <div className="transfer-code-container">
                <div className="static">
                  Código:
                </div>
                <div className="code">
                  {item.transfer_code}
                </div>
              </div>
            </div>
            <div className="buttons-container">
              <div
                className="button"
                onClick={(e) => {
                  this.onOpenRejectUserPayment(e);
                  this.setState({
                    userPaymentCardSelected: item.id,
                  });
                }}
              >
                Rechazar
              </div>
              <div
                className="button"
                onClick={(e) => {
                  this.onOpenConfirmUserPayment(e);
                  this.setState({
                    userPaymentCardSelected: item.id,
                  });
                }}
              >
                Confirmar
              </div>
            </div>
          </div>
        );
      });
    }

    const viewsNotificationsInfo = [
      {
        text: 'Revisar Pagos',
        textClass: 'text-admin-tab',
        content: (
          <div className="info-tab-condo">
            <Modal
              classNames={{
                modal: 'custom-package-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={this.state.openConfirmUserPayment}
              onClose={this.onCloseModal}
              little
            >
              {contentConfirmUserPayment}
            </Modal>
            <Modal
              classNames={{
                modal: 'custom-package-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={this.state.openRejectUserPayment}
              onClose={this.onCloseModal}
              little
            >
              {contentRejectUserPayment}
            </Modal>
            <div className="payed-user-payments-container">
              <div className="text">
                Asegúrate que el monto depositado coincide con el monto a pagar.
              </div>
              {payedUserPaymentsCards}
            </div>
          </div>
        ),
      },
    ];

    const employeesListCards = this.state.employeesList.map((item, index) => {
      return (
        <div
          key={item.id}
          className="employee-card"
        >
          <div className="top-container">
            <div className="image-container">
              <img
                alt="Placeholder"
                className="image"
                src={require("../../assets/imgs/user-placeholder.svg")}
              />
            </div>
            <div className="name-container">
              <div className="name">
                {item.name}
              </div>
            </div>
            <div
              className="trash-container"
              onClick={(e) => {
                this.setState({ selectedEmployeeCard: item.id });
                this.onOpenRemoveEmployeeModal(e);
              }}
            >
              <img
                alt="Borrar Empleado"
                className="trash"
                src={require("../../assets/imgs/trash-white.svg")}
              />
            </div>
          </div>
          <div className="info-container">
            <div className="info-line">
              <div className="text">
                Rut:
              </div>
              <div className="dynamic-text">
                {item.rut}
              </div>
            </div>
            <div className="info-line">
              <div className="text">
                Cargo:
              </div>
              <div className="dynamic-text">
                {item.charge}
              </div>
            </div>
            <div className="info-line">
              <div className="text">
                Email:
              </div>
              <div className="dynamic-text">
                {item.email}
              </div>
            </div>
          </div>
        </div>
      );
    });

    const storageTypesListCards = this.state.storageTypesList.map((item, index) => {
      return (
        <div
          key={item.id}
          className="settings-type-card"
        >
          <div className="top-container">
            <div className="name-container">
              <div className="name">
                {item.name}
              </div>
            </div>
            <div
              className="trash-container"
              onClick={(e) => {
                this.setState({
                  selectedStorageTypeCard: item.id,
                });
                this.onOpenRemoveStorageTypeModal(e);
              }}
            >
              <img
                alt="Ícono"
                className="trash"
                src={require("../../assets/imgs/trash-white.svg")}
              />
            </div>
          </div>
          <div className="body-container">
            <div className="description-container">
              <div className="description">
                {item.description}
              </div>
            </div>
            <div className="price-container">
              <div className="price">
                $ {item.cost.toLocaleString('es-IN')}
              </div>
            </div>
          </div>
        </div>
      );
    });

    let messageAddEmployeeResponse;
    if (this.state.successfulNewEmployee === 1) {
      messageAddEmployeeResponse = '¡Se ha creado funcionario con éxito!';
    } else if (this.state.successfulNewEmployee === 2) {
      messageAddEmployeeResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentAddEmployeeModal;
    switch (this.state.stepAddEmployeeModal) {
      case 1: {
        contentAddEmployeeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                ¡Registra a los funcionarios de la comunidad!
              </div>
              <div className="sub-title">
                Los vecinos lo verán en la aplicación
              </div>
            </div>
            <div className="icon-package-modal">
              <img
                alt="Ícono"
                className="icon"
                src={require("../../assets/imgs/bucket.svg")}
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => { this.onNextStepAddEmployeeModal(2); }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentAddEmployeeModal = (
          <div className="package-modal-container">
            <div className="sub-title-container">
              <div className="sub-title">
                Por favor, rellena los siguientes datos:
              </div>
            </div>
            <div className="form-line">
              <div className="line-title">
                Nombre Funcionario:
              </div>
              <input
                type="text"
                onChange={this.onNameEmployeeChange}
                value={this.state.newEmployeeName}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Rut:
              </div>
              <input
                type="text"
                onChange={this.onRutEmployeeChange}
                value={this.state.newEmployeeRut}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Cargo:
              </div>
              <input
                type="text"
                onChange={this.onChargeEmployeeChange}
                value={this.state.newEmployeeCharge}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Email:
              </div>
              <input
                type="text"
                onChange={this.onEmailEmployeeChange}
                value={this.state.newEmployeeEmail}
                className="input-form"
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  const object = {
                    name: this.state.newEmployeeName,
                    rut: this.state.newEmployeeRut,
                    charge: this.state.newEmployeeCharge,
                    email: this.state.newEmployeeEmail,
                  };
                  this.registerEmployee(this.state.adminCondoInfo.condo_id, object);
                  this.onNextStepAddEmployeeModal(3); 
                }}
              >
                Guardar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 3: {
        contentAddEmployeeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageAddEmployeeResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getEmployees(this.state.adminCondoInfo.condo_id)
                    .then((employeesList) => {
                      this.setState({
                        employeesList: employeesList,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    let messageRemoveEmployeeResponse;
    if (this.state.succesfulDestroyEmployee === 1) {
      messageRemoveEmployeeResponse = '¡Se ha borrado el empleado de los registors con éxito!';
    } else if (this.state.succesfulDestroyEmployee === 2) {
      messageRemoveEmployeeResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentRemoveEmployeeModal;
    switch (this.state.stepRemoveEmployeeModal) {
      case 1: {
        contentRemoveEmployeeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                ¿Seguro que quieres borrar a este empleado de los registros?
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  this.destroyEmployee(this.state.selectedEmployeeCard);
                  this.onNextStepRemoveEmployeeModal(2);
                }}
              >
                Confirmar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentRemoveEmployeeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageRemoveEmployeeResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getEmployees(this.state.adminCondoInfo.condo_id)
                    .then((employeesList) => {
                      this.setState({
                        employeesList: employeesList,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    let messageAddStorageTypeResponse;
    if (this.state.successfulNewStorageType === 1) {
      messageAddStorageTypeResponse = '¡Se ha agregado con éxito el nuevo tipo de bodega';
    } else if (this.state.successfulNewStorageType === 2) {
      messageAddStorageTypeResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentAddStorageTypeModal;
    switch (this.state.stepAddStorageTypeModal) {
      case 1: {
        contentAddStorageTypeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                Registra los distintos tipos de bodega
              </div>
            </div>
            <div className="icon-package-modal">
              <img
                alt="Ícono"
                className="icon"
                src={require("../../assets/imgs/warehouse-icon.svg")}
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => { this.onNextStepModal(2); }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentAddStorageTypeModal = (
          <div className="package-modal-container">
            <div className="sub-title-container">
              <div className="sub-title">
                Rellena con los siguientes datos:
              </div>
            </div>
            <div className="form-line">
              <div className="line-title">
                Nombre Tipo de Bodega:
              </div>
              <input
                type="text"
                onChange={this.onNameStorageTypeChange}
                value={this.state.newStorageTypeName}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Costo:
              </div>
              <input
                type="text"
                onChange={this.onCostStorageTypeChange}
                value={this.state.newStorageTypeCost}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Descripción:
              </div>
              <textarea
                rows={5}
                onChange={this.onDescriptionStorageTypeChange}
                value={this.state.newStorageTypeDescription}
                className="text-area-form"
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  const object = {
                    name: this.state.newStorageTypeName,
                    cost: this.state.newStorageTypeCost,
                    description: this.state.newStorageTypeDescription,
                  };
                  this.registerStorageType(this.state.adminCondoInfo.condo_id, object);
                  this.onNextStepModal(3);
                }}
              >
                Guardar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 3: {
        contentAddStorageTypeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageAddStorageTypeResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getStorageTypes(this.state.adminCondoInfo.condo_id)
                    .then((storageTypesListFromApi) => {
                      this.setState({
                        storageTypesList: storageTypesListFromApi,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    let messageRemoveStorageTypeResponse;
    if (this.state.successfulDestroyStorageType === 1) {
      messageRemoveStorageTypeResponse = '¡Se ha borrado con éxito el tipo de bodega!';
    } else if (this.state.successfulDestroyStorageType === 2) {
      messageRemoveStorageTypeResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentRemoveStorageTypeModal;
    switch (this.state.stepRemoveStorageType) {
      case 1: {
        contentRemoveStorageTypeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                ¿Seguro que deseas borrar este tipo de bodega?
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  this.destroyStorageType(this.state.selectedStorageTypeCard);
                  this.onNextStepModal(2);
                }}
              >
                Confirmar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentRemoveStorageTypeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageRemoveStorageTypeResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getStorageTypes(this.state.adminCondoInfo.condo_id)
                    .then((storageTypesList) => {
                      this.setState({
                        storageTypesList: storageTypesList,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    const parkingTypesListCards = this.state.parkingTypesList.map((item, index) => {
      return (
        <div
          key={item.id}
          className="settings-type-card"
        >
          <div className="top-container">
            <div className="name-container">
              <div className="name">
                {item.name}
              </div>
            </div>
            <div
              className="trash-container"
              onClick={(e) => {
                this.setState({ selectedParkingTypeCard: item.id });
                this.onOpenRemoveParkingTypeModal(e);
              }}
            >
              <img
                alt="Ícono"
                className="trash"
                src={require("../../assets/imgs/trash-white.svg")}
              />
            </div>
          </div>
          <div className="body-container">
            <div className="description-container">
              <div className="description">
                {item.description}
              </div>
            </div>
            <div className="price-container">
              <div className="price">
                $ {item.cost.toLocaleString('es-IN')}
              </div>
            </div>
          </div>
        </div>
      );
    });

    let messageAddParkingTypeResponse;
    if (this.state.successfulNewParkingType === 1) {
      messageAddParkingTypeResponse = '¡Se ha agregado con éxito el nuevo tipo de estacionamiento';
    } else if (this.state.successfulNewParkingType === 2) {
      messageAddParkingTypeResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentAddParkingTypeModal;
    switch (this.state.stepAddParkingTypeModal) {
      case 1: {
        contentAddParkingTypeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                Registra los tipos de estacionamientos.
              </div>
            </div>
            <div className="icon-package-modal">
              <img
                alt="Ícono"
                className="icon"
                src={require("../../assets/imgs/garage.svg")}
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => { this.onNextStepModal(2); }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentAddParkingTypeModal = (
          <div className="package-modal-container">
            <div className="sub-title-container">
              <div className="sub-title">
                Por favor rellena los siguientes datos.
              </div>
            </div>
            <div className="form-line">
              <div className="line-title">
                Nombre Tipo de Estacionamiento:
              </div>
              <input
                type="text"
                onChange={this.onNameParkingTypeChange}
                value={this.state.newParkingTypeName}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Costo:
              </div>
              <input
                type="number"
                onChange={this.onCostParkingTypeChange}
                value={this.state.newParkingTypeCost}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Descripción:
              </div>
              <textarea
                rows={5}
                onChange={this.onDescriptionParkingTypeChange}
                value={this.state.newParkingTypeDescription}
                className="text-area-form"
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  const object = {
                    name: this.state.newParkingTypeName,
                    cost: this.state.newParkingTypeCost,
                    description: this.state.newParkingTypeDescription,
                  };
                  this.registerParkingType(this.state.adminCondoInfo.condo_id, object);
                  this.onNextStepModal(3);
                }}
              >
                Guardar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 3: {
        contentAddParkingTypeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageAddParkingTypeResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getParkingTypes(this.state.adminCondoInfo.condo_id)
                    .then((parkingTypesListFromApi) => {
                      this.setState({
                        parkingTypesList: parkingTypesListFromApi,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    let messageRemoveParkingTypeResponse;
    if (this.state.successfulDestroyParkingType === 1) {
      messageRemoveParkingTypeResponse = '¡Se ha borrado con éxito el tipo de estacionamiento!';
    } else if (this.state.successfulDestroyParkingType === 2) {
      messageRemoveParkingTypeResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentRemoveParkingTypeModal;
    switch (this.state.stepRemoveParkingType) {
      case 1: {
        contentRemoveParkingTypeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                ¿Seguro que deseas borrar este tipo de estacionamiento?
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  this.destroyParkingType(this.state.selectedParkingTypeCard);
                  this.onNextStepModal(2);
                }}
              >
                Confirmar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentRemoveParkingTypeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageRemoveParkingTypeResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getParkingTypes(this.state.adminCondoInfo.condo_id)
                    .then((parkingTypesListFromApi) => {
                      this.setState({
                        parkingTypesList: parkingTypesListFromApi,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    const appartmentTypesListCards = this.state.appartmentTypesList.map((item, index) => {
      return (
        <div
          key={item.id}
          className="settings-type-card"
        >
          <div className="top-container">
            <div className="name-container">
              <div className="name">
                {item.name}
              </div>
            </div>
            <div
              className="trash-container"
              onClick={(e) => {
                this.setState({ selectedAppartmentTypeCard: item.id });
                //this.onOpenRemoveParkingTypeModal(e);
              }}
            >
              <img
                alt="Ícono"
                className="trash"
                src={require("../../assets/imgs/trash-white.svg")}
              />
            </div>
          </div>
          <div className="body-container">
            <div className="description-container">
              <div className="description">
                {item.description}
              </div>
            </div>
            <div className="price-container">
              <div className="price">
                $ {item.cost.toLocaleString('es-IN')}
              </div>
            </div>
          </div>
        </div>
      );
    });

    let messageAddAppartmentTypeResponse;
    if (this.state.successfulNewAppartmentType === 1) {
      messageAddAppartmentTypeResponse = '¡Se ha creado con éxito el tipo de apartmento!';
    } else if (this.state.successfulNewAppartmentType === 2) {
      messageAddAppartmentTypeResponse = 'Ha ocurrido un error, por favor inténtalo de nuevo mas tarde.';
    }

    let contentAddAppartmentTypeModal;
    switch (this.state.stepAddAppartmentTypeModal) {
      case 1: {
        contentAddAppartmentTypeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                Registra los tipos de apartamentos.
              </div>
            </div>
            <div className="icon-package-modal">
              <img
                alt="Ícono"
                className="icon"
                src={require("../../assets/imgs/appartmentmodal.svg")}
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => { this.onNextStepModal(2); }}
              >
                Siguiente
              </button>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentAddAppartmentTypeModal = (
          <div className="package-modal-container">
            <div className="sub-title-container">
              <div className="sub-title">
                Por favor rellena los siguientes datos.
              </div>
            </div>
            <div className="form-line">
              <div className="line-title">
                Nombre Tipo de Apartamento:
              </div>
              <input
                type="text"
                onChange={this.onNameAppartmentTypeChange}
                value={this.state.newAppartmentTypeName}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Costo:
              </div>
              <input
                type="number"
                onChange={this.onCostAppartmentTypeChange}
                value={this.state.newAppartmentTypeCost}
                className="input-form"
              />
            </div>
            <div className="form-line">
              <div className="line-title">
                Descripción:
              </div>
              <textarea
                rows={5}
                onChange={this.onDescriptionAppartmentTypeChange}
                value={this.state.newAppartmentTypeDescription}
                className="text-area-form"
              />
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  const object = {
                    name: this.state.newAppartmentTypeName,
                    cost: this.state.newAppartmentTypeCost,
                    description: this.state.newAppartmentTypeDescription,
                  };
                  this.registerAppartmentType(this.state.adminCondoInfo.condo_id, object);
                  this.onNextStepModal(3);
                }}
              >
                Guardar
              </button>
            </div>
          </div>
        );
        break;
      }
      case 3: {
        contentAddAppartmentTypeModal = (
          <div className="package-modal-container">
            <div className="title-container">
              <div className="title">
                {messageAddAppartmentTypeResponse}
              </div>
            </div>
            <div className="next-button-container">
              <button
                className="next-button"
                onClick={() => {
                  getAppartmentTypes(this.state.adminCondoInfo.condo_id)
                    .then((appartmentTypesListFromApi) => {
                      this.setState({
                        appartmentTypesList: appartmentTypesListFromApi,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </button>
            </div>
          </div>
        );
        break;
      }
    }

    const viewsSettingsInfo = [
      {
        text: 'Staff',
        textClass: 'text-admin-tab',
        content: (
          <div className="info-tab-condo">
            <Modal
              classNames={{
                modal: 'custom-package-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={openAddEmployeeModal}
              onClose={this.onCloseModal}
              little
            >
              {contentAddEmployeeModal}
            </Modal>
            <Modal
              classNames={{
                modal: 'custom-package-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={openRemoveEmployeeModal}
              onClose={this.onCloseModal}
              little
            >
              {contentRemoveEmployeeModal}
            </Modal>
            <div
              className="add-button"
              onClick={this.onOpenAddEmployeeModal}
            >
              <img
                alt="Placeholder"
                className="add-icon"
                src={require("../../assets/imgs/add-admin.svg")}
              />
            </div>
            <div className="employees-cards-container">
              {employeesListCards}
            </div>
          </div>
        ),
      },
      {
        text: 'Bodegas',
        textClass: 'text-admin-tab',
        content: (
          <div className="info-tab-condo">
            <Modal
              classNames={{
                modal: 'custom-package-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={openAddStorageTypeModal}
              onClose={this.onCloseModal}
              little
            >
              {contentAddStorageTypeModal}
            </Modal>
            <Modal
              classNames={{
                modal: 'custom-package-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={openRemoveStorageTypeModal}
              onClose={this.onCloseModal}
              little
            >
              {contentRemoveStorageTypeModal}
            </Modal>
            <div
              className="add-button"
              onClick={this.onOpenAddStorageTypeModal}
            >
              <img
                alt="Placeholder"
                className="add-icon"
                src={require("../../assets/imgs/warehouse.svg")}
              />
            </div>
            <div className="storage-cards-container">
              <div className="title">
                Gestiona los tipos de bodegas existentes
              </div>
              {storageTypesListCards}
            </div>
          </div>
        ),
      },
      {
        text: 'Estcnmtos.',
        textClass: 'text-admin-tab',
        content: (
          <div className="info-tab-condo">
            <Modal
              classNames={{
                modal: 'custom-package-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={openAddParkingTypeModal}
              onClose={this.onCloseModal}
              little
            >
              {contentAddParkingTypeModal}
            </Modal>
            <Modal
              classNames={{
                modal: 'custom-package-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={openRemoveParkingTypeModal}
              onClose={this.onCloseModal}
              little
            >
              {contentRemoveParkingTypeModal}
            </Modal>
            <div
              className="add-button"
              onClick={this.onOpenAddParkingTypeModal}
            >
              <img
                alt="Placeholder"
                className="add-icon"
                src={require("../../assets/imgs/parking.svg")}
              />
            </div>
            <div className="storage-cards-container">
              <div className="title">
                Gestiona los tipos de estacionamiento
              </div>
              {parkingTypesListCards}
            </div>
          </div>
        ),
      },
      {
        text: 'Casas',
        textClass: 'text-admin-tab',
        content: (
          <div className="info-tab-condo">
            <Modal
              classNames={{
                modal: 'custom-package-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={openAddAppartmentTypeModal}
              onClose={this.onCloseModal}
              little
            >
              {contentAddAppartmentTypeModal}
            </Modal>
            <div
              className="add-button"
              onClick={this.onOpenAddAppartmentTypeModal}
            >
              <img
                alt="Placeholder"
                className="add-icon"
                src={require("../../assets/imgs/appartbutton.svg")}
              />
            </div>
            <div className="storage-cards-container">
              <div className="title">
                Gestiona los tipos de apartamentos
              </div>
              {appartmentTypesListCards}
            </div>
          </div>
        ),
      },
    ];

    const addParkingForm = this.state.addParkingList.map((item, index) => {
      return (
        <div
          key={index}
          className="add-parking-type-form-container"
        >
          <div className="inputs-container">
            <div className="input-text">
              Tipo de Estacionamiento:
              {this.state.errorsParking.parkingTypeSelected[index] && (
                <div className="error-text">{this.state.errorsParking.parkingTypeSelected[index]}</div>
              )}
            </div>
            <Async
              name="form-field-name"
              loadOptions={this.loadParkingTypesOption}
              searchable={true}
              searchPromptText="No se ha encontrado tipo de estacionamiento"
              onChange={(event) => {
                this.onParkingTypeSelectedChange(event, index);
              }}
              value={this.state.addParkingList[index]}
              placeholder="Apartamento..."
            />
            <div className="input-text">
              Nombre:
              {this.state.errorsParking.nameNewParking[index] && (
                <div className="error-text">{this.state.errorsParking.nameNewParking[index]}</div>
              )}
            </div>
            <input
              value={this.state.addParkingNameList[index]}
              onChange={(event) => {
                const name = event.target.value;
                this.onInputNameParkingChanged(name, index);
              }}
              className="input-form"
              type="text"
            />
          </div>
          <div className="trash-container">
            <img
              alt="Borrar Estacionamiento"
              className="trash"
              src={require("../../assets/imgs/trash-color.svg")}
              onClick={() => {
                this.onParkingDelete(index);
              }}
            />
          </div>
        </div>
      );
    });

    const addStorageForm = this.state.addStorageList.map((item, index) => {
      return (
        <div
          key={index}
          className="add-storage-form-container"
        >
          <div className="inputs-container">
            <div className="input-text">
              Tipo de Bodega:
              {this.state.errorsStorage.storageTypeSelected[index] && (
                <div className="error-text">{this.state.errorsStorage.storageTypeSelected[index]}</div>
              )}
            </div>
            <Async
              name="form-field-name"
              loadOptions={this.loadStorageTypesOption}
              searchable={true}
              searchPromptText="No se ha encontrado tipo de bodega"
              onChange={(event) => {
                this.onStorageTypeSelectedChange(event, index);
              }}
              value={this.state.addStorageList[index]}
              placeholder="Bodega..."
            />
            <div className="input-text">
              Nombre:
              {this.state.errorsStorage.nameNewStorage[index] && (
                <div className="error-text">{this.state.errorsStorage.nameNewStorage[index]}</div>
              )}
            </div>
            <input
              value={this.state.addStorageNameList[index]}
              onChange={(event) => {
                const name = event.target.value;
                this.onInputNameStorageChanged(name, index);
              }}
              className="input-form"
              type="text"
            />
          </div>
          <div className="trash-container">
            <img
              alt="Borrar Bodega"
              className="trash"
              src={require("../../assets/imgs/trash-color.svg")}
              onClick={() => {
                this.onStorageDelete(index);
              }}
            />
          </div>
        </div>
      );
    });

    let nextButtonContentParking;
    if (is.empty(this.state.addParkingList)) {
      nextButtonContentParking = 'No agregar estacionamiento';
    } else {
      nextButtonContentParking = 'Siguiente';
    }

    let nextButtonContentStorage;
    if (is.empty(this.state.addStorageList)) {
      nextButtonContentStorage = 'No agregar bodega';
    } else {
      nextButtonContentStorage = 'Siguiente';
    }

    let numberOfParkings = 0;
    this.state.addParkingList.map((item) => {
      numberOfParkings = numberOfParkings + 1;
    });

    let numberOfStorages = 0;
    this.state.addStorageList.map((item) => {
      numberOfStorages = numberOfStorages + 1;
    });

    let messageCreateCopropietaryResponse;
    if (this.state.successfulNewCopropietary === 1) {
      messageCreateCopropietaryResponse = '¡Se ha creado copropietario con éxito!';
    } else if (this.state.successfulNewCopropietary === 2) {
      messageCreateCopropietaryResponse = 'Ha ocurrido un problema, inténtalo de nuevo mas tarde';
    }

    let contentAddCopropietaryModal;
    switch (this.state.stepAddCopropietaryModal) {
      case 1: {
        contentAddCopropietaryModal = (
          <div className="modal-container">
            <div className="title-container">
              <div className="title">
                Agregar Copropietario
              </div>
            </div>
            <div
              className="form-container"
              ref={(c) => {
                this.scrollDiv = c;
              }}
            >
              <div className="form-line">
                <div className="line-text">
                  Nombre:
                  {this.state.errors.name && (
                    <div className="error-text">{this.state.errors.name}</div>
                  )}
                </div>
                <input
                  type="text"
                  onChange={this.onNameCopropietaryChange}
                  value={this.state.newCopropietaryName}
                  className="input-form"
                />
              </div>
              <div className="form-line">
                <div className="line-text">
                  Rut:
                  {this.state.errors.rut && (
                    <div className="error-text">{this.state.errors.rut}</div>
                  )}
                </div>
                <input
                  type="text"
                  onChange={this.onRutCopropietaryChange}
                  value={this.state.newCopropietaryRut}
                  className="input-form"
                />
              </div>
              <div className="form-line">
                <div className="line-text">
                  Email:
                  {this.state.errors.email && (
                    <div className="error-text">{this.state.errors.email}</div>
                  )}
                </div>
                <input
                  type="text"
                  onChange={this.onEmailCopropietaryChange}
                  value={this.state.newCopropietaryEmail}
                  className="input-form"
                />
              </div>
              <div className="form-line">
                <div className="line-text">
                  Elige el tipo de apartamento:
                  {this.state.errors.selectedAppartmentType && (
                    <div className="error-text">{this.state.errors.selectedAppartmentType}</div>
                  )}
                </div>
                <Async
                  name="form-field-name"
                  loadOptions={this.loadAppartmentTypesOption}
                  searchable={true}
                  searchPromptText="No se ha encontrado tipo de apartamento"
                  onChange={this.onAppartmentTypeSelectedChange}
                  value={this.state.appartmentTypeSelected}
                  placeholder="Apartamento..."
                />
              </div>
              <div className="form-line">
                <div className="line-text">
                  Dirección:
                  {this.state.errors.address && (
                    <div className="error-text">{this.state.errors.address}</div>
                  )}
                </div>
                <input
                  type="text"
                  onChange={this.onAddressCopropietaryChange}
                  value={this.state.newCopropietaryAddress}
                  className="input-form"
                />
              </div>
              <div className="form-line">
                <div className="line-text">
                  Numeración:
                  {this.state.errors.number && (
                    <div className="error-text">{this.state.errors.number}</div>
                  )}
                </div>
                <input
                  type="number"
                  onChange={this.onNumberCopropietaryChange}
                  value={this.state.newCopropietaryNumber}
                  className="input-form"
                />
              </div>
              <div className="button-container">
                <div
                  className="button"
                  onClick={() => {
                    //change this
                    this.onSubmitNewCopropietary();
                  }}
                >
                  Siguiente
                </div>
              </div>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentAddCopropietaryModal = (
          <div className="modal-container">
            <div className="title-wizard">
              <div className="title">
                Agregar estacionamientos:
              </div>
            </div>
            <div className="list-container">
              {addParkingForm}
            </div>
            <div className="button-container">
              <div
                className="button-add"
                onClick={() => {
                  this.onAddParkingInNewCopropietary();
                }}
              >
                +
              </div>
            </div>
            <div className="jump-button-container">
              <div 
                className="jump-button"
                onClick={() => {
                  this.onSubmitNewCopropietaryParkings();
                }}
              >
                {nextButtonContentParking}
                <img
                  alt="right arrow"
                  className="jump-icon"
                  src={require("../../assets/imgs/right-arrow.svg")}
                />
              </div>
            </div>
          </div>
        );
        break;
      }
      case 3: {
        contentAddCopropietaryModal = (
          <div className="modal-container">
            <div className="title-wizard">
              <div className="title">
                Agregar bodega
              </div>
            </div>
            <div className="list-container">
              {addStorageForm}
            </div>
            <div className="button-container">
              <div
                className="button-add"
                onClick={this.onAddStorageInNewCopropietary}
              >
                +
              </div>
            </div>
            <div className="jump-button-container">
              <div
                className="jump-button"
                onClick={() => {
                  this.onSubmitNewCopropietaryStorages();
                }}
              >
                {nextButtonContentStorage}
                <img
                  alt="right arrow"
                  className="jump-icon"
                  src={require("../../assets/imgs/right-arrow.svg")}
                />
              </div>
            </div>
          </div>
        );
        break;
      }
      case 4: {
        contentAddCopropietaryModal = (
          <div className="modal-container">
            <div className="title-wizard">
              <div className="title">
                Asegura que todo esté bien:
              </div>
            </div>
            <div className="check-container">
              <div className="check-line">
                <div className="check-static">
                  Nombre:
                </div>
                <div className="check-dynamic">
                  {this.state.newCopropietaryName}
                </div>
              </div>
              <div className="check-line">
                <div className="check-static">
                  Rut:
                </div>
                <div className="check-dynamic">
                  {this.state.newCopropietaryRut}
                </div>
              </div>
              <div className="check-line">
                <div className="check-static">
                  Email:
                </div>
                <div className="check-dynamic">
                  {this.state.newCopropietaryEmail}
                </div>
              </div>
              <div className="check-line">
                <div className="check-static">
                  Apartmnt.:
                </div>
                <div className="check-dynamic">
                  {this.state.appartmentTypeSelected.label}
                </div>
              </div>
              <div className="check-line">
                <div className="check-static">
                  Dirección:
                </div>
                <div className="check-dynamic">
                  {this.state.newCopropietaryAddress} {this.state.newCopropietaryNumber}
                </div>
              </div>
              <div className="check-line">
                <div className="check-static">
                  Estc.:
                </div>
                <div className="check-dynamic">
                  {numberOfParkings}
                </div>
              </div>
              <div className="check-line">
                <div className="check-static">
                  Bodegas:
                </div>
                <div className="check-dynamic">
                  {numberOfStorages}
                </div>
              </div>
            </div>
            <div className="button-container-save">
              <div
                className="button"
                onClick={() => {
                  const object = {
                    copropietary: {
                      name: this.state.newCopropietaryName,
                      email: this.state.newCopropietaryEmail,
                      rut: this.state.newCopropietaryRut,
                      house_address: this.state.newCopropietaryAddress,
                      house_number: this.state.newCopropietaryNumber,
                      appartment_type_id: this.state.appartmentTypeSelected.value,
                    },
                    parkings: this.state.addParkingList.map((item, index) => {
                      return {
                        name: this.state.addParkingNameList[index],
                        parking_type_id: item.value,
                      };
                    }),
                    storages: this.state.addStorageList.map((item, index) => {
                      return {
                        name: this.state.addStorageNameList[index],
                        storage_type_id: item.value,
                      };
                    }),
                  };
                  this.createCopropietary(this.state.adminCondoInfo.condo_id, object);
                  this.setState({
                    stepAddCopropietaryModal: 5,
                  });
                }}
              >
                Crear
              </div>
            </div>
          </div>
        );
        break;
      }
      case 5: {
        contentAddCopropietaryModal = (
          <div className="modal-container">
            <div className="title-wizard-response">
              <div className="title">
                {messageCreateCopropietaryResponse}
              </div>
            </div>
            <div className="button-container">
              <div
                className="button"
                onClick={() => {
                  getCopropietaries(this.state.adminCondoInfo.condo_id)
                    .then((copropietariesList) => {
                      this.setState({
                        copropietariesList: copropietariesList,
                      });
                    });
                  this.onCloseModal();
                }}
              >
                Aceptar
              </div>
            </div>
          </div>
        );
        break;
      }
    }

    let messageAddNewPaymentResponse;
    if (this.state.successfulNewPayment === 1) {
      messageAddNewPaymentResponse = 'Se ha creado con éxito el nuevo cobro';
    } else if (this.state.successfulNewPayment === 2) {
      messageAddNewPaymentResponse = 'El cobro para este mes y año ya ha sido asignado';
    } else if (this.state.successfulNewPayment === 3) {
      messageAddNewPaymentResponse = 'Por favor seleccione un mes válido.';
    }

    let contentAddMonthPaymentModal;
    switch (this.state.stepAddPaymentModal) {
      case 1: {
        contentAddMonthPaymentModal = (
          <div className="modal-container">
            <div className="title-container">
              <div className="title">
                Selecciona el mes actual para empezar a cobrar los gastos comunes.
              </div>
            </div>
            <div className="form-payment-container">
              <div className="form-line">
                <div className="line-text">
                  Año:
                </div>
                <select value={this.state.yearPayment} onChange={this.handleYearPaymentChange} className="select-custom" disabled>
                  <option value={this.state.yearPayment}>{this.state.yearPayment}</option>
                </select>
              </div>
              <div className="form-line">
                <div className="line-text">
                  Mes:
                </div>
                <select value={this.state.monthPayment} onChange={this.handleMonthPaymentChange} className="select-custom">
                  <option value="" disabled>Selecciona Mes...</option>
                  <option value="1">Enero</option>
                  <option value="2">Febrero</option>
                  <option value="3">Marzo</option>
                  <option value="4">Abril</option>
                  <option value="5">Mayo</option>
                  <option value="6">Junio</option>
                  <option value="7">Julio</option>
                  <option value="8">Agosto</option>
                  <option value="9">Septiembre</option>
                  <option value="10">Octubre</option>
                  <option value="11">Noviembre</option>
                  <option value="12">Diciembre</option>
                </select>
              </div>
            </div>
            <div className="button-container">
              <div
                className="button"
                onClick={() => {
                  const object = {
                    year: this.state.yearPayment,
                    month: this.state.monthPayment,
                  };
                  this.createPayment(this.state.adminCondoInfo.condo_id, object);
                  this.setState({
                    stepAddPaymentModal: 2,
                  });
                }}
              >
                Guardar
              </div>
            </div>
          </div>
        );
        break;
      }
      case 2: {
        contentAddMonthPaymentModal = (
          <div className="modal-container">
            <div className="title-response-container">
              <div className="title">
                {messageAddNewPaymentResponse}
              </div>
            </div>
            <div className="button-container">
              <div
                className="button"
                onClick={() => {
                  this.onCloseModal();
                  getNotPublishedPayments(this.state.adminCondoInfo.condo_id)
                    .then((notPublishedPaymentsFromApi) => {
                      this.setState({
                        notPublishedPaymentsList: notPublishedPaymentsFromApi,
                      });
                    });
                  getPublishedPayments(this.state.adminCondoInfo.condo_id)
                    .then((response) => {
                      this.setState({
                        publishedPaymentsList: response,
                      });
                    });
                }}
              >
                Aceptar
              </div>
            </div>
          </div>
        );
        break;
      }
    }
    let publishedPaymentsCards;
    if (is.empty(this.state.publishedPaymentsList)) {
      publishedPaymentsCards = (
        <div className="message-empty-container">
          <div className="message">
            No hay historial de cobros :(
          </div>
        </div>
      );
    } else {
      publishedPaymentsCards = this.state.publishedPaymentsList.map((item, index) => {
        let monthAsString;
        if (item.payment_published.month === 1) {
          monthAsString = 'Enero';
        } else if (item.payment_published.month === 2) {
          monthAsString = 'Febrero';
        } else if (item.payment_published.month === 3) {
          monthAsString = 'Marzo';
        } else if (item.payment_published.month === 4) {
          monthAsString = 'Abril';
        } else if (item.payment_published.month === 5) {
          monthAsString = 'Mayo';
        } else if (item.payment_published.month === 6) {
          monthAsString = 'Junio';
        } else if (item.payment_published.month === 7) {
          monthAsString = 'Julio';
        } else if (item.payment_published.month === 8) {
          monthAsString = 'Agosto';
        } else if (item.payment_published.month === 9) {
          monthAsString = 'Septiembre';
        } else if (item.payment_published.month === 10) {
          monthAsString = 'Octubre';
        } else if (item.payment_published.month === 11) {
          monthAsString = 'Noviembre';
        } else {
          monthAsString = 'Diciembre';
        }
        return (
          <div className="actual-month-container">
            <div className="top-container">
              <div className="year-container">
                <div className="year">
                  {item.payment_published.year}
                </div>
              </div>
              <div className="edit-container">
                <div
                  className="more-info-icon"
                  onClick={(e) => {
                    this.setState({
                      paymentPublishedSelected: item.payment_published.id,
                    }, () => {
                      getPaymentDetails(this.state.paymentPublishedSelected)
                        .then((detailsFromApi) => {
                          this.setState({
                            detailsPaymentPublishSelectedList: detailsFromApi,
                            detailsPaymenPublishSelectedMonth: monthAsString,
                            detailsPaymenPublishSelectedYear: item.payment_published.year,
                          });
                        });
                      this.onOpenPaymentPublishedInfoModal(e);
                    });
                  }}
                />
              </div>
            </div>
            <div className="middle-container">
              <div className="month-container">
                <div className="month">
                  {monthAsString}
                </div>
              </div>
            </div>
            <div className="bottom-container">
              <div className="amount-container">
                <div className="amount">
                  $ {item.total_amount.total_amount.toLocaleString('es-IN')}
                </div>
              </div>
            </div>
          </div>
        );
      });
    }

    let notPublishedPaymentsCards;
    if (is.empty(this.state.notPublishedPaymentsList)) {
      notPublishedPaymentsCards = (
        <div className="message-empty-container">
          <div className="message">
            No hay cobros activos aún :(
          </div>
        </div>
      );
    } else {
      notPublishedPaymentsCards = this.state.notPublishedPaymentsList.map((item, index) => {
        let monthAsString;
        if (item.payment_not_published.month === 1) {
          monthAsString = 'Enero';
        } else if (item.payment_not_published.month === 2) {
          monthAsString = 'Febrero';
        } else if (item.payment_not_published.month === 3) {
          monthAsString = 'Marzo';
        } else if (item.payment_not_published.month === 4) {
          monthAsString = 'Abril';
        } else if (item.payment_not_published.month === 5) {
          monthAsString = 'Mayo';
        } else if (item.payment_not_published.month === 6) {
          monthAsString = 'Junio';
        } else if (item.payment_not_published.month === 7) {
          monthAsString = 'Julio';
        } else if (item.payment_not_published.month === 8) {
          monthAsString = 'Agosto';
        } else if (item.payment_not_published.month === 9) {
          monthAsString = 'Septiembre';
        } else if (item.payment_not_published.month === 10) {
          monthAsString = 'Octubre';
        } else if (item.payment_not_published.month === 11) {
          monthAsString = 'Noviembre';
        } else {
          monthAsString = 'Diciembre';
        }

        return (
          <div className="actual-month-container">
            <div className="top-container">
              <div className="year-container">
                <div className="year">
                  {item.payment_not_published.year}
                </div>
              </div>
              <div className="edit-container">
                <Link to={`/admin-cond/payments/${item.payment_not_published.id}`}>
                  <div className="edit-icon" />
                </Link>
              </div>
            </div>
            <div className="middle-container">
              <div className="month-container">
                <div className="month">
                  {monthAsString}
                </div>
              </div>
            </div>
            <div className="bottom-container">
              <div className="amount-container">
                <div className="amount">
                  $ {item.total_amount.total_amount.toLocaleString('es-IN')}
                </div>
              </div>
            </div>
          </div>
        );
      });
    }

    const detailsPaymentSelected = this.state.detailsPaymentPublishSelectedList.map((item, index) => {
      return (
        <div className="detail-container">
          <div className="description">
            {item.payment_detail.description}
          </div>
          <div className="amount">
            $ {item.payment_detail.amount.toLocaleString('es-IN')}
          </div>
          <div className="amount-copro">
            $ {item.total_amount_per_copro.total.toLocaleString('es-IN')}
          </div>
        </div>
      );
    });

    const contentPaymentPublishModal = (
      <div className="modal-container">
        <div className="title-container">
          <div className="title">
            Detalle cobro {this.state.detailsPaymenPublishSelectedMonth} año {this.state.detailsPaymenPublishSelectedYear}
          </div>
        </div>
        <div className="details-titles">
          <div className="description">
            Detalle
          </div>
          <div className="amount">
            Total
          </div>
          <div className="amount-copro">
            Total Copro
          </div>
        </div>
        <div className="details-container">
          {detailsPaymentSelected}
        </div>
      </div>
    );

    const viewsPaymentsInfo = [
      {
        text: 'Cobros',
        textClass: 'text-admin-tab',
        content: (
          <div className="info-tab-condo">
            <Modal
              classNames={{
                modal: 'custom-transfer-info-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={this.state.openAddMonthPaymentModal}
              onClose={this.onCloseModal}
              little
            >
              {contentAddMonthPaymentModal}
            </Modal>
            <Modal
              classNames={{
                modal: 'custom-transfer-info-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={this.state.openPaymentPublishedInfoModal}
              onClose={this.onCloseModal}
              little
            >
              {contentPaymentPublishModal}
            </Modal>
            <div
              className="add-payments-month-button"
              onClick={(e) => {
                this.onOpenAddMonthPaymentModal(e);
              }}
            >
              <img
                alt="Placeholder"
                className="add-icon"
                src={require("../../assets/imgs/receipt.svg")}
              />
            </div>
            <div className="refresh-button-container">
              <div 
                className="refresh-button"
                onClick={() => {
                  // y también el historial
                  getNotPublishedPayments(this.state.adminCondoInfo.condo_id)
                    .then((notPublishedPaymentsFromApi) => {
                      this.setState({
                        notPublishedPaymentsList: notPublishedPaymentsFromApi,
                      });
                    });
                  getPublishedPayments(this.state.adminCondoInfo.condo_id)
                    .then((publishedPaymentsFromApi) => {
                      this.setState({
                        publishedPaymentsList: publishedPaymentsFromApi,
                      });
                    });
                }}
              >
                Actualizar página
                <img
                  alt="Placeholder"
                  className="refresh"
                  src={require("../../assets/imgs/reload.svg")}
                />
              </div>
            </div>
            <div className="not-published-payments-container">
              {notPublishedPaymentsCards}
            </div>
            <div className="published-payments-container">
              {publishedPaymentsCards}
            </div>
          </div>
        ),
      },
    ];

    const viewsCopropietariesInfo = [
      {
        text: 'Copropietarios',
        textClass: 'text-admin-tab',
        content: (
          <div className="info-tab-condo">
            <Modal
              classNames={{
                modal: 'custom-transfer-info-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={this.state.openAddCopropietaryModal}
              onClose={this.onCloseModal}
              little
            >
              {contentAddCopropietaryModal}
            </Modal>
            <div
              className="add-copropietaries-button"
              onClick={(e) => {
                this.onOpenAddCopropietaryModal(e);
              }}
            >
              <img
                alt="Placeholder"
                className="add-icon"
                src={require("../../assets/imgs/add-admin.svg")}
              />
            </div>
            <div className="copropietaries-card-container">
              {copropietariesListCards}
            </div>
          </div>
        ),
      },
    ];

    let contentTransferInfoModal;
    if (this.state.transferInfoModalEdit === false) {
      contentTransferInfoModal = (
        <div className="modal-container">
          <div className="title-container">
            <div className="title">
              Datos Cuenta Bancaria
            </div>
          </div>
          <div className="line-container">
            <div className="static-info-container">
              <div className="static-info">
                Banco:
              </div>
            </div>
            <div className="dinamic-info-container">
              {is.existy(this.state.transferInfo.bank) && (
                <div className="dinamic-info">
                  {this.state.transferInfo.bank}
                </div>
              )}
            </div>
          </div>
          <div className="line-container">
            <div className="static-info-container">
              <div className="static-info">
                Nº Cuenta:
              </div>
            </div>
            <div className="dinamic-info-container">
              <div className="dinamic-info">
                {this.state.transferInfo.account_number}
              </div>
            </div>
          </div>
          <div className="line-container">
            <div className="static-info-container">
              <div className="static-info">
                Rut:
              </div>
            </div>
            <div className="dinamic-info-container">
              <div className="dinamic-info">
                {this.state.transferInfo.rut}
              </div>
            </div>
          </div>
          <div className="line-container">
            <div className="static-info-container">
              <div className="static-info">
                Tipo:
              </div>
            </div>
            <div className="dinamic-info-container">
              <div className="dinamic-info">
                {this.state.transferInfo.account_type}
              </div>
            </div>
          </div>
          <div className="line-container">
            <div className="static-info-container">
              <div className="static-info">
                Nombre:
              </div>
            </div>
            <div className="dinamic-info-container">
              <div className="dinamic-info">
                {this.state.transferInfo.name}
              </div>
            </div>
          </div>
          <div className="button-container">
            <div
              className="button"
              onClick={() => {
                this.setState({
                  transferInfoModalEdit: true,
                });
              }}
            >
              Editar
            </div>
          </div>
        </div>
      );
    } else {
      let messageEditTransferInfoResponse;
      if (this.state.successfulEditTransferInfo === 1) {
        messageEditTransferInfoResponse = 'Se ha modificado los datos con éxito!';
      } else if (this.state.successfulEditTransferInfo === 2) {
        messageEditTransferInfoResponse = 'Ha ocurrido un problema, por favor inténtalo de nuevo mas tarde';
      }
      switch (this.state.stepEditTransferInfo) {
        case 1: {
          contentTransferInfoModal = (
            <div className="modal-container">
              <div className="title-container">
                <div className="title">
                  Editar Cuenta Bancaria
                </div>
              </div>
              <div className="line-container">
                <div className="static-info-container">
                  <div className="static-info">
                    Banco:
                  </div>
                </div>
                <div className="dinamic-info-container">
                  <div className="input-container">
                    <input
                      type="text"
                      onChange={this.onTransferInfoBankChange}
                      value={this.state.transferInfo.bank}
                      className="input-form"
                    />
                  </div>
                </div>
              </div>
              <div className="line-container">
                <div className="static-info-container">
                  <div className="static-info">
                    Nº Cuenta:
                  </div>
                </div>
                <div className="dinamic-info-container">
                  <div className="input-container">
                    <input
                      type="text"
                      onChange={this.onTransferInfoAccountNumberChange}
                      value={this.state.transferInfo.account_number}
                      className="input-form"
                    />
                  </div>
                </div>
              </div>
              <div className="line-container">
                <div className="static-info-container">
                  <div className="static-info">
                    Rut:
                  </div>
                </div>
                <div className="dinamic-info-container">
                  <div className="input-container">
                    <input
                      type="text"
                      onChange={this.onTransferInfoRutChange}
                      value={this.state.transferInfo.rut}
                      className="input-form"
                    />
                  </div>
                </div>
              </div>
              <div className="line-container">
                <div className="static-info-container">
                  <div className="static-info">
                    Tipo:
                  </div>
                </div>
                <div className="dinamic-info-container">
                  <div className="input-container">
                    <input
                      type="text"
                      onChange={this.onTransferInfoAccountTypeChange}
                      value={this.state.transferInfo.account_type}
                      className="input-form"
                    />
                  </div>
                </div>
              </div>
              <div className="line-container">
                <div className="static-info-container">
                  <div className="static-info">
                    Nombre:
                  </div>
                </div>
                <div className="dinamic-info-container">
                  <div className="input-container">
                    <input
                      type="text"
                      onChange={this.onTransferInfoNameChange}
                      value={this.state.transferInfo.name}
                      className="input-form"
                    />
                  </div>
                </div>
              </div>
              <div className="button-container-edit">
                <div
                  className="button-edit"
                  onClick={() => {
                    this.onCancelEditClick();
                    this.onCloseModal();
                  }}
                >
                  Cancelar
                </div>
                <div 
                  className="button-edit-ok"
                  onClick={() => {
                    this.editTransferInfo(this.state.transferInfo.id, this.state.transferInfo);
                    this.onNextStepModal(2);
                  }}
                >
                  Confirmar
                </div>
              </div>
            </div>
          );
          break;
        }
        case 2: {
          contentTransferInfoModal = (
            <div className="modal-container">
              <div className="message-successful-container">
                <div className="message">
                  {messageEditTransferInfoResponse}
                </div>
              </div>
              <div className="button-container">
                <div 
                  className="button"
                  onClick={this.onCloseModal}
                >
                  Aceptar
                </div>
              </div>
            </div>
          );
          break;
        }
      }
    }

    console.log(this.state);
    let nameadmin;
    if ( is.existy(this.props.sessionState) && is.existy(this.props.sessionState.user)){
      nameadmin = this.props.sessionState.user.name;
    }

    let graphContent;
    if (this.state.graphData.length === 0) {
      graphContent = (
        <div className="">
          Aún no hay información suficiente.
        </div>
      );
    } else {
      graphContent = (
        <Pie
          data={{
            datasets: [{
              data: this.state.graphData,
              backgroundColor: ['#A7A7A7', '#FF6B6B'],
            }],
            labels: this.state.graphLabels,
          }}
          style={{
            width: '100%',
            height: '100%',
          }}
        />
      );
    }

    const { adminCondoInfo } = this.state;
    const views = [
      {
        icon: 'icon home-condo-icon-admin',
        textClass: 'icon-text',
        text: 'Inicio',
        content: (
          <div className="condo-view">
            <Modal
              classNames={{
                modal: 'custom-transfer-info-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={this.state.openTransferInfoModal}
              onClose={this.onCloseModal}
              little
            >
              {contentTransferInfoModal}
            </Modal>
            <div className="top-menu-container">
              <div className="title-page">
                Bienvenido {nameadmin}
              </div>
              <div className="info-condoadmin-container">
                <div className="img-condo-condoadmin-container">
                </div>
                <div className="info-text-condoadmin-container">
                  {adminCondoInfo.namecondo}
                </div>
              </div>
              <div className="info-condoadmin-container">
                <div className="img-address-condoadmin-container">
                </div>
                <div className="info-text-condoadmin-container-address">
                  {adminCondoInfo.address}
                </div>
              </div>
              <div className="graph-condo-admin-container">
                {graphContent}
              </div>
            </div>
            <div className="menu-container">
              <div className="left-menu-container">
                <div className="visits-button-container">
                  <Link to="/admin-cond/visits">
                    <div className="button-container">
                      <div className="image-button-container visits">
                      </div>
                      <div className="text-button-container">
                        Visitas
                      </div>
                    </div>
                  </Link>
                </div>
                <div className="news-button-container">
                  <Link to="/admin-cond/news">
                    <div className="button-container">
                      <div className="image-button-container news">
                      </div>
                      <div className="text-button-container">
                        Noticias
                      </div>
                    </div>
                  </Link>
                </div>
              </div>
              <div className="right-menu-container">
                <div className="packages-button-container">
                  <Link to="/admin-cond/packages">
                    <div className="button-container">
                      <div className="image-button-container package">
                      </div>
                      <div className="text-button-container">
                        Paquetes
                      </div>
                    </div>
                  </Link>
                </div>
                <div className="transfer-info-button-container">
                  <div
                    className="button-container"
                    onClick={this.onOpenTransferInfoModal}
                  >
                    <div className="image-button-container transfer-info">
                    </div>
                    <div className="text-button-container">
                      Cta. Bancaria
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ),
      },
      {
        icon: 'icon copropietaries-icon-admin',
        textClass: 'icon-text',
        text: 'Copropietarios',
        content: (
          <div className="condo-view">
            <TabBarAdminCondo
              views={viewsCopropietariesInfo}
            />
          </div>
        ),
      },
      {
        icon: 'icon payments-icon-admin',
        textClass: 'icon-text',
        text: 'Cobros',
        content: (
          <div className="condo-view">
            <Switch>
              <Route path="/admin-cond/payments/:paymentId" render={(props) => <HomePaymentsAdminCond sessionState={this.props.sessionState} history={this.props.history} match={props.match} />} />
              <Route
                render={() => (
                  <TabBarAdminCondo
                    history={this.props.history}
                    views={viewsPaymentsInfo}
                  />
                )}
              />
            </Switch>
          </div>
        ),
      },
      {
        icon: 'icon notifications-icon-admin',
        textClass: 'icon-text',
        text: 'Revisar Pagos',
        content: (
          <div className="condo-view">
            <TabBarAdminCondoNotificaciones
              views={viewsNotificationsInfo}
            />
          </div>
        ),
      },
      {
        icon: 'icon settings-icon-admin',
        textClass: 'icon-text',
        text: 'Otros',
        content: (
          <div className="condo-view">
            <TabBarAdminCondoNotificaciones
              views={viewsSettingsInfo}
            />
          </div>
        ),
      },
    ];
    return (
      <div className="">
        <Switch>
          <Route path="/admin-cond/visits" render={() => <HomeVisitsAdminCond sessionState={this.props.sessionState} history={this.props.history} />} />
          <Route path="/admin-cond/news" render={() => <HomeNewsAdminCond sessionState={this.props.sessionState} history={this.props.history} />} />
          <Route path="/admin-cond/packages" render={() => <HomePackagesAdminCond sessionState={this.props.sessionState} history={this.props.history} />} />
          <Route
            render={() => (
              <TabBarNavigation
                history={this.props.history}
                onTabSelected={this.onTabSelected}
                views={views}
              />
            )}
          />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminCondContainer));