import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import { Async } from 'react-select';

import history from '../utils/historyHelper';
import { getCondos, deleteCondo, addCondo, getCondoAdmins, addCondoAdmin } from '../api/adminApi';



import TabBarNavigation from '../components/TabBarNavigation';

class AdminContainer extends Component {
  // Constructor
  constructor(props) {
    super(props);
    this.state = {
      condos: [],
      condoAdmins: [],
      selectedItem: null,
      open: false,
      openDelete: false,
      openAddAdmin: false,
      name: '',
      address: '',
      nameadmin: '',
      passwordadmin: '',
      passwordconfirmation: '',
      emailadmin: '',
      condominio: '',
    };

    this.renderItems = this.renderItems.bind(this);
    this.deleteCondo = this.deleteCondo.bind(this);
    this.onOpenModal = this.onOpenModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.onOpenDeleteModal = this.onOpenDeleteModal.bind(this);
    this.onOpenAddAccountModal = this.onOpenAddAccountModal.bind(this);
    this.onCloseDeleteModal = this.onCloseDeleteModal.bind(this);
    this.sendCondoInfo = this.sendCondoInfo.bind(this);
    this.sendAdminCondoInfo = this.sendAdminCondoInfo.bind(this);
    this.onNameChange = this.onNameChange.bind(this);
    this.onAddressChange = this.onAddressChange.bind(this);
    this.onNameAdminChange = this.onNameAdminChange.bind(this);
    this.onPasswordAdminChange = this.onPasswordAdminChange.bind(this);
    this.onPasswordConfirmationAdminChange = this.onPasswordConfirmationAdminChange.bind(this);
    this.onAdminEmailChange = this.onAdminEmailChange.bind(this);
    this.onCondominioChange = this.onCondominioChange.bind(this);
    this.logout = this.logout.bind(this);

    this.loadCondoOptions = this.loadCondoOptions.bind(this);
  }

  componentDidMount() {
    if (this.props.sessionState.token === null) {
      this.props.history.push('/auth');
      return;
    }
    getCondos()
      .then((condosFromApi) => {
        this.setState({
          condos: condosFromApi,
        });
      });
    getCondoAdmins()
      .then((condoAdminsFromApi) => {
        this.setState({
          condoAdmins: condoAdminsFromApi,
        });
      });
  }

  logout() {
       
  }

  loadCondoOptions(input) {
    // Aqui definimos que haremos durante la promesa.
    // Primero, obtenemos los items de la API
    return getCondos({ withoutAdmin: true })
      .then((data) => {

        // Aqui los formateamos para seguir las reglas que nos impuso
        // el autor del componente react-select. Eso es porque
        // se le paro la raja.
        const transformedData = data.map((condo) => {
          // Esto es para cada condo. Recordar que es una transformación
          return { label: condo.namecondo, value: condo.id };
        });

        return {
          options: transformedData,
        };
      });
  }

  onOpenModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ open: true });
  }

  onOpenDeleteModal(e, item) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openDelete: true, selectedItem: item });
  }

  onOpenAddAccountModal(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ openAddAdmin: true });
  }

  onCloseModal() {
    this.setState({ open: false, openAddAdmin: false });
  }

  onYesDelete() {
    const item = this.state.selectedItem;
    this.deleteCondo(item.id);
    this.setState({ openDelete: false });
  }

  onCloseDeleteModal() {
    this.setState({ openDelete: false });
  }

  deleteCondo(condoId) {
    // en un principio condos viene vacío, 
    // pero sucede algo que hace que el estado cambie antes de llegar aqui.
    const condos = this.state.condos;
    const index = condos.findIndex((condo) => {
      return condo.id === condoId;
    });
    condos.splice(index, 1);

    this.setState({
      condos: condos,
    });

    deleteCondo(condoId);
  }

  renderCondoAdminCards() {
    const { condoAdmins } = this.state;
    const renderedAdminCondos = condoAdmins.map((adminCondo) => {
      return (
        <div className="admin-card" key={adminCondo.id}>
          <div className="content-admin-card">
            <div className="line-content">
              <div className="name-admin-card">
                {adminCondo.name}
              </div>
            </div>
            <div className="line-content">
              <img
                alt="Ícono"
                className="icon-admin-card"
                src={require("../../assets/imgs/house-admin.svg")}
              />
              <div className="text-admin-card">
                {adminCondo.namecondo}
              </div>
            </div>
            <div className="line-content">
              <img
                alt="Ícono"
                className="icon-admin-card"
                src={require("../../assets/imgs/email.svg")}
              />
              <div className="text-admin-card">
                {adminCondo.email}
              </div>
            </div>
          </div>
        </div>
      );
    });
    return renderedAdminCondos;
  }

  renderItems() {
    const { condos } = this.state;

    const renderedCondos = condos.map((condo) => {
      return (
        <div className="card" key={condo.id}>
          <div className="avatar-container">
            <div className="avatar" />
          </div>
          <div className="content">
            <div className="name">
              {condo.namecondo}
            </div>
            <div className="condo-info-container">
              <div className="text-condo-container">
                <div className="icon-condo-container">
                  <img
                    alt="Direeción"
                    className="condo-info-icon"
                    src={require("../../assets/imgs/sign-post.svg")}
                  />
                </div>
                <div className="text-condo">
                  {condo.address}
                </div>
              </div>
              <div className="text-condo-container">
                <div className="icon-condo-container">
                  <img
                    alt="Direeción"
                    className="condo-info-icon"
                    src={require("../../assets/imgs/team.svg")}
                  />
                </div>
                <div className="text-condo">
                  0 copropietarios
                </div>
              </div>
            </div>
          </div>
          <div className="actions-container">

            <button
              onClick={(e) => {
                this.onOpenDeleteModal(e, condo);
              }}
            >
              <img
                alt="Borrar"
                className="actions-icon"
                src={require("../../assets/imgs/trash.svg")}
              />
            </button>

            <button>
              <img
                alt="Ver condominio"
                className="actions-icon"
                src={require("../../assets/imgs/view.svg")}
              />
            </button>

          </div>
        </div>
      );
    });
    return renderedCondos;
  }

  onNameChange(event) {
    const data = event.target.value;
    this.setState({
      name: data,
    });
  }

  onAddressChange(event) {
    const data = event.target.value;
    this.setState({
      address: data,
    });
  }

  onNameAdminChange(event) {
    const data = event.target.value;
    this.setState({
      nameadmin: data,
    });
  }

  onPasswordAdminChange(event) {
    const data = event.target.value;
    this.setState({
      passwordadmin: data,
    });
  }

  onPasswordConfirmationAdminChange(event) {
    const data = event.target.value;
    this.setState({
      passwordconfirmation: data,
    });
  }

  onAdminEmailChange(event) {
    const data = event.target.value;
    this.setState({
      emailadmin: data,
    });
  }

  onCondominioChange(option) {
    const data = option;
    this.setState({
      condominio: data,
    });
  }

  sendCondoInfo() {
    // acá validación si form va vacio
    const condoInfo = {
      namecondo: this.state.name,
      address: this.state.address,
    };
    addCondo(condoInfo)
      .then(() => {
        this.onCloseModal();
        this.setState({
          name: '',
          address: '',
        });
        //Para actualizar lista.
        getCondos()
          .then((condosFromApi) => {
            this.setState({
              condos: condosFromApi,
            });
          });
      });
  }

  sendAdminCondoInfo(){
    const adminCondoInfo = {
      name: this.state.nameadmin,
      email: this.state.emailadmin,
      password: this.state.passwordadmin,
      password_confirmation: this.state.passwordconfirmation,
      condominio_id: this.state.condominio.value,
    };
    addCondoAdmin(adminCondoInfo)
      .then(() => {
        this.onCloseModal();
        this.setState({
          nameadmin: '',
          passwordadmin: '',
          passwordconfirmation: '',
          emailadmin: '',
          condominio: '',
        });
        // Para actualizar lista.
        getCondoAdmins()
          .then((adminsCondoFromApi) => {
            this.setState({
              condoAdmins: adminsCondoFromApi,
            });
          });
      });
  }

  render() {
    const { open, openDelete, openAddAdmin } = this.state;
    const views = [
      {
        icon: 'icon condo-icon',
        textClass: 'icon-text',
        text: 'Condominios',
        content: (
          <div className="condo-view">

            <Modal 
              classNames={{
                modal: 'custom-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={open} 
              onClose={this.onCloseModal}
              little
            >
              <div className="form-container">
                <h2>Agregar condominio</h2>
                Nombre de condominio:
                <input type="text" onChange={this.onNameChange} value={this.state.name} className="input-form" />
                Dirección de condominio:
                <input type="text" onChange={this.onAddressChange} value={this.state.address} className="input-form" />
                <button className="input-form-submit" onClick={this.sendCondoInfo}>
                  Agregar
                </button>
              </div>
            </Modal>

            <Modal
              classNames={{
                modal: 'custom-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={openDelete}
              onClose={this.onCloseDeleteModal}
              little
            >
              <div className="form-container">
                <h2>¿Seguro que desea eliminar condominio?</h2>
                <button
                  className="danger-button"
                  onClick={() => {
                    this.onYesDelete();
                  }}
                >
                  Borrar
                </button>
              </div>
            </Modal>
            <div className="title-page">
              Administrador de Sistema
            </div>
            <div className="subtitle-page">
              Gestión de condominios
            </div>
            <div className="list-container">
              {this.renderItems()}
            </div>
            <div
              className="add-condo"
              onClick={this.onOpenModal}
            >
              <img
                alt="Agregar Condominio"
                className="add-condo-img"
                src={require("../../assets/imgs/add-condo.png")}
              />
            </div>
          </div>
        ),
      }, {
        icon: 'icon account-icon',
        textClass: 'icon-text',
        text: 'Cuentas Administrativas',
        content: (
          <div className="condo-view">

            <Modal 
              classNames={{
                modal: 'custom-modal',
                closeIcon: 'custom-modal-close',
              }}
              open={openAddAdmin}
              onClose={this.onCloseModal}
              little
            >
              <div className="form-container">
                <h2>Agregar Cuenta Administrativa</h2>
                Nombre Administrador:
                <input type="text" onChange={this.onNameAdminChange} value={this.state.nameadmin} className="input-form" />
                Contraseña:
                <input type="password" onChange={this.onPasswordAdminChange} value={this.state.passwordadmin} className="input-form" />
                Reingrese Contraseña:
                <input type="password" onChange={this.onPasswordConfirmationAdminChange} value={this.state.passwordconfirmation} className="input-form" />
                Correo:
                <input type="text" onChange={this.onAdminEmailChange} value={this.state.emailadmin} className="input-form" />
                Condominio a cargo:
                <Async
                  name="form-field-name"
                  loadOptions={this.loadCondoOptions}
                  searchable={false}
                  searchPromptText="Seleccionar..."
                  onChange={this.onCondominioChange}
                  value={this.state.condominio}
                  placeholder="Vincular condominio..."
                />
                <button className="input-form-submit" onClick={this.sendAdminCondoInfo}>
                  Agregar
                </button>
              </div>
            </Modal>

            <div className="title-page">
              Administrador de Sistema
            </div>
            <div className="subtitle-page">
              Gestión de cuentas
            </div>
            <div className="list-container">
              {this.renderCondoAdminCards()}
            </div>
            <div
              className="add-admin-account"
              onClick={this.onOpenAddAccountModal}
            >
              <img
                alt="Agregar Cuenta Administrativa"
                className="add-admin-img"
                src={require("../../assets/imgs/add-admin.svg")}
              />
            </div>
          </div>
        ),
      },
    ];

    return (
      <TabBarNavigation
        history={this.props.history}
        views={views}
      />
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminContainer));
