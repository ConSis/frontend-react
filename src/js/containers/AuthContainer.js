import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classnames from 'classnames';

import Input from '../components/Input';
import Button from '../components/Button';

import { logIn } from '../api/authApi';

import UserLoginIcon from '../../assets/imgs/user-login.svg';
import KeyIcon from '../../assets/imgs/key.svg';


// El componente big boss
class AuthContainer extends Component {
  // Constructor
  constructor(props) {
    super(props);
    this.state = {
      rut: '',
      password: '',
    };

    this.onRutChanged = this.onRutChanged.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onLogin = this.onLogin.bind(this);
  }

  onLogin() {
    if (this.props.sessionState.isLoggingIn) {
      return;
    }
    
    const credentials = {
      email: this.state.rut,
      password: this.state.password,
    };

    logIn(credentials)
      .then(() => {
        const userType = this.props.sessionState.user.user_type;
        if (userType === 'adminSistema') {
          this.props.history.push('/admin');
        }
        else if(userType === 'admin') {
          this.props.history.push('/admin-cond');
        }
        else if(userType === 'copropietary') {
          this.props.history.push('/copropietary')
        }
      })
      .catch(() => {
        alert('Las credenciales no son correctas', undefined, 'Error');
      });
  }

  onRutChanged(data) {
    this.setState({
      rut: data,
    });
  }

  onPasswordChange(data) {
    this.setState({
      password: data,
    });
  }

  render() {
    return (
      <div className="auth-page">
        <div className="auth-header">
          <div className="logo-overlay">
            <div className="logo" />
          </div>
        </div>
        <div className="form-container">
          <Input
            type="email"
            icon={UserLoginIcon}
            onChange={this.onRutChanged}
            value={this.state.rut}
            placeholder="Usuario"
          />
          <Input
            type="password"
            icon={KeyIcon}
            onChange={this.onPasswordChange}
            value={this.state.password}
            placeholder="Contraseña"
          />
          <Button
            onClick={this.onLogin}
            className={classnames({
              'button-login': true,
              disabled: this.props.sessionState.isLoggingIn,
            })}
          />
        </div>
      </div>
    );
  }
}

AuthContainer.propTypes = {
};

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}


export default connect(mapStateToProps, mapDispatchToProps)(AuthContainer);
