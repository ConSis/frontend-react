import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
import is from 'is_js';

import history from '../utils/historyHelper';
import TabBarNavigation from '../components/TabBarNavigation';
import { Carousel } from 'react-responsive-carousel';
import HomeBlackboardCopropietary from '../components/copropietary/HomeBlackboardCopropietary';
import HomeComplaintsCopropietary from '../components/copropietary/HomeComplaintsCopropietary';
import HomeCopropietary from '../components/copropietary/HomeCopropietary';
import HomePackageCopropietary from '../components/copropietary/HomePackageCopropietary';
import CondoCopropietary from '../components/copropietary/CondoCopropietary';
import NotificationsCopropietary from '../components/copropietary/NotificationsCopropietary';
import PaymentsCopropietary from '../components/copropietary/PaymentsCopropietary';

import { getCopropietaryInfo, checkPayment } from '../api/copropietaryApi';
import { getNotifications } from '../api/notificationApi';

class CopropietaryContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copropietaryInfo: {},
    };

    this.onTabSelected = this.onTabSelected.bind(this);

    this.getMoreNotifications = this.getMoreNotifications.bind(this);
  }

  componentDidMount() {
    if (this.props.sessionState.token === null) {
      this.props.history.push('/auth');
      return;
    }

    getCopropietaryInfo()
      .then((copropietaryInfoFromApi) => {
        this.setState({
          copropietaryInfo: copropietaryInfoFromApi,
        }, () => {
          this.getMoreNotifications();
        });

      });
  }

  onTabSelected(index) {
    
  }

  getMoreNotifications() {
    getNotifications(this.state.copropietaryInfo.id)
      .then(() => {
        setTimeout(this.getMoreNotifications, 3000);
      })
      .catch(() => {
        setTimeout(this.getMoreNotifications, 3000);
      });
  }

  render() {
    //const { copropietaryInfo } = this.state;
    const views = [
      {
        icon: 'icon home-condo-icon-admin',
        textClass: 'icon-text',
        text: 'Inicio',
        content: (
          <Switch>
            <Route path="/copropietary/blackboard" render={()=><HomeBlackboardCopropietary sessionState={this.props.sessionState} history={this.props.history} />} />
            <Route path="/copropietary/complaints" render={()=><HomeComplaintsCopropietary sessionState={this.props.sessionState} history={this.props.history} />} />
            <Route path="/copropietary/packages" render={()=><HomePackageCopropietary sessionState={this.props.sessionState} history={this.props.history} />} />
            <Route render={()=><HomeCopropietary sessionState={this.props.sessionState} history={this.props.history} />} />
          </Switch>
        ),
      },
      {
        icon: 'icon payments-icon-copropietary',
        textClass: 'icon-text',
        text: 'Pagos',
        content: (
          <Switch>
            <Route render={()=><PaymentsCopropietary sessionState={this.props.sessionState} history={this.props.history} />} />
          </Switch>
        ),
      },
      {
        /*
          CONDOMINIO INFO PAGE
        */
        icon: 'icon condo-icon-copropietary',
        textClass: 'icon-text',
        text: 'Condominio',
        content: (
          <Switch>
            <Route render={()=><CondoCopropietary sessionState={this.props.sessionState} history={this.props.history} />} />
          </Switch>
        ),
      },
      {
        /*
          NOTIFICATIONS PAGE
        */
        icon: 'icon notification-icon-copropietary',
        textClass: 'icon-text',
        text: 'Notificaciones',
        content: (
          <Switch>
            <Route render={()=><NotificationsCopropietary sessionState={this.props.sessionState} history={this.props.history} notifications={this.props.notificationsState.notifications} />} />
          </Switch>
        ),
      },
    ];
    return (
      <TabBarNavigation
        history={this.props.history}
        onTabSelected={this.onTabSelected}
        views={views}
      />
    );
  }
}

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
    notificationsState: stores.notificationsState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CopropietaryContainer));