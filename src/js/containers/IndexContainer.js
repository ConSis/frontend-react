import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import is from 'is_js';

import history from '../utils/historyHelper';


class IndexContainer extends Component {
  // Constructor
  constructor(props) {
    super(props);
    this.state = {

    };

    this.sendMessage = this.sendMessage.bind(this);
  }

  componentDidMount() {
    const { sessionState } = this.props;

    let redirectionTarget = '/auth';
    if (is.existy(sessionState)
    && is.existy(sessionState.token)
    && is.existy(sessionState.user)) {
      switch (sessionState.user.user_type) {
        case 'copropietary': {
          redirectionTarget = '/copropietary';
          break;
        }
        case 'adminSistema': {
          redirectionTarget = '/admin';
          break;
        }
        case 'admin': {
          redirectionTarget = '/admin-cond';
          break;
        }
        default: {
          redirectionTarget = '/auth';
        }
      }
    }
    this.props.history.replace(redirectionTarget);
  }

  sendMessage() {

  }

  render() {
    return (
      <div>
        Inicio
      </div>
    );
  }
}

IndexContainer.propTypes = {
};

const mapStateToProps = function mapState(stores) {
  return {
    sessionState: stores.sessionState,
  };
};

function mapDispatchToProps(dispatch) {
  const combiner = Object.assign({},
    { dispatch },
  );
  return bindActionCreators(
    combiner,
    dispatch,
  );
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(IndexContainer));
