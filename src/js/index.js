import { render } from 'react-dom';
import initReactFastclick from 'react-fastclick';

// Stylesheets
import '../sass/main.scss';
import 'react-select/dist/react-select.css';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import routes from './routes';

console.log('Successfully attached javascript. Starting App')


const startApp = () => {
  console.log('Starting app :3');
  initReactFastclick();
  const html = document.getElementsByTagName('html');
  const body = document.getElementsByTagName('body');
  html.className += ' cordova';
  body.className += ' cordova';
  render(routes, document.getElementById('root'));
};

// Wait a little if we are running cordova
if (window.cordova) {
  document.addEventListener('deviceready', startApp, false);
} else {
  startApp();
}

// Installation of the service worker
/*
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('service-worker.js');
}
*/

if (module.hot) {
  module.hot.accept('./routes', () => {
    render(routes, document.getElementById('root'));
  });
}
