import { combineReducers } from 'redux';

import sessionReducer from './sessionReducer';
import notificationsReducer from './notificationsReducer';

const rootReducer = combineReducers({
  sessionState: sessionReducer,
  notificationsState: notificationsReducer,
});

export default rootReducer;
