import * as types from '../actions/action-types';

const initialState = {
  notifications: [],
};

const notificationsReducer = function (state = initialState, action) {
  switch (action.type) {
    case types.ON_GET_NOTIFICATIONS_SUCCESS: {
      const { notifications } = action;

      return Object.assign(
        {},
        state,
        {
          notifications: notifications,
        },
      );
    }

    default:
      return state;
  }
};

export default notificationsReducer;
