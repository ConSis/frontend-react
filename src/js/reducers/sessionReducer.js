import * as types from '../actions/action-types';
import { loadSession, saveSession } from '../utils/localStorageHelper';

const initialState = {
  user: null,
  token: null,
  uid: null,
  client: null,
  isLoggingIn: false,
};

const sessionReducer = function (state = initialState, action) {
  switch (action.type) {
    case types.ON_LOGIN_REQUEST: {
      return Object.assign(
        {},
        state,
        {
          isLoggingIn: true,
        },
      );
    }

    case types.ON_LOGIN_SUCCESS: {
      const { session } = action;

      return Object.assign(
        {},
        state,
        {
          user: session.user,
          token: session.token,
          uid: session.uid,
          client: session.client,
          isLoggingIn: false,
        },
      );
    }

    case types.ON_LOGIN_FAILURE: {
      return Object.assign(
        {},
        state,
        {
          isLoggingIn: false,
        },
      );
    }

    default:
      return state;
  }
};

export default sessionReducer;
