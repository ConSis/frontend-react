import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router-dom';

import store from './store';
import history from './utils/historyHelper';

import IndexContainer from './containers/IndexContainer';
import AuthContainer from './containers/AuthContainer';
import AdminContainer from './containers/AdminContainer';
import AdminCondContainer from './containers/AdminCondContainer';
import CopropietaryContainer from './containers/CopropietaryContainer';


class Diagnoser extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount(){
    console.log(this.props);
  }

  render() {
    return (
      <div>
        <Switch>
          <Route path="/" exact component={IndexContainer} />
          <Route path="/auth" component={AuthContainer} />
          <Route path="/admin" component={AdminContainer} />
          <Route path="/admin-cond" component={AdminCondContainer} />
          <Route path="/copropietary" component={CopropietaryContainer} />
        </Switch>
      </div>
    );
  }
}

const routes = (
  <Provider store={store}>
    <Router history={history}>
      <Diagnoser />
    </Router>
  </Provider>
);

export default routes;
