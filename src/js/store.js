import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers';
import { loadSession } from './utils/localStorageHelper';

// Import previous session
const persistedSession = loadSession();

// Create the store
const store = createStore(
  rootReducer,
  persistedSession,
  compose(
    applyMiddleware(thunkMiddleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f,
  ),
);
export default store;
