import is from 'is_js';
import store from '../store';
import history from './historyHelper';

export const getAuthToken = () => {
  const state = store.getState();
  const user = state.sessionState.user;
  return user.token;
};

export const getUserSession = () => {
  const state = store.getState();
  const session = state.sessionState;
  return session;
};
