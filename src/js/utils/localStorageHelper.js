export const loadSession = () => {
  try {
    const serializedState = localStorage.getItem('sessionState');
    if (serializedState === null) {
      return undefined;
    }
    return {
      sessionState: JSON.parse(serializedState),
    };
  } catch (err) {
    return undefined;
  }
};

export const saveSession = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('sessionState', serializedState);
  } catch (err) {
    console.err(err);
  }
};
