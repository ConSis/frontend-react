const WS_PROTOCOL = 'wss';
const API_VERSION = 'v1';

// Local
const DOMAIN = 'localhost:3000';
const PROTOCOL = 'http';

// Custom backend
//const DOMAIN = '192.168.43.95:3000';
//const PROTOCOL = 'http';

// Lo de abajo es analogo a como se usa format en C
// format( "%s://$s/%s", PROTOCOL, DOMAIN, API_VERSION)
const URL = `${PROTOCOL}://${DOMAIN}`;

export const WS_URL = `${WS_PROTOCOL}://${DOMAIN}/cable`;
export default URL;
