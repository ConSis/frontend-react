// More info: https://webpack.js.org/api/node/

/* eslint-disable no-console */
/* eslint-disable comma-dangle */

import webpack from 'webpack';
import historyApiFallback from 'connect-history-api-fallback';
import compression from 'compression';
import chalk from 'chalk';
import { exec } from 'child_process';

import webpackConfig from '../webpackConfig/config.prod.onlybuild';


const env = process.env.NODE_ENV;
const compiler = webpack(webpackConfig);

console.log(chalk.cyan(
  `
=>  webpack is bundling project files...`
));

console.log(chalk.green(
  `=>  NODE_ENV is set to ${chalk.white(env)}.
`
));

const buildCordova = () => {
  // Move dist files to cordova
  console.log(chalk.cyan('Preparing CORDOVA environment'));
  exec('cordova prepare', (err, out, code) => {
    console.log(chalk.cyan('CORDOVA Set up. Ready to run platforms'));
  });
};

compiler.run((err, stats) => {
  if (err) {
    console.log(chalk.red(err.stack || err));
    if (err.details) {
      console.log(chalk.red(err.details));
    }
    return 1;
  }

  stats.toJson('verbose');

  console.log(stats.toString({
    context: webpackConfig.context,
    performance: true,
    hash: true,
    timings: true,
    entrypoints: true,
    chunkOrigins: true,
    chunkModules: false,
    colors: true,
  }));

  const buildErrors = stats.hasErrors();
  const buildWarnings = stats.hasWarnings();

  if (buildErrors) {
    console.log(chalk.red.bold(
      `
:(  ERRORS DURING COMPILATION!
=>  Fix them and try again!`
    ));

    return 1;
  }

  console.log(chalk.green(
    `
:)  PROJECT FILES ARE COMPILED!
    `
  ));

  if (buildWarnings) {
    console.log(chalk.yellow(
      `=>  But build have some issues...
=>  Look at compiler warnings above!`
    ));
  }

  buildCordova();

  return 0;
});
