import ExtractTextPlugin from 'extract-text-webpack-plugin';

import { NODE_MODULES, SRC } from './paths';

export default [
  {
    test: /\.css$/,
    include: [SRC],
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: ['css-loader'],
    }),
  },
  {
    test: /\.scss$/,
    include: [SRC],
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: ['css-loader', 'postcss-loader', 'resolve-url-loader', 'sass-loader'],
    }),
  },

  {
    test: /\.css$/,
    include: [NODE_MODULES],
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: ['css-loader', 'postcss-loader'],
    }),
  },
];
